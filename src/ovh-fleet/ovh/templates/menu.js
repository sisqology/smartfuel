import * as React from 'react';
import {Router} from "react-router";
import {history} from "./../../../reuse/history";
import {Fragment} from "react";
import {NavLink} from "react-router-dom";
import {IconFolder} from "../../../reuse/svgs";
import MaterialIcon from 'material-icons-react';

class OvhMenu extends React.Component {

    componentDidMount() {
        
    }

    render() {
        const url = "/vendor/";

        return (
            <Fragment>
                <div id="side-nav-mask"></div>
                <aside className="isw-sideBar" id="side-nav">
                    <div className="isw-sideBar-innerScroll" style={{ overflowY: "scroll" }}>
                        <div className="isw-sideBar--wrapper">
                            <div className="isw-sideBar--linkBox">
                                <ul>
                                    <span className="isw-sideBar-ulSpan" />
                                    <li className="isw-sideBar-li" role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}dashboard`}
                                            className="isw-sideBar-a"
                                            tabIndex="1"
                                        >
                                            <i className="material-icons">
                                                <MaterialIcon icon="web_asset" />
                                            </i>
                                            <span className="isw-sideBar-spanText">
                                        Dashboard
                                    </span>
                                        </NavLink>
                                    </li>
                                </ul>

                                <ul>
                                    <span className="isw-sideBar-ulSpan" />
                                    <li className="isw-sideBar-li " role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}users`}
                                            className="isw-sideBar-a"
                                            tabIndex="2"
                                        >
                                            <i className="material-icons">
                                                <MaterialIcon icon="people" />
                                            </i>
                                            <span className="isw-sideBar-spanText">
                                        User Management
                                    </span>
                                        </NavLink>
                                    </li>

                                    <li className="isw-sideBar-li" role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}fleet-owners`}
                                            className="isw-sideBar-a"
                                            tabIndex="3"
                                        >
                                            <i className="material-icons">
                                                <MaterialIcon icon="people" />
                                            </i>
                                            <span className="isw-sideBar-spanText">
                                        Fleet Management
                                    </span>
                                        </NavLink>
                                    </li>


                                    <li className="isw-sideBar-li" role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}order-management`}
                                            className="isw-sideBar-a"
                                            tabIndex="3"
                                        >
                                            <i className="material-icons">
                                                <MaterialIcon icon="statistics" />
                                            </i>
                                            <span className="isw-sideBar-spanText">
                                        Order Management
                                    </span>
                                        </NavLink>
                                    </li>

                                    {/*<li className="isw-sideBar-li" role="menuitem">*/}
                                        {/*<NavLink*/}
                                            {/*activeClassName="isw-sideBar-liActive"*/}
                                            {/*to={`${url}profile-info`}*/}
                                            {/*className="isw-sideBar-a"*/}
                                            {/*tabIndex="5"*/}
                                        {/*>*/}
                                            {/*<IconFolder />*/}
                                            {/*<span className="isw-sideBar-spanText">*/}
                                        {/*Profile Info*/}
                                    {/*</span>*/}
                                        {/*</NavLink>*/}
                                    {/*</li>*/}

                                    <li className="isw-sideBar-li" role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}vendor-user-permissions`}
                                            className="isw-sideBar-a"
                                            tabIndex="5"
                                        >
                                            <i className="material-icons">
                                                <MaterialIcon icon="settings" />
                                            </i>
                                            <span className="isw-sideBar-spanText">System
                                    </span>
                                        </NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </aside>
            </Fragment>
        );
    }
}

export default OvhMenu;





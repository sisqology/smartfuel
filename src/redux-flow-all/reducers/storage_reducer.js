import {
    READ_FAILURE,
    READ_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET,
    ENCRYPT_USER
} from "../arsVariables";
import { decryptAndRead } from "../services/localStorageHelper";

const role = decryptAndRead(ENCRYPT_USER);
console.log(role);
const storage_reducer = (state = {}, action) => {
    const { type, role, ...rest } = action;
    const data = { ...rest };
    console.log({ action }, data);
    switch (type) {
        case READ_SUCCESS:
            return { storage: SUCCESS, role: role, data };
        case READ_FAILURE:
            return { storage: FAILURE, data: null };

        case RESET:
        // return { role:  };
        default:
            return state;
    }
};

export default storage_reducer;

import React, { Component } from "react";
import { connect } from "react-redux";
// import { Link } from "react-router-dom";
import { Actions } from "../../../redux-flow-all/actions/index";
import { ToastContainer, toast } from "react-toastify";

// styles
import "react-toastify/dist/ReactToastify.min.css";
import "../../../scss/pending.scss";
import {
    SUCCESS,
    FAILURE,
    ENCRYPT_USER
} from "../../../redux-flow-all/arsVariables";
import { history } from "../../../reuse/history";
import Static from "../../../reuse/Static";
// import { validateEmail } from "../../../reuse/regexes";
import { decryptAndRead } from "../../../redux-flow-all/services/localStorageHelper";

// Assets

class ChangePW extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            currentPW: "",
            token: "",
            password: "",
            confirmPassword: "",
            validPassword: null,
            pending: false,
            errorShown: false,
            firstPassInput: false,
            firstCPInput: false,
            upper: false,
            lower: false,
            num: false,
            minEight: false
        };
    }
    componentDidMount() {
        const { fakeAuth } = this.props;
        const storage = decryptAndRead(ENCRYPT_USER);
        console.log({ fakeAuth, history, storage }, this.props);
        const token = storage.access_token;
        this.setState({ token });
    }
    static showToast = (message, type) => {
        if (toast.isActive(ChangePW.toastId)) {
            return toast.update(ChangePW.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (ChangePW.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (ChangePW.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const { change, change_data } = props;
        console.log({ change_data, props });

        // if reset is successful
        if (change === SUCCESS && errorShown === false && pending === true) {
            ChangePW.showToast(change_data, "success");
            setTimeout(() => {
                props.dispatch(Actions.logoutUser());
            }, 2000);

            return {
                pending: false,
                errorShown: true
            };
        }

        // if reset fails
        if (change === FAILURE && pending === true && errorShown === false) {
            ChangePW.showToast(change_data, "error");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }
        return state;
    };

    onCurrentPW_Change = ({ target }) => {
        const { value } = target;
        this.setState({ currentPW: value });
    };

    onPassword_Change = ({ target }) => {
        const { value } = target;
        const reUpperCase = /[A-Z]/;
        const reLowerCase = /[a-z]/;
        const reNumber = /[0-9]/;
        const hasUpper = reUpperCase.test(value);
        const hasLower = reLowerCase.test(value);
        const hasNumber = reNumber.test(value);
        const { confirmPassword } = this.state;
        if (value.length > 0) {
            this.setState(prevState => {
                return {
                    ...prevState,
                    firstPassInput: true,
                    password: value,
                    upper: hasUpper ? true : false,
                    lower: hasLower ? true : false,
                    num: hasNumber ? true : false,
                    minEight: value.length >= 8 ? true : false,
                    // testing validity when there is a change
                    validPassword: this._validatePassword(
                        confirmPassword,
                        value
                    )
                        ? "true"
                        : "false"
                };
            });
        } else if (value.length === 0) {
            this.setState({
                password: value,
                upper: false,
                lower: false,
                num: false,
                minEight: false
            });
        }
    };

    onConfirmPassword = ({ target }) => {
        const { value } = target;
        const { password } = this.state;
        if (value.length > 0) {
            this.setState(prevState => {
                return {
                    ...prevState,
                    firstCPInput: true,
                    confirmPassword: value,
                    // testing validity when there is a change
                    validPassword: this._validatePassword(password, value)
                        ? "true"
                        : "false"
                };
            });
        } else if (value.length === 0) {
            this.setState({
                validPassword: "false",
                validPasswordPassword: value
            });
        }
    };

    _validatePassword = (confirmer, password) => {
        // confirmer is existing value in state used to confirm the input is legit
        if (confirmer === "" && password.length >= 8) {
            return false;
        } else if (
            confirmer !== "" &&
            password === confirmer &&
            password.length >= 8
        ) {
            return true;
        }
    };

    onSubmit = e => {
        e.preventDefault();
        const { token, password, currentPW } = this.state;
        this.setState({ pending: true, errorShown: false });

        if (currentPW === password) {
            ChangePW.showToast(
                "New password must be different from the old one",
                "error"
            );
        } else {
            this.props.dispatch(
                Actions.cus_changePW({
                    token,
                    currentPW,
                    password,
                    rtPassword: password
                })
            );
        }
    };

    goBack = () => {
        console.log(history);
        history.goBack();
    };

    render() {
        const {
            validPassword,
            firstPassInput,
            lower,
            upper,
            num,
            minEight,
            firstCPInput,
            currentPW,
            password,
            confirmPassword
        } = this.state;

        const condition = lower && upper && num && minEight;
        return (
            <div className="isw-login">
                <div className="container-fluid p-0">
                    <div className="row no-gutters bg-primary">
                        <Static />

                        <div className="col-lg-6">
                            <ToastContainer autoClose={5000} />
                            <div
                                className="isw-login--middle"
                                style={{
                                    overflowY: "auto"
                                }}
                            >
                                <div className="isw-login--middle-form">
                                    <form
                                        className="row"
                                        style={{
                                            maxWidth: `${30}rem`,
                                            margin: `${0} auto`
                                        }}
                                    >
                                        <div className="col-12">
                                            <header>
                                                <h1 className="text-primary">
                                                    Enter New Password
                                                </h1>
                                                <p>
                                                    Enter your new password to
                                                    continue
                                                </p>
                                            </header>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-3">
                                                <div className="form-field__control">
                                                    <input
                                                        className={`form-field__input k_input`}
                                                        id="exampleField3"
                                                        type="password"
                                                        placeholder="Current Password"
                                                        onChange={
                                                            this
                                                                .onCurrentPW_Change
                                                        }
                                                        onBlur={
                                                            this
                                                                .onCurrentPW_Change
                                                        }
                                                        value={currentPW}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-3">
                                                <div className="form-field__control">
                                                    <input
                                                        className={
                                                            condition &&
                                                            firstPassInput
                                                                ? `form-field__input k_input green`
                                                                : !condition &&
                                                                  firstPassInput
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        id="exampleField0"
                                                        type="password"
                                                        placeholder="New Password"
                                                        onChange={
                                                            this
                                                                .onPassword_Change
                                                        }
                                                        onBlur={
                                                            this
                                                                .onPassword_Change
                                                        }
                                                        onFocus={
                                                            this
                                                                .onPassword_Change
                                                        }
                                                        value={password}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div
                                            className={
                                                !condition && firstPassInput
                                                    ? `validate-cont visible`
                                                    : `validate-cont`
                                            }
                                        >
                                            <span className="validate">
                                                <p
                                                    className={
                                                        lower ? `met` : "red"
                                                    }
                                                >
                                                    One lowercase letter
                                                </p>
                                                <p
                                                    className={
                                                        upper ? `met` : "red"
                                                    }
                                                >
                                                    One uppercase letter
                                                </p>
                                                <p
                                                    className={
                                                        num ? `met` : "red"
                                                    }
                                                >
                                                    One number
                                                </p>
                                                <p
                                                    className={
                                                        minEight ? `met` : "red"
                                                    }
                                                >
                                                    Minimum of 8 characters
                                                </p>
                                            </span>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-5">
                                                <div className="form-field__control">
                                                    <input
                                                        className={
                                                            validPassword ===
                                                                "true" &&
                                                            firstCPInput
                                                                ? `form-field__input k_input green`
                                                                : validPassword ===
                                                                      "false" &&
                                                                  firstCPInput
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        id="exampleField1"
                                                        type="password"
                                                        placeholder="Confirm Password"
                                                        onChange={
                                                            this
                                                                .onConfirmPassword
                                                        }
                                                        onBlur={
                                                            this
                                                                .onConfirmPassword
                                                        }
                                                        onFocus={
                                                            this
                                                                .onConfirmPassword
                                                        }
                                                        value={confirmPassword}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <button
                                                onClick={this.onSubmit}
                                                disabled={
                                                    condition &&
                                                    validPassword === "true" &&
                                                    currentPW !== ""
                                                        ? false
                                                        : true
                                                }
                                                className={
                                                    condition &&
                                                    validPassword === "true" &&
                                                    currentPW !== ""
                                                        ? `isw-btn isw-btn--raised bg-primary text-white w-100`
                                                        : `isw-btn  bg-primary text-white w-100 false`
                                                }
                                            >
                                                <span>Change Password</span>
                                            </button>
                                        </div>
                                    </form>
                                    <button onClick={this.goBack}>
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-2">
                            <div className="isw-column--right" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { change, change_data } = state.cus_changePW_reducer;
    return {
        change,
        change_data
    };
};

export default connect(mapStateToProps)(ChangePW);

import {
    RM_USER_PERMISSION_FAILURE,
    RM_USER_PERMISSION_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../../../redux-flow-all/arsVariables";

const cus_removeUserPermission_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case RM_USER_PERMISSION_SUCCESS:
            return {
                ...state,
                remove_up: SUCCESS,
                remove_up_data: message
            };
        case RM_USER_PERMISSION_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                remove_up: FAILURE,
                remove_up_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_removeUserPermission_reducer;

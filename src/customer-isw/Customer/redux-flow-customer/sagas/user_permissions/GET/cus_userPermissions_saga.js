import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../../../redux-flow-all/services/services";

import {
    GET_USER_PERMISSIONS,
    GET_USER_PERMISSIONS_FAILURE,
    GET_USER_PERMISSIONS_SUCCESS,
    ENCRYPT_USER
} from "../../../../../../redux-flow-all/arsVariables";
import { decryptAndRead } from "../../../../../../redux-flow-all/services/localStorageHelper";

function* userPermissions(payload) {
    const decryptedToken = decryptAndRead(ENCRYPT_USER).access_token;
    console.log({ decryptedToken });
    try {
        const returnedData = yield call(Services.signupService, payload);
        const { error, status, data } = returnedData;
        const { code, description } = data;
        console.log({ returnedData, data });
        if (error === false && status === 200 && code === 1) {
            return yield put({
                type: GET_USER_PERMISSIONS_SUCCESS,
                message: description
            });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({
                type: GET_USER_PERMISSIONS_FAILURE,
                message: description
            });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({
                type: GET_USER_PERMISSIONS_FAILURE,
                message: description
            });
        }
    } catch (error) {
        console.log({ error });

        const { response } = error;
        if (response) {
            const { data, status } = response;
            if (status === 400) {
                return yield put({
                    type: GET_USER_PERMISSIONS_FAILURE,
                    message: data.error_description
                });
            }
        } else if (response === undefined) {
            return yield put({
                type: GET_USER_PERMISSIONS_FAILURE,
                message: "Failed to connect"
            });
        }
    }
}

export default function* cus_userPermissionsSaga() {
    yield takeLatest(GET_USER_PERMISSIONS, userPermissions);
}

import {
    CHANGE_SUCCESS,
    CHANGE_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_changePW_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case CHANGE_SUCCESS:
            return {
                ...state,
                change: SUCCESS,
                change_data: message
            };
        case CHANGE_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                change: FAILURE,
                change_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_changePW_reducer;

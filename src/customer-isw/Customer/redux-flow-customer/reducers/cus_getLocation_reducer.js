import {
    GET_LOCATION_FAILURE,
    GET_LOCATION_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_getLocation_reducer = (state = {}, action) => {
    const { type, message, code } = action;
    switch (type) {
        case GET_LOCATION_SUCCESS:
            return { get_location: SUCCESS, get_location_data: message };
        case GET_LOCATION_FAILURE:
            // message here is the error message from the server
            return {
                get_location: FAILURE,
                get_location_data: message
                    ? message
                    : "Bad Network Connectivity"
            };
        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_getLocation_reducer;

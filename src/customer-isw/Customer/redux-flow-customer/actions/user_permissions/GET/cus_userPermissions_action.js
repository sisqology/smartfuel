import { GET_USER_PERMISSIONS } from "../../../../../../redux-flow-all/arsVariables";

const cus_userPermissions = payload => ({
    type: GET_USER_PERMISSIONS,
    payload
});

export default cus_userPermissions;

import { CHANGE_PW } from "../../../../redux-flow-all/arsVariables";

const cus_changePW = payload => ({
    type: CHANGE_PW,
    payload
});

export default cus_changePW;

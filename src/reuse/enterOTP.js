import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { ToastContainer, toast } from "react-toastify";

// styles
import "../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";

import Static from "./Static";
import ProgressBar from "./progressBar";
import { Actions } from "../redux-flow-all/actions/index";
import { history as HISTORY } from "./history";
import { SUCCESS, FAILURE } from "../redux-flow-all/arsVariables";
// Assets
class EnterOTP extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            pending: false,
            redirectToReferrer: false,
            errorShown: false,
            code: ""
        };
    }

    componentDidMount() {
        console.log({ HISTORY });
        const { fakeAuth, history, location } = this.props;
        console.log({ fakeAuth, history, location });
        if (fakeAuth.isAuthenticated) {
            history.goBack("/customer/user-list");
        }
        if (location.state) {
            this.setState({ email: location.state.email });
        }
    }

    static showToast = (message, type) => {
        if (toast.isActive(EnterOTP.toastId)) {
            return toast.update(EnterOTP.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (EnterOTP.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (EnterOTP.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const {
            verify,
            verify_data,
            resend_data,
            resend_error_code,
            verify_error_code,
            OTP,
            fakeAuth
        } = props;

        console.log({ resend_data, verify_data }, "from verify reducer");

        // if verification is successful
        if (verify === SUCCESS && errorShown === false && pending === true) {
            EnterOTP.showToast(verify_data, "success");
            console.log(props.email);
            setTimeout(() => {
                fakeAuth.authenticate();
                props.history.push({
                    pathname: "/customer",
                    state: {
                        email: props.email
                    }
                });
            }, 4000);

            props.dispatch(Actions.reset());
            return {
                redirectToReferrer: true,
                pending: false,
                errorShown: true
            };
        }

        // if verification fails
        if (verify === FAILURE && errorShown === false && pending === true) {
            fakeAuth.signout();
            if (verify_data && verify_error_code === -2) {
                fakeAuth.signout();
                EnterOTP.showToast(verify_data, "error");
                setTimeout(() => {
                    props.history.push("/customer/signup");
                }, 2000);
            } else {
                fakeAuth.signout();
                EnterOTP.showToast(verify_data, "error");
            }

            props.dispatch(Actions.reset());
            return {
                redirectToReferrer: false,
                pending: false,
                errorShown: true
            };
        }

        // if RESEND is successful
        if (OTP === SUCCESS && errorShown === false && pending === true) {
            EnterOTP.showToast(resend_data, "success");
            props.dispatch(Actions.reset());
            return {
                redirectToReferrer: true,
                pending: false,
                errorShown: true
            };
        }

        // if RESEND fails
        if (OTP === FAILURE && errorShown === false && pending === true) {
            fakeAuth.signout();
            if (resend_data && resend_error_code === -2) {
                EnterOTP.showToast(resend_data, "error");
                setTimeout(() => {
                    props.history.push("/customer/signup");
                }, 2000);
            } else {
                EnterOTP.showToast(resend_data, "error");
            }
            props.dispatch(Actions.reset());
            return {
                redirectToReferrer: false,
                pending: false,
                errorShown: true
            };
        }
        return null;
    };

    onOTP_Change = e => {
        this.setState({ code: e.target.value });
    };

    submitOTP = e => {
        e.preventDefault();
        this.setState({ pending: true, errorShown: false });
        const { code } = this.state;
        let email;

        // is user doesn't verify and attempt to login
        if (this.props.email) {
            email = this.props.email;
        } else {
            email = this.state.email;
        }
        this.props.dispatch(Actions.cus_verifyProfile({ code, email }));
    };

    reSendOTP = () => {
        this.setState({ pending: true, errorShown: false });
        let email;

        if (this.props.email) {
            email = this.props.email;
        } else {
            email = this.state.email;
        }
        const description = `request to send OTP to ${email}`;
        this.props.dispatch(
            Actions.cus_resendOTP({
                email,
                description
            })
        );
    };

    render() {
        const { phone, location } = this.props;
        let email;

        // is user doesn't verify and attempt to login... getting their email back
        if (this.props.email) {
            email = this.props.email;
        } else {
            email = this.state.email;
        }
        const { code } = this.state;

        let new_number = null;
        if (phone) {
            const phone_number = phone.split("");
            const length = phone_number.length;
            if (length === 14) {
                phone_number.splice(4, 6, "*".repeat(6));
                new_number = phone_number.join("");
                console.log({ new_number, phone_number });
            } else {
                phone_number.splice(3, 6, "*".repeat(6));
                new_number = phone_number.join("");
                console.log({ new_number, phone_number });
            }
        }

        return (
            <div className="isw-login">
                <ToastContainer autoClose={5000} />
                <div className="container-fluid p-0">
                    <div className="row no-gutters">
                        <Static />

                        <div className="col-lg-6">
                            <div className="isw-login--middle">
                                <div
                                    className="isw-login--middle-form"
                                    style={{
                                        display: "flex",
                                        alignItems: "center"
                                    }}
                                >
                                    <form
                                        className="row"
                                        style={{
                                            maxWidth: `${30}rem`,
                                            margin: `${0} auto`
                                        }}
                                    >
                                        <div className="col-12">
                                            <header>
                                                <h1 className="text-primary">
                                                    Almost Done!
                                                </h1>
                                                <p>One more information</p>
                                            </header>
                                        </div>

                                        <div className="col-12">
                                            <ProgressBar />
                                        </div>

                                        <div className="col-12 mb-5">
                                            <div className="isw-login--infoBox rounded">
                                                <p className="mb-0 small">
                                                    An OTP has been sent to{" "}
                                                    <span
                                                        style={{
                                                            fontWeight: 700
                                                        }}
                                                    >
                                                        {email ? email : null}
                                                    </span>
                                                    . Enter the code to verify
                                                    your profile.
                                                </p>
                                            </div>

                                            {phone ? (
                                                <div className="my-4 w-100 text-center">
                                                    <h3>Or</h3>
                                                </div>
                                            ) : null}
                                        </div>

                                        {phone ? (
                                            <div className="col-12">
                                                <p>
                                                    Enter the code sent to{" "}
                                                    <span className="font-weight-bold">
                                                        {new_number}
                                                    </span>{" "}
                                                    below
                                                </p>
                                            </div>
                                        ) : null}

                                        <div className="col-12">
                                            <div className="form-field mb-2">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField1"
                                                        onChange={
                                                            this.onOTP_Change
                                                        }
                                                        onBlur={
                                                            this.onOTP_Change
                                                        }
                                                        value={code}
                                                        className="form-field__input k_input"
                                                        type="text"
                                                        placeholder="Enter Code"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            className="mb-4 text-center"
                                            style={{ paddingLeft: `${15}px` }}
                                        >
                                            <p>
                                                Didn't receive the OTP?{" "}
                                                <span
                                                    className="text-primary font-weight-bold csr"
                                                    onClick={this.reSendOTP}
                                                >
                                                    Resend OTP
                                                </span>
                                            </p>
                                        </div>

                                        <div className="col-12">
                                            <button
                                                onClick={this.submitOTP}
                                                disabled={
                                                    code === "" ? true : false
                                                }
                                                className={
                                                    code !== ""
                                                        ? `isw-btn isw-btn--raised bg-primary text-white w-100`
                                                        : `isw-btn  bg-primary text-white w-100 false`
                                                }
                                            >
                                                <span>Verify Profile</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-2">
                            <div className="isw-column--right" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const {
        verify,
        verify_data,
        verify_error_code
    } = state.cus_verifyProfile_reducer;
    const { email, phone } = state.cus_signup_reducer;

    console.log({ state });
    const { resend_data, resend_error_code, OTP } = state.cus_resendOTP_reducer;
    return {
        verify,
        verify_data,
        email,
        phone,
        resend_error_code,
        resend_data,
        verify_error_code,
        OTP
    };
};

export default connect(mapStateToProps)(EnterOTP);

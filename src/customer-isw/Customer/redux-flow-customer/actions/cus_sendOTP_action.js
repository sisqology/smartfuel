import { SEND_OTP } from "../../../../redux-flow-all/arsVariables";

const cus_sendOTP = payload => ({
    type: SEND_OTP,
    payload
});

export default cus_sendOTP;

import { combineReducers } from "redux";
import storage_reducer from "./storage_reducer";
import login_reducer from "./login_reducer";
import logout_reducer from "./logout_reducer";

// handling CUSTOMER admin
import cus_verifyProfile_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_verifyProfile_reducer";
import cus_signup_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_signup_reducer";
import cus_resetPW_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_resetPW_reducer";
// import sendOTP_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/sendOTP_reducer";
import cus_resendOTP_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_resendOTP_reducer";
import cus_forgotPW_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_forgotPW_reducer";
import cus_changePW_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_changePW_reducer";

/**
 *
 * CUSTOMER USER
 *
 */
// handling user
import cus_createUser_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_createUser_reducer";
import cus_getLocation_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_getLocation_reducer";
import cus_getLocations_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_getLocations_reducer";
import cus_createLocation_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_createLocation_reducer";
import cus_editBasicInfo_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_editBasicInfo_reducer";
import cus_getUsers_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_getUsers_reducer";
import cus_getCustomers_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/cus_getCustomers_reducer";

// handling permissions
import cus_permissions_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/user_permissions/GET/cus_permissions_reducer";
import cus_userPermissions_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/user_permissions/GET/cus_userPermissions_reducer";
import cus_roles_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/user_permissions/GET/cus_roles_reducer";
import cus_getRemoveUserPermissions_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/user_permissions/GET/cus_getRemoveUserPermissions_reducer";
import cus_batchUpdateUserPermissions_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/user_permissions/POST/cus_batchUpdateUserPermissions_reducer";
import cus_changeUserRole_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/user_permissions/POST/cus_changeUserRole_reducer";
import cus_removeUserPermission_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/user_permissions/POST/cus_removeUserPermission_reducer";
import cus_removeUserPermissions_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/user_permissions/POST/cus_removeUserPermissions_reducer";
import cus_updateUserPermissions_reducer from "../../customer-isw/Customer/redux-flow-customer/reducers/user_permissions/POST/cus_updateUserPermissions_reducer";
import rootReducer from "../../ovh-fleet/rootReducer";
import {createVendorReducer, getUsersReducer} from "../../ovh-fleet/ovh/redux-flow-ovh/reducers/users.reducers";

/**
 *
 * ISW reducers
 *
 */
import isw_createVendor_reducer from "../../customer-isw/ISW/redux-flow-isw/reducers/isw_createVendor_reducer";
import {createFleetOwnerReducer, getFleetsReducer} from "../../ovh-fleet/ovh/redux-flow-ovh/reducers/fleet.reducers";
import {updateProfileReducer} from "../../ovh-fleet/ovh/redux-flow-ovh/reducers/profile.reducers";
const IndexReducer = combineReducers({
    // handling responses from storage saga
    storage_reducer,
    // handling responses from login_saga
    login_reducer,
    // handling responses from logout_saga
    logout_reducer,
    // handling responses from verifyProfile_saga
    cus_verifyProfile_reducer,
    // handling responses from signup_saga
    cus_signup_reducer,
    // handling responses from resetPW_saga
    cus_resetPW_reducer,
    // handling responses from resendOTP_saga
    cus_resendOTP_reducer,
    // handling response from forgotPW_saga
    cus_forgotPW_reducer,
    // handling response from changePW_saga
    cus_changePW_reducer,
    // handling response from cus_createUser_saga
    cus_createUser_reducer,
    // handling response from cus_createLocation_saga
    cus_createLocation_reducer,
    // handling response from cus_editBasicInfo_saga
    cus_editBasicInfo_reducer,
    // handling response from cus_getLocation_saga
    cus_getLocation_reducer,
    // handling response from cus_getLocations_saga
    cus_getLocations_reducer,
    // handling response from cus_getUsers_saga
    cus_getUsers_reducer,
    // handling response from cus_getCustomers_saga
    cus_getCustomers_reducer,

    // GET
    // handling response from cus_permissions_saga
    cus_permissions_reducer,
    // handling response from cus_userPermissions_saga
    cus_userPermissions_reducer,
    // handling response from cus_roles_saga
    cus_roles_reducer,
    // handling response from cus_getRemoveUserPermissions_saga
    cus_getRemoveUserPermissions_reducer,

    // POST
    // handling response from cus_batchUpdateUserPermissions_saga
    cus_batchUpdateUserPermissions_reducer,
    // handling response from cus_changeUserRole_saga
    cus_changeUserRole_reducer,
    // handling response from cus_removeUserPermission_saga
    cus_removeUserPermission_reducer,
    // handling response from cus_removeUserPermissions_saga
    cus_removeUserPermissions_reducer,
    // handling response from cus_updateUserPermissions_saga
    cus_updateUserPermissions_reducer,

    ven_vendor_users: getUsersReducer,
    ven_create_vendor: createVendorReducer,
    ven_vendor_fleets: getFleetsReducer,
    ven_create_fleet: createFleetOwnerReducer,
    ven_update_profile: updateProfileReducer,
    //rootReducer, //TODO: Put in a single file later
    // ISW below
    isw_createVendor_reducer
});

export default IndexReducer;

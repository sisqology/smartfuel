import {
    BATCH_UU_PERMISSIONS_FAILURE,
    BATCH_UU_PERMISSIONS_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../../../redux-flow-all/arsVariables";

const cus_batchUpdateUserPermissions_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case BATCH_UU_PERMISSIONS_SUCCESS:
            return {
                ...state,
                batch_uup: SUCCESS,
                batch_uup_data: message
            };
        case BATCH_UU_PERMISSIONS_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                batch_uup: FAILURE,
                batch_uup_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_batchUpdateUserPermissions_reducer;

import React, { Component, Fragment } from "react";

import { history } from "../../../reuse/history";
class CustomerInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        console.log(this.props, history);
    }

    // static getDerivedStateFromProps = (props, state) => {
    //     console.log({ props });
    //     const { data, total, currentPage } = props;
    //     return { users: data, total: total, currentPage: currentPage };
    // };
    render() {
        const {
            activated,
            companyName,
            createdBy,
            createdOn,
            createdOnUtc,
            customerId,
            emailAddress,
            firstName,
            id,
            isDeleted,
            lastLoginDate,
            lastName,
            modifiedBy,
            modifiedOn,
            phoneNumber,
            primaryLocationId
        } = history.location.state.userFromId[0];
        return (
            <Fragment>
                <ul>
                    <li>activated: {activated}</li>
                    <li>companyName: {companyName}</li>
                    <li>createdBy: {createdBy}</li>
                    <li>createdOn: {createdOn}</li>
                    <li>createdOnUtc: {createdOnUtc}</li>
                    <li>customerId: {customerId}</li>
                    <li>emailAddress: {emailAddress}</li>
                    <li>firstName: {firstName}</li>
                    <li>id: {id}</li>
                    <li>isDeleted: {isDeleted}</li>
                    <li>lastLoginDate: {lastLoginDate}</li>
                    <li>lastName: {lastName}</li>
                    <li>modifiedBy: {modifiedBy}</li>
                    <li>modifiedOn: {modifiedOn}</li>
                    <li>phoneNumber: {phoneNumber}</li>
                    <li>primaryLocationId: {primaryLocationId}</li>
                </ul>

                <button
                    onClick={() =>
                        history.goBack({
                            pathname: "/customer/user-list"
                        })
                    }
                >
                    Go back
                </button>
            </Fragment>
        );
    }
}

export default CustomerInfo;

import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { Icon500 } from "./svgs";
import { history } from './history'
import { fakeAuth } from '../App'
const Page_500 = ({ history, fakeAuth }) => {
    console.log({ history, fakeAuth });
    return (
        <Fragment>
            <div className="isw-error">
                <div className="isw-error-wrapper">
                    <h4>Error: 500 Unexpected Error</h4>
                    <div
                        className="isw-image"
                        style={{ margin: `${3}rem ${1}rem` }}
                    >
                        <Icon500 />
                    </div>
                    <div className="err-text-wrapper">
                        <p className="mb-2">
                            An error ocurred and your request couldn’t be
                            completed.
                        </p>
                        <p>
                            Either check the URL,{" "}
                            <Link to="" className="text-danger">
                                go home
                            </Link>{" "}
                            , or feel free to{" "}
                            <Link to="" className="text-danger">
                                report this issue
                            </Link>{" "}
                            .
                        </p>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default Page_500;

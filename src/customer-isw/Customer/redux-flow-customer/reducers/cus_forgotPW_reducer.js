import {
    FORGOT_SUCCESS,
    FORGOT_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_forgotPW_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case FORGOT_SUCCESS:
            return {
                ...state,
                forgot: SUCCESS,
                forgot_data: message
            };
        case FORGOT_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                forgot: FAILURE,
                forgot_data: message ? message : "Bad Network Connectivity"
            };


        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_forgotPW_reducer;

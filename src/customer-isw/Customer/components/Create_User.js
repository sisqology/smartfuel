import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { ToastContainer, toast } from "react-toastify";

// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";
import Sidebar from "../../../reuse/sidebar";
import { Actions } from "../../../redux-flow-all/actions/index";

import {
    areDigits,
    validateEmail,
    validateNumber
} from "../../../reuse/regexes";

import { SUCCESS, FAILURE } from "../../../redux-flow-all/arsVariables";
import { history } from "../../../reuse/history";
// Assets
class CreateUser extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            phone: "",
            f_name: "",
            l_name: "",
            username: "",
            validEmail: null,
            validPhone: null,
            role: null,
            clicked: false,
            location: null,
            searchString: "",
            locations: [],

            pending: false,
            errorShown: false,
            redirect: false
        };
    }

    componentDidMount() {
        const { dispatch, allowed, role, fakeAuth } = this.props;
        console.log(this.props, "KAMSIIIIIIIII", { history, allowed, role });

        if (role === allowed && fakeAuth.isAuthenticated) {
            this.setState({ pending: true, errorShown: false });
            dispatch(Actions.cus_getLocations());
        } else if (role !== allowed && fakeAuth.isAuthenticated) {
            this.setState({ redirect: true });
            console.log("HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    resetClick = () => {
        this.setState({
            clicked: false
        });
    };

    static showToast = (message, type) => {
        if (toast.isActive(CreateUser.toastId)) {
            return toast.update(CreateUser.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (CreateUser.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (CreateUser.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const {
            fakeAuth,
            createuser,
            createuser_data,
            payload,
            get_locations_data,
            get_locations
        } = props;

        console.log({ props, state, payload });

        // if create is successful
        if (
            createuser === SUCCESS &&
            errorShown === false &&
            pending === true
        ) {
            CreateUser.showToast(createuser_data, "success");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }

        // if create fails
        if (
            createuser === FAILURE &&
            errorShown === false &&
            pending === true
        ) {
            CreateUser.showToast(createuser_data, "error");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }
        // if access denied
        if (
            createuser === FAILURE &&
            errorShown === false &&
            pending === true
        ) {
            CreateUser.showToast(createuser_data, "error");
            props.dispatch(Actions.reset());

            return {
                redirectToReferrer: false,
                pending: false,
                errorShown: true
            };
        }
        if (get_locations === SUCCESS) {
            props.dispatch(Actions.reset());
            return {
                locations: payload
            };
        }
        return null;
    };

    onUsername_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            username: value
        });
    };

    onEmail_Change = ({ target }) => {
        const { value } = target;

        const isValidEmail = validateEmail(value);

        this.setState({
            email: value,
            validEmail: isValidEmail ? "true" : "false"
        });
    };

    onFirstname_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            f_name: value
        });
    };
    onLastname_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            l_name: value
        });
    };

    onPhone_Change = ({ target }) => {
        const { value } = target;
        const isValidPhone = validateNumber(value);
        console.log(areDigits(value));
        if (areDigits(value)) {
            this.setState({
                phone: value,
                validPhone: isValidPhone ? "true" : "false"
            });
        }
    };

    onRoleAssign = e => {
        // this.
    };

    goBack = () => {
        history.push({ pathname: "/customer/user-list" });
    };
    searching = ({ target }) => {
        const { value } = target;
        this.setState({ searchString: value });
    };
    selectLocation = () => {
        this.setState({ clicked: true });
    };

    locationPicked = value => {
        console.log(value);
        this.setState({
            location: value,
            clicked: false
        });
    };

    handleSave = e => {
        e.preventDefault();
        this.setState({ pending: true, errorShown: false });
        const { email, phone, f_name, l_name, location } = this.state;
        this.props.dispatch(
            Actions.cus_createUser({
                firstname: f_name,
                lastname: l_name,
                phoneno: phone,
                locationId: location,
                emailaddress: email
            })
        );
    };
    render() {
        const {
            validEmail,
            locations,
            validPhone,
            email,
            phone,
            location,
            f_name,
            l_name,
            clicked,
            searchString,
            redirect
        } = this.state;
        console.log({ clicked, locations });

        const filteredLocations = locations.filter(location =>
            location.address.toLowerCase().includes(searchString.toLowerCase())
        );

        if (redirect) {
            return <Redirect to={{ pathname: "/#/403" }} />;
        }
        return (
            <Fragment>
                <div className="isw-mainLayout1" id="body">
                    <ToastContainer autoClose={5000} />
                    <aside className="isw-sideBar">
                        <Sidebar type="CUS" />
                    </aside>
                    <div />
                    <div className="isw-content--wrapper">
                        <div>
                            <form className="content" id="content-body">
                                <div className="container-fluid container-limited">
                                    <div className="row">
                                        <div className="col-lg-9">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">
                                                        New Staff
                                                    </h3>
                                                    <div className="isw-card-p">
                                                        Personal information of
                                                        the user managing this
                                                        account
                                                    </div>
                                                </div>
                                                <div
                                                    className="card-body p-3"
                                                    style={{
                                                        paddingTop: `${0} !important`
                                                    }}
                                                >
                                                    <div className="row">
                                                        <div className="col-lg-8">
                                                            <div className="row">
                                                                {/* <div className="col-12">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField0"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Username"
                                                                                onChange={
                                                                                    this
                                                                                        .onUsername_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onUsername_Change
                                                                                }
                                                                                value={
                                                                                    username
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div> */}

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField1"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="First Name"
                                                                                onChange={
                                                                                    this
                                                                                        .onFirstname_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onFirstname_Change
                                                                                }
                                                                                value={
                                                                                    f_name
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField2"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Last Name"
                                                                                onChange={
                                                                                    this
                                                                                        .onLastname_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onLastname_Change
                                                                                }
                                                                                value={
                                                                                    l_name
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField3"
                                                                                type="text"
                                                                                placeholder="Phone Number"
                                                                                onChange={
                                                                                    this
                                                                                        .onPhone_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onPhone_Change
                                                                                }
                                                                                value={
                                                                                    phone
                                                                                }
                                                                                className={
                                                                                    validPhone ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validPhone ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField4"
                                                                                type="text"
                                                                                className={
                                                                                    validEmail ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validEmail ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                                placeholder="Email Address"
                                                                                onChange={
                                                                                    this
                                                                                        .onEmail_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onEmail_Change
                                                                                }
                                                                                value={
                                                                                    email
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField5"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Select Role"
                                                                                // onChange={this.}
                                                                                // onBlur={this.}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="my-4">
                                                                        <div className="isw-card">
                                                                            <div className="card-footer bg-white">
                                                                                <button
                                                                                    type="button"
                                                                                    className="isw-btn isw-btn--raised bg-primary text-white"
                                                                                    data-toggle="modal"
                                                                                    data-target="#exampleModalCenter"
                                                                                    onClick={
                                                                                        this
                                                                                            .selectLocation
                                                                                    }
                                                                                >
                                                                                    <span>
                                                                                        Select
                                                                                        Location
                                                                                    </span>
                                                                                </button>

                                                                                {location ? (
                                                                                    <span
                                                                                        style={{
                                                                                            marginLeft: `${10}px`
                                                                                        }}
                                                                                    >
                                                                                        <strong>
                                                                                            Selected
                                                                                            Location:
                                                                                        </strong>{" "}
                                                                                        {
                                                                                            location
                                                                                        }
                                                                                    </span>
                                                                                ) : null}

                                                                                <div
                                                                                    id="myDropdown"
                                                                                    className="dropdown-content"
                                                                                    style={{
                                                                                        display: clicked
                                                                                            ? "flex"
                                                                                            : "none",
                                                                                        flexDirection:
                                                                                            "column"
                                                                                    }}
                                                                                >
                                                                                    {filteredLocations.length >
                                                                                    0 ? (
                                                                                        <input
                                                                                            type="text"
                                                                                            placeholder="Search.."
                                                                                            className="form-field__input"
                                                                                            id="myInput"
                                                                                            onChange={
                                                                                                this
                                                                                                    .searching
                                                                                            }
                                                                                        />
                                                                                    ) : (
                                                                                        "No created locations yet"
                                                                                    )}
                                                                                    {filteredLocations.map(
                                                                                        location => {
                                                                                            const {
                                                                                                address,
                                                                                                id
                                                                                            } = location;
                                                                                            return (
                                                                                                <span
                                                                                                    key={id.toString()}
                                                                                                    onClick={() =>
                                                                                                        this.locationPicked(
                                                                                                            address
                                                                                                        )
                                                                                                    }
                                                                                                    value={
                                                                                                        address
                                                                                                    }
                                                                                                >
                                                                                                    {
                                                                                                        address
                                                                                                    }
                                                                                                </span>
                                                                                            );
                                                                                        }
                                                                                    )}
                                                                                </div>
                                                                            </div>
                                                                            <div className="card-body p-3 isw-hoverClick bg-iswLight1" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-md">
                                                            Picture
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md">
                                            <div className="d-block mb-2">
                                                <button
                                                    onClick={this.handleSave}
                                                    className="isw-btn isw-btn--raised bg-primary text-white w-100"
                                                >
                                                    <span>Save</span>
                                                </button>
                                            </div>

                                            <div className="d-block">
                                                <button
                                                    onClick={this.goBack}
                                                    className="isw-btn isw-btn--raised text-primary w-100"
                                                >
                                                    <span>Cancel</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    console.log({ state });
    const { createuser, createuser_data } = state.cus_createUser_reducer;
    const {
        get_locations,
        get_locations_data,
        payload
    } = state.cus_getLocations_reducer;
    return {
        createuser,
        createuser_data,
        get_locations,
        get_locations_data,
        payload
    };
};

export default connect(mapStateToProps)(CreateUser);

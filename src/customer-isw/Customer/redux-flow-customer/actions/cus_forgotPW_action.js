import { FORGOT_RESET } from "../../../../redux-flow-all/arsVariables";

const cus_forgotPW = payload => ({
    type: FORGOT_RESET,
    payload
});

export default cus_forgotPW;

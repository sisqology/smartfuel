import {
    LOGOUT_SUCCESS,
    SUCCESS,
    FAILURE,
    LOGOUT_FAILURE,
    RESET
} from "../arsVariables";

const logout_reducer = (state = {}, action) => {
    switch (action.type) {
        case LOGOUT_SUCCESS:
            return { logout: SUCCESS };
        case LOGOUT_FAILURE:
            return { logout: FAILURE };
        case RESET:
            return {};
        default:
            return state;
    }
};

export default logout_reducer;

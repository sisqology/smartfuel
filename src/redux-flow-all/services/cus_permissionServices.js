const axios = require("axios");
const qs = require("qs");
const baseurl = "https://feulapp.azurewebsites.net/api/";

const header = token => {
    if (token) {
        return {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };
    } else {
        return {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };
    }
};

// Signup User
// export const signupService = payload => {
//     const { customerAdmin, customerInfo } = payload;
//     console.log({ payload });
//     const objectToSend = { customerAdmin, customerInfo };
//     console.log({ objectToSend }, "for signing up");
//     return new Promise((resolve, reject) => {
//         axios
//             .post(`${baseurl}userprofiling/createcustomer`, objectToSend)
//             .then(res => {
//                 console.log({ res });
//                 return resolve({ ...res, error: false });
//             })
//             .catch(({ response }) => {
//                 if (response) {
//                     console.log({ response }, "if");
//                     return reject({
//                         response: response,
//                         error: true
//                     });
//                 } else {
//                     console.log({ response }, "else");
//                     return reject({ error: true, response });
//                 }
//             });
//     });
// };

import React, { Fragment } from "react";
import { IconLogo } from "./svgs";

const Static = () => {
    return (
        <Fragment>
            <div className="col-lg-4">
                <div className="isw-column--left">
                    <div className="isw-column--left-top">
                        {/* <IconLogo /> */}
                        <h1>LOGO</h1>
                    </div>

                    <div className="isw-column--left-base">
                        <section>
                            <h3>Smartfuel</h3>
                            <p className="mb-0">
                                You have just invested in a new vehicle, the one
                                of your dreams, and you take it on a trip across
                                Canada. You only put gas in it and drive.
                            </p>
                            <div className="d-flex mt-4">
                                <a
                                    href="https://www.google.com"
                                    className="text-white small"
                                >
                                    Privacy Policy
                                </a>
                                <span className="small px-3">|</span>
                                <a
                                    href="https://www.google.com"
                                    className="text-white small"
                                >
                                    Contact
                                </a>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default Static;

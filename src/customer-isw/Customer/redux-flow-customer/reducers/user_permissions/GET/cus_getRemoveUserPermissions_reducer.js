import {
    GET_RM_USER_PERMISSIONS_SUCCESS,
    GET_RM_USER_PERMISSIONS_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../../../redux-flow-all/arsVariables";

const cus_getRemoveUserPermissions_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case GET_RM_USER_PERMISSIONS_SUCCESS:
            return {
                ...state,
                get_rm_up: SUCCESS,
                get_rm_up_data: message
            };
        case GET_RM_USER_PERMISSIONS_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                get_rm_up: FAILURE,
                get_rm_up_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_getRemoveUserPermissions_reducer;

const axios = require("axios");
const qs = require("qs");
const baseurl = "https://feulapp.azurewebsites.net/api/";

const header = token => {
    if (token) {
        return {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };
    } else {
        return {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };
    }
};

// create customer user
export const cus_createUserService = ({ payload }, token) => {
    console.log({ payload, token }, "for CREATING USER");
    return new Promise((resolve, reject) => {
        axios
            .post(
                `${baseurl}userprofiling/createcustomeruser`,
                payload,
                header(token)
            )
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// edit basic profile info of customer user
export const cus_editBasicInfoService = ({ payload }, token) => {
    console.log({ payload, token }, "for editing basic profile of USER");
    return new Promise((resolve, reject) => {
        axios
            .post(`${baseurl}userprofiling/editprofile`, payload, header(token))
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// create location
export const cus_createLocationService = ({ payload }, token) => {
    console.log({ payload, token }, "for creating a new location");
    return new Promise((resolve, reject) => {
        axios
            .post(`${baseurl}customer/createlocation`, payload, header(token))
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// get locations
export const cus_getLocationsService = ({ payload }, token) => {
    console.log({ payload, token }, "for getting locations");
    return new Promise((resolve, reject) => {
        axios
            .get(
                `${baseurl}customer/getlocations?searchkeyword&pageindex=1&pagesize=15`,

                header(token)
            )
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// get users
export const cus_getUsersService = ({ payload }, token) => {
    console.log({ payload, token }, "for getting users");
    return new Promise((resolve, reject) => {
        axios
            .get(
                `${baseurl}customer/getusers?searchkeyword&pageindex=1&pagesize=15`,

                header(token)
            )
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// get customers
export const cus_getCustomersService = ({ payload }, token) => {
    console.log({ payload, token }, "for getting customers");
    return new Promise((resolve, reject) => {
        axios
            .get(
                `${baseurl}customer/getcustomers?searchkeyword&pageindex=1&pagesize=15`,

                header(token)
            )
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// get permissions
export const cus_permissionsService = ({ payload }, token) => {
    console.log({ payload, token }, "for getting permissions");
    return new Promise((resolve, reject) => {
        axios
            .get(`${baseurl}Roles/GetPermissions`, header(token))
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// get roles
export const cus_rolesService = ({ payload }, token) => {
    console.log({ payload, token }, "for getting roles");
    return new Promise((resolve, reject) => {
        axios
            .get(`${baseurl}Roles/GetRoles`, header(token))
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// assign user to a location
export const cus_assignUserToLocationService = ({ payload }, token) => {
    console.log({ payload, token }, "for getting roles");
    return new Promise((resolve, reject) => {
        axios
            .post(
                `${baseurl}customer/adduserstolocation`,
                payload,
                header(token)
            )
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

import {
    GET_LOCATIONS_FAILURE,
    GET_LOCATIONS_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_getLocations_reducer = (state = {}, action) => {
    const { type, message, code, payloadReturned } = action;
    switch (type) {
        case GET_LOCATIONS_SUCCESS:
            return {
                get_locations: SUCCESS,
                get_locations_data: message,
                payload: payloadReturned
            };
        case GET_LOCATIONS_FAILURE:
            // message here is the error message from the server
            return {
                get_locations: FAILURE,
                get_locations_data: message
                    ? message
                    : "Bad Network Connectivity"
            };
        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_getLocations_reducer;

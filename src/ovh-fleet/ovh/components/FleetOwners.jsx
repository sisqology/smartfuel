import * as React from "react";
import {Fragment} from "react";
import OvhHeader from "../templates/header";
import {ToastContainer} from "react-toastify";
import OvhMenu from "../templates/menu";
import {IconAddContact, IconFilter} from "../../../reuse/svgs";
import Leye from "../../../assets/images/isw-imageBox.png";
import { connect } from "react-redux";
import {NavLink} from "react-router-dom";
import {history} from "../../../reuse/history";
import {getFleetOwners} from "../redux-flow-ovh/actions/fleet.actions";
import * as OvhConstants from "../redux-flow-ovh/ovh-constants";
import loading from "../../../assets/images/loading.gif";
import {showToast} from "../../shared/Toast";

class FleetOwners extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            buttonToggle: false
        };
        this.goToAddFleetOwner = this.goToAddFleetOwner.bind(this);
        this.toggleButton = this.toggleButton.bind(this);
    }

    goToAddFleetOwner(){
        history.push(`create-fleet-owner`);
    }

    toggleButton(){
        this.setState({ buttonToggle: !this.state.buttonToggle })
    }

    componentDidMount() {
        // this.fetchUsers();
        this.props.dispatch(getFleetOwners());
        console.log(this.state);
    }

    renderElements(object){
        console.error(object.fleets);
        let status = object.fleets.fetch_fleets_status;
        let resp = object.fleets.fetch_fleets_data;
        const show = (this.state.buttonToggle) ? "show" : "" ;

        switch(status){
            case OvhConstants.FETCH_FLEETS_PENDING:
                return(
                    <div className="row mb-4 mt-3">
                        <div className="col-12">
                            <div className="isw-table text-center">
                                <p>&nbsp;</p>
                                <img src={loading} />
                            </div>
                        </div>
                    </div>
                );
            case OvhConstants.FETCH_FLEETS_SUCCESS:
                let fleets = resp.response.data.payload;

                return (
                    <Fragment>
                        <div className="row mb-4 mt-3">
                            <div className="col-12">
                                <div className="isw-table">
                                    <ul className="isw-table--head">
                                        <li className="isw-table--headLi">
                                            <div className="row">
                                                <div className="col-lg-4">
                                                    <span className="isw-table--headItem">Name</span>
                                                </div>
                                                <div className="col-lg-2">
                                                                <span
                                                                    className="isw-table--headItem">Phone Number</span>
                                                </div>
                                                <div className="col-lg-2 text-right">
                                                    <span className="isw-table--headItem">Status</span>
                                                </div>
                                                <div className="col-lg-2 text-right">
                                                    <span className="isw-table--headItem">Actions</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>


                                    <ul className="isw-table--body">
                                        {fleets.map(fleet => (
                                            <li className="isw-table--bodyLi" key={fleet.vendorId}>
                                                <a href="#" className="row isw-table--bodyA">
                                                    <div className="col-lg-4 isw-valign--middle">
                                                        <div
                                                            className="isw-table--bodyItem isw-table--bodyItem-img">
                                                            <div className="isw-image isw-valign--middle mr-3"
                                                                 style={{
                                                                     height: `${2.5}rem`,
                                                                     width: `${2.5}rem`,
                                                                     borderRadius: `${100}%`,
                                                                     overflow:
                                                                         "hidden"
                                                                 }}>
                                                                <img
                                                                    src={
                                                                        Leye
                                                                    }
                                                                    alt="leye"
                                                                />

                                                            </div>
                                                            <div className="isw-valign--middle">
                                                                <h3 className="isw-subtitle">{fleet.fullName}</h3>
                                                                <p className="isw-p2 mb-0">{fleet.username || fleet.emailAddress}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-2 isw-valign--middle">
                                                        <span className="isw-p2">{fleet.phoneNo}</span>
                                                    </div>
                                                    <div className="col-lg-2 isw-valign--middle">
                                                        <span className="isw-p2">{fleet.role}</span>
                                                    </div>

                                                    <div className="col-lg-2 isw-valign--middle text-right">
                                                        <span className="isw-p2">Active</span>
                                                    </div>
                                                </a>
                                                <div className="col-lg-2 isw-valign--middle text-right pull-right">
                                                    <div className="btn-group">
                                                        <button type="button" className="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false" onClick={ this.toggleButton }>
                                                            Action
                                                        </button>
                                                        <div className={"dropdown-menu " + show}>
                                                            <button className="dropdown-item" href="#">Activate/Disable</button>
                                                            <button className="dropdown-item" href="#">Edit User Permissions</button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <nav aria-label="pagination" className="float-right">
                                    <ul className="pagination">
                                        <li className="page-item disabled"><span
                                            className="page-link">Previous</span></li>
                                        <li className="page-item"><a className="page-link" href="#">1</a>
                                        </li>
                                        <li className="page-item active" aria-current="page">
                                                    <span className="page-link">2<span
                                                        className="sr-only">(current)</span></span>
                                        </li>
                                        <li className="page-item"><a className="page-link" href="#">3</a>
                                        </li>
                                        <li className="page-item"><a className="page-link" href="#">Next</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </Fragment>
                );
            case OvhConstants.FETCH_FLEETS_FAILURE:
                return(

                    <div className="row mb-4 mt-3">
                        {showToast("Unable to fetch users", "error")}
                        <div className="col-12">
                            <div className="isw-table">
                                <ul className="isw-table--head">
                                    <li className="isw-table--headLi">
                                        <div className="row">
                                            <div className="col-lg-4">
                                                <span className="isw-table--headItem">Name</span>
                                            </div>
                                            <div className="col-lg-2">
                                                                <span
                                                                    className="isw-table--headItem">Phone Number</span>
                                            </div>

                                            <div className="col-lg-2 text-right">
                                                <span className="isw-table--headItem">Status</span>
                                            </div>
                                            <div className="col-lg-2 text-right">
                                                <span className="isw-table--headItem">Actions</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                );
        }

    }


    render() {
        const resp = this.props;
        const url = "/vendor/";
        let objects  = this.props;
        return (
            <Fragment>
                <div id="">
                    <OvhHeader />
                </div>
                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000}/>
                    <OvhMenu/>
                    <div></div>
                    <div className="isw-content--wrapper">
                        <div>
                            <div className="content" id="content-body">
                                <div className="container-fluid container-limited">

                                    <div className="row my-4">
                                        <div className="col-lg-6 isw-valign--middle mr-auto">
                                            <p className="isw-p mb-0">Users list (showing 1-8 of 26 records)</p>
                                        </div>

                                        <div className="col-lg-3 text-right">
                                            <button className="isw-btn border w-100" data-toggle="modal"
                                                    data-target="#exampleModalCenter">
                                                <IconFilter /><span>Filter Table</span>
                                            </button>
                                        </div>

                                        <div className="col-md text-right">
                                            <button className="isw-btn isw-btn--raised bg-primary text-white w-100" onClick={this.goToAddFleetOwner}>
                                                <IconAddContact /><span>Add Fleet Owner</span>
                                            </button>
                                        </div>
                                    </div>
                                    {this.renderElements(objects)}

                                    <div className="row">
                                        <div className="col-12">
                                            <nav aria-label="pagination" className="float-right">
                                                <ul className="pagination">
                                                    <li className="page-item disabled"><span
                                                        className="page-link">Previous</span></li>
                                                    <li className="page-item"><a className="page-link" href="#">1</a>
                                                    </li>
                                                    <li className="page-item active" aria-current="page">
                                                        <span className="page-link">2<span
                                                            className="sr-only">(current)</span></span>
                                                    </li>
                                                    <li className="page-item"><a className="page-link" href="#">3</a>
                                                    </li>
                                                    <li className="page-item"><a className="page-link" href="#">Next</a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state){
    return {
        fleets: state.ven_vendor_fleets,
    };
}

export default connect(mapStateToProps)(FleetOwners);

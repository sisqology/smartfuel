import React, { Fragment, Component } from "react";
// import { Actions } from "../../redux-flow-all/actions";
import { history } from "./history";
// import { fakeAuth } from "../../App";
// import { clear } from "../../redux-flow-all/services/localStorageHelper";

class Header extends Component {
    componentDidMount() {
        console.log(this.props);
    }
    changePassword = () => {
        history.push("/settings/changePassword");
    };
    render() {
        const { logout } = this.props;

        return (
            <Fragment>
                <header
                    className="isw-nav--header"
                    style={{
                        display: "flex",
                        color: "#fff",
                        justifyContent: "space-between",
                        alignItems: "center",
                        height: `${43}px`,
                        backgroundColor: "#00425f"
                    }}
                >
                    <div className="">header</div>
                    <button
                        onClick={this.changePassword}
                        style={{
                            border: `${5}px`,
                            color: "white",
                            backgroundColor: "green",
                            height: `${30}px`,
                            cursor: "pointer",
                            marginLeft: `${200}px`
                        }}
                    >
                        Change Password
                    </button>
                    <button
                        onClick={logout}
                        style={{
                            border: `${5}px`,
                            color: "white",
                            backgroundColor: "#0082c8",
                            width: `${100}px`,
                            height: `${30}px`,
                            cursor: "pointer",
                            marginLeft: `${200}px`
                        }}
                    >
                        Signout
                    </button>
                </header>
            </Fragment>
        );
    }
}

export default Header;

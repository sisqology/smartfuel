import {
    GET_CUSTOMERS_FAILURE,
    GET_CUSTOMERS_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_getCustomers_reducer = (state = {}, action) => {
    const { type, message, code, payloadReturned } = action;
    switch (type) {
        case GET_CUSTOMERS_SUCCESS:
            return {
                get_customers: SUCCESS,
                get_customers_data: message,
                payload: payloadReturned
            };
        case GET_CUSTOMERS_FAILURE:
            // message here is the error message from the server
            return {
                get_customers: FAILURE,
                get_customers_data: message
                    ? message
                    : "Bad Network Connectivity"
            };
        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_getCustomers_reducer;

import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import {
    IconFolder,
    IconGear,
    IconStaffs,
    IconLocation,
    IconAddContact
} from "./svgs";

const Sidebar = ({ type }) => {
    const url = "/customer/";
    return (
        <Fragment>
            <div
                className="isw-sideBar-innerScroll"
                style={{ overflowY: "scroll" }}
            >
                {type === "CUS" ? (
                    <div className="isw-sideBar--wrapper">
                        <div className="isw-sideBar--linkBox">
                            <ul>
                                <span className="isw-sideBar-ulSpan" />
                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`/isw/create-vendor`}
                                        className="isw-sideBar-a"
                                        tabIndex="1"
                                    >
                                        <IconFolder />
                                        <span className="isw-sideBar-spanText">
                                            Dashboard
                                        </span>
                                    </NavLink>
                                </li>
                            </ul>

                            <ul>
                                <span className="isw-sideBar-ulSpan" />
                                <li className="isw-sideBar-li " role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}user-list`}
                                        className="isw-sideBar-a"
                                        tabIndex="2"
                                    >
                                        <IconStaffs />
                                        <span className="isw-sideBar-spanText">
                                            Staffs
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}create-location`}
                                        className="isw-sideBar-a"
                                        tabIndex="3"
                                    >
                                        <IconLocation />
                                        <span className="isw-sideBar-spanText">
                                            Location
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}create-user`}
                                        className="isw-sideBar-a "
                                        tabIndex="4"
                                    >
                                        <IconAddContact place="sidebar" />
                                        <span className="isw-sideBar-spanText">
                                            Create User
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}profile-info`}
                                        className="isw-sideBar-a"
                                        tabIndex="5"
                                    >
                                        <IconFolder />
                                        <span className="isw-sideBar-spanText">
                                            Profile Info
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}user-permission`}
                                        className="isw-sideBar-a"
                                        tabIndex="5"
                                    >
                                        <IconGear />
                                        <span className="isw-sideBar-spanText">
                                            User Permission
                                        </span>
                                    </NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                ) : type === "ISW" ? (
                    <div className="isw-sideBar--wrapper">
                        <div className="isw-sideBar--linkBox">
                            <ul>
                                <span className="isw-sideBar-ulSpan" />
                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`/isw/dashboard`}
                                        className="isw-sideBar-a"
                                        tabIndex="1"
                                    >
                                        <IconFolder />
                                        <span className="isw-sideBar-spanText">
                                            Dashboard
                                        </span>
                                    </NavLink>
                                </li>
                            </ul>

                            <ul>
                                <span className="isw-sideBar-ulSpan" />
                                <li className="isw-sideBar-li " role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`/isw/create-vendor`}
                                        className="isw-sideBar-a"
                                        tabIndex="2"
                                    >
                                        <IconAddContact place="sidebar" />
                                        <span className="isw-sideBar-spanText">
                                            Create Vendors
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}create-location`}
                                        className="isw-sideBar-a"
                                        tabIndex="3"
                                    >
                                        <IconLocation />
                                        <span className="isw-sideBar-spanText">
                                            Location
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`/isw/vendors`}
                                        className="isw-sideBar-a "
                                        tabIndex="4"
                                    >
                                        <IconStaffs />

                                        <span className="isw-sideBar-spanText">
                                            Vendors
                                        </span>
                                    </NavLink>
                                </li>

                                {/* <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}profile-info`}
                                        className="isw-sideBar-a"
                                        tabIndex="5"
                                    >
                                        <IconFolder />
                                        <span className="isw-sideBar-spanText">
                                            Profile Info
                                        </span>
                                    </NavLink>
                                </li> */}

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}user-permission`}
                                        className="isw-sideBar-a"
                                        tabIndex="5"
                                    >
                                        <IconGear />
                                        <span className="isw-sideBar-spanText">
                                            System
                                        </span>
                                    </NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                ) : (
                    <div className="isw-sideBar--wrapper">
                        <div className="isw-sideBar--linkBox">
                            <ul>
                                <span className="isw-sideBar-ulSpan" />
                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`isw/dashboard`}
                                        className="isw-sideBar-a"
                                        tabIndex="1"
                                    >
                                        <IconFolder />
                                        <span className="isw-sideBar-spanText">
                                            Dashboard
                                        </span>
                                    </NavLink>
                                </li>
                            </ul>

                            <ul>
                                <span className="isw-sideBar-ulSpan" />
                                <li className="isw-sideBar-li " role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}user-list`}
                                        className="isw-sideBar-a"
                                        tabIndex="2"
                                    >
                                        <IconStaffs />
                                        <span className="isw-sideBar-spanText">
                                            Staffs
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}create-location`}
                                        className="isw-sideBar-a"
                                        tabIndex="3"
                                    >
                                        <IconLocation />
                                        <span className="isw-sideBar-spanText">
                                            Location
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}create-user`}
                                        className="isw-sideBar-a "
                                        tabIndex="4"
                                    >
                                        <IconAddContact place="sidebar" />
                                        <span className="isw-sideBar-spanText">
                                            Create User
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}profile-info`}
                                        className="isw-sideBar-a"
                                        tabIndex="5"
                                    >
                                        <IconFolder />
                                        <span className="isw-sideBar-spanText">
                                            Profile Info
                                        </span>
                                    </NavLink>
                                </li>

                                <li className="isw-sideBar-li" role="menuitem">
                                    <NavLink
                                        activeClassName="isw-sideBar-liActive"
                                        to={`${url}user-permission`}
                                        className="isw-sideBar-a"
                                        tabIndex="5"
                                    >
                                        <IconGear />
                                        <span className="isw-sideBar-spanText">
                                            User Permission
                                        </span>
                                    </NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                )}
            </div>
        </Fragment>
    );
};

export default Sidebar;

import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import { connect } from "react-redux";
import { Actions } from "../../../redux-flow-all/actions/index";
import { ToastContainer, toast } from "react-toastify";
import { history } from "../../../reuse/history";

// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";
import "../../../scss/pending.scss";
import "./signup.scss";
import Static from "../../../reuse/Static";
import ProgressBar from "../../../reuse/progressBar";
import {
    validateEmail,
    validateNumber,
    areDigits
} from "../../../reuse/regexes";
import { SUCCESS, FAILURE } from "../../../redux-flow-all/arsVariables";

// Assets

class SetupProfile extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            company_name: "",
            company_email: "",
            company_contact: "",
            company_phone: "",
            company_address: "",
            company_website: "",
            RC_no: "",
            validName: null,
            validMail: null,
            validNumber: null,
            validAddress: null,
            validSite: null,
            validContact: null,
            validRC: null,
            pending: false,
            redirectToReferrer: false,
            errorShown: false,
            checkBoxValue: false
        };
    }

    componentDidMount() {
        const { fakeAuth, location, history } = this.props;
        console.log({ fakeAuth, history });
        if (fakeAuth.isAuthenticated) {
            history.goBack("/customer/user-list");
        }

        if (location.state) {
            const { STATE } = location.state;
            console.log({ STATE }, this.state);
            this.setState({
                ...this.state,
                ...STATE
            });
        }
    }

    static showToast = (message, type) => {
        if (toast.isActive(SetupProfile.toastId)) {
            return toast.update(SetupProfile.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (SetupProfile.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (SetupProfile.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const { signup, data, email, phone } = props;
        // console.log({ data });

        // if signup is successful
        if (signup === SUCCESS && errorShown === false && pending === true) {
            SetupProfile.showToast(data, "success");
            setTimeout(() => {
                history.push({
                    pathname: "/customer/verify-profile",
                    state: {
                        email,
                        phone
                    }
                });
            }, 3000);
            return {
                redirectToReferrer: true,
                pending: false,
                errorShown: true
            };
        }

        // if signup fails
        if (signup === FAILURE && pending === true && errorShown === false) {
            SetupProfile.showToast(data, "error");
            props.dispatch(Actions.reset());
            return {
                redirectToReferrer: false,
                pending: false,
                errorShown: true
            };
        }
        return state;
    };

    companyName_Handler = ({ target }) => {
        const { value } = target;
        this.setState({
            company_name: value,
            validName: value !== "" ? "true" : "false"
        });
    };

    companyEmail_Handler = ({ target }) => {
        const { value } = target;
        const isvalidMail = validateEmail(value);

        this.setState({
            company_email: value,
            validMail: isvalidMail ? "true" : "false"
        });
    };

    companyAddress_Handler = ({ target }) => {
        const { value } = target;
        this.setState({
            company_address: value,
            validAddress: value !== "" ? "true" : "false"
        });
    };

    companyContact_Handler = ({ target }) => {
        const { value } = target;
        this.setState({
            company_contact: value,
            validContact: value !== "" ? "true" : "false"
        });
    };

    companyWebsite_Handler = ({ target }) => {
        const { value } = target;
        this.setState({
            company_website: value,
            validSite: value !== "" ? "true" : "false"
        });
    };

    companyPhone_Handler = ({ target }) => {
        const { value } = target;
        const isvalidNumber = validateNumber(value);
        console.log(areDigits(value));
        if (areDigits(value)) {
            this.setState({
                company_phone: value,
                validNumber: isvalidNumber ? "true" : "false"
            });
        }
    };

    RC_Handler = ({ target }) => {
        const { value } = target;
        this.setState({
            RC_no: value,
            validRC: value.length > 8 ? "true" : "false"
        });
    };

    checkBox = e => {
        console.log(e.target, e.target.value, e.target.checked);
        this.setState({ checkBoxValue: e.target.checked });
    };

    onSubmit = e => {
        e.preventDefault();
        const {
            email,
            f_name,
            l_name,
            phone,
            password,
            company_name,
            company_email,
            company_contact,
            company_phone,
            company_address,
            company_website,
            RC_no
        } = this.state;

        this.setState({ pending: true, errorShown: false });

        const customerInfo = {
            companyName: company_name,
            emailAddress: company_email,
            contactName: company_contact,
            phoneNo: company_phone,
            address: company_address,
            websiteUrl: company_website,
            regNo: RC_no
        };

        const customerAdmin = {
            emailAddress: email,
            firstName: f_name,
            lastName: l_name,
            phoneNo: phone,
            password: password,
            role: 1
        };

        this.props.dispatch(
            Actions.cus_signupUser({
                customerAdmin,
                customerInfo
            })
        );
    };

    goBack = () => {
        const STATE = this.state;
        history.push({
            pathname: "/customer/signup",
            state: {
                STATE
            }
        });
    };

    render() {
        const {
            redirectToReferrer,
            f_name,
            company_address,
            company_contact,
            company_email,
            company_name,
            company_phone,
            company_website,
            RC_no,
            validNumber,
            validAddress,
            validSite,
            validContact,
            validRC,
            validMail,
            validName,
            pending,
            checkBoxValue
        } = this.state;

        const conditions =
            checkBoxValue &&
            validNumber === "true" &&
            validAddress === "true" &&
            validSite === "true" &&
            validContact === "true" &&
            validRC === "true" &&
            validMail === "true" &&
            validName === "true";
        return (
            <div className="isw-login">
                <ToastContainer autoClose={5000} />
                <div className="container-fluid p-0">
                    <div className="row no-gutters">
                        <Static />
                        <div className="col-lg-6 cont">
                            <div className="isw-login--middle">
                                <div className="isw-login--middle-form">
                                    <form
                                        className="row"
                                        style={{
                                            maxWidth: `${30}rem`,
                                            margin: `${0} auto`
                                        }}
                                    >
                                        <div className="col-12">
                                            <header>
                                                <h1 className="text-primary">
                                                    Hi <span>{f_name}</span>!
                                                </h1>
                                                <p>
                                                    Let setup your company
                                                    information
                                                </p>
                                            </header>
                                        </div>

                                        <div className="col-12">
                                            <div className="pb-3">
                                                <ProgressBar />
                                                <h3>
                                                    Enter Corporate information
                                                    details
                                                </h3>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField1"
                                                        onChange={
                                                            this
                                                                .companyName_Handler
                                                        }
                                                        onBlur={
                                                            this
                                                                .companyName_Handler
                                                        }
                                                        onFocus={
                                                            this
                                                                .companyName_Handler
                                                        }
                                                        value={company_name}
                                                        className={
                                                            validName === "true"
                                                                ? `form-field__input k_input green`
                                                                : validName ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Company Name"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-6">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField2"
                                                        onChange={
                                                            this.RC_Handler
                                                        }
                                                        onBlur={this.RC_Handler}
                                                        onFocus={
                                                            this.RC_Handler
                                                        }
                                                        value={RC_no}
                                                        className={
                                                            validRC === "true"
                                                                ? `form-field__input k_input green`
                                                                : validRC ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="RC Number"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-6">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField0"
                                                        onChange={
                                                            this
                                                                .companyPhone_Handler
                                                        }
                                                        onBlur={
                                                            this
                                                                .companyPhone_Handler
                                                        }
                                                        onFocus={
                                                            this
                                                                .companyPhone_Handler
                                                        }
                                                        value={company_phone}
                                                        className={
                                                            validNumber ===
                                                            "true"
                                                                ? `form-field__input k_input green`
                                                                : validNumber ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Phone Number"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField21"
                                                        onChange={
                                                            this
                                                                .companyContact_Handler
                                                        }
                                                        onBlur={
                                                            this
                                                                .companyContact_Handler
                                                        }
                                                        onFocus={
                                                            this
                                                                .companyContact_Handler
                                                        }
                                                        value={company_contact}
                                                        className={
                                                            validContact ===
                                                            "true"
                                                                ? `form-field__input k_input green`
                                                                : validContact ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Contact Name"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField22"
                                                        onChange={
                                                            this
                                                                .companyEmail_Handler
                                                        }
                                                        onBlur={
                                                            this
                                                                .companyEmail_Handler
                                                        }
                                                        onFocus={
                                                            this
                                                                .companyEmail_Handler
                                                        }
                                                        value={company_email}
                                                        className={
                                                            validMail === "true"
                                                                ? `form-field__input k_input green`
                                                                : validMail ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Email Address"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField222"
                                                        onChange={
                                                            this
                                                                .companyAddress_Handler
                                                        }
                                                        onBlur={
                                                            this
                                                                .companyAddress_Handler
                                                        }
                                                        onFocus={
                                                            this
                                                                .companyAddress_Handler
                                                        }
                                                        value={company_address}
                                                        className={
                                                            validAddress ===
                                                            "true"
                                                                ? `form-field__input k_input green`
                                                                : validAddress ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Office Address"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField2222"
                                                        onChange={
                                                            this
                                                                .companyWebsite_Handler
                                                        }
                                                        onBlur={
                                                            this
                                                                .companyWebsite_Handler
                                                        }
                                                        onFocus={
                                                            this
                                                                .companyWebsite_Handler
                                                        }
                                                        value={company_website}
                                                        className={
                                                            validSite === "true"
                                                                ? `form-field__input k_input green`
                                                                : validSite ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Company Website"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12 mb-4">
                                            <div className="checkbox">
                                                <label>
                                                    <input
                                                        type="checkbox"
                                                        onClick={this.checkBox}
                                                        value={checkBoxValue}
                                                        // defaultChecked={
                                                        //     checkBoxValue
                                                        //         ? true
                                                        //         : false
                                                        // }
                                                        onChange={this.checkBox}
                                                        checked={
                                                            checkBoxValue
                                                                ? true
                                                                : false
                                                        }
                                                    />
                                                    <i className="helper" />
                                                    I've read & agree to the{" "}
                                                    <span
                                                        // href="https://www.google.com"
                                                        // target="_blank"
                                                        // rel="noopener noreferrer"
                                                        style={{
                                                            color: "#00425f",
                                                            fontWeight: 700
                                                        }}
                                                        onClick={() =>
                                                            history.push({
                                                                pathname:
                                                                    "/#/404"
                                                            })
                                                        }
                                                    >
                                                        Terms & Conditons
                                                    </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div
                                            className="col-12"
                                            style={{
                                                padding: 0
                                            }}
                                        >
                                            <div
                                                style={{
                                                    display: "flex",
                                                    justifyContent:
                                                        "space-between",
                                                    marginTop: `${20}px`
                                                }}
                                            >
                                                <div className="col-4">
                                                    <button
                                                        onClick={this.goBack}
                                                        disabled={
                                                            pending
                                                                ? true
                                                                : false
                                                        }
                                                        className={
                                                            pending
                                                                ? `isw-btn  bg-primary text-white w-100 false`
                                                                : `isw-btn isw-btn--raised bg-primary text-white w-100`
                                                        }
                                                    >
                                                        <span>Previous</span>
                                                    </button>
                                                </div>
                                                <div className="col-4">
                                                    {pending ? (
                                                        <button
                                                            className="dot isw-btn bg-primary text-white w-100"
                                                            disabled
                                                        >
                                                            Loading
                                                            <span>.</span>
                                                            <span>.</span>
                                                            <span>.</span>
                                                        </button>
                                                    ) : (
                                                        <button
                                                            onClick={
                                                                this.onSubmit
                                                            }
                                                            disabled={
                                                                conditions
                                                                    ? false
                                                                    : true
                                                            }
                                                            className={
                                                                conditions
                                                                    ? `isw-btn isw-btn--raised bg-primary text-white w-100`
                                                                    : `isw-btn  bg-primary text-white w-100 false`
                                                            }
                                                        >
                                                            <span>Sign Up</span>
                                                        </button>
                                                    )}
                                                </div>
                                            </div>

                                            <div className="mt-4 text-center">
                                                <p>
                                                    Do you have an account
                                                    already?{" "}
                                                    {pending ? (
                                                        <span className="text-primary font-weight-bold">
                                                            Sign in
                                                        </span>
                                                    ) : (
                                                        <Link
                                                            to="/signin"
                                                            className="text-primary font-weight-bold"
                                                        >
                                                            Sign in
                                                        </Link>
                                                    )}
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-2">
                            <div className="isw-column--right" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { signup, data, email, phone } = state.cus_signup_reducer;
    return {
        signup,
        data,
        email,
        phone
    };
};

export default connect(mapStateToProps)(SetupProfile);

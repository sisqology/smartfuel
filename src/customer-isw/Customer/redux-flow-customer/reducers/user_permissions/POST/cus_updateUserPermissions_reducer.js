import {
    UU_PERMISSIONS_FAILURE,
    UU_PERMISSIONS_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../../../redux-flow-all/arsVariables";

const cus_updateUserPermissions_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case UU_PERMISSIONS_SUCCESS:
            return {
                ...state,
                update_up: SUCCESS,
                update_up_data: message
            };
        case UU_PERMISSIONS_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                update_up: FAILURE,
                update_up_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_updateUserPermissions_reducer;

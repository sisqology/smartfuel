import {ApiService} from "../../../api-service";
import {routeApis} from "../../../services-constants";
import * as OVHCONSTANTS from "../ovh-constants";
const axios = require('axios');


export function postFleetOwner(payload){
    return (dispatch) => {
        // let payload = payload;
        // console.log(payload);
        let consume = ApiService.request(routeApis.CREATE_FLEET_OWNER, "POST", payload);
        dispatch(request(consume));
        return consume
            .then(response => {

                if(response.data.code !== 1){
                    // console.log(response.data.description);
                    console.error(response);
                    dispatch(failure(response.data.description));
                }
                else{
                    dispatch(success(response));
                }
            })
            .catch(error => {
                dispatch(failure(error.description));
            });
    };

    function request(request) { return { type:OVHCONSTANTS.CREATE_FLEET_OWNER_PENDING, request} }
    function success(response) { return {type:OVHCONSTANTS.CREATE_FLEET_OWNER_SUCCESS, response} }
    function failure(error) { return {type:OVHCONSTANTS.CREATE_FLEET_OWNER_FAILURE, error} }
}

//
export const getFleetOwners = () => {
    return (dispatch) => {
        let consume = ApiService.request(routeApis.LIST_FLEET_OWNERS, "GET", null);
        dispatch(request(consume));
        return consume
            .then(response => {
                // consume.log(response);
                dispatch(success(response));
            })
            .catch(error => {
                dispatch(failure(error.description));
            });
    };

    function request(request) { return { type:OVHCONSTANTS.FETCH_FLEETS_PENDING, request} }
    function success(response) { return {type:OVHCONSTANTS.FETCH_FLEETS_SUCCESS, response} }
    function failure(error) { return {type:OVHCONSTANTS.FETCH_FLEETS_FAILURE, error} }
};
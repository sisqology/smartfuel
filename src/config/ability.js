import { AbilityBuilder, Ability } from "@casl/ability";
import {
    SUPER_ADMIN,
    CUSTOMER_ADMIN,
    VENDOR_ADMIN
} from "../redux-flow-all/arsVariables";

const defineAbilityFor = role => {
    const { rules, can: allow, cannot: forbid  } = AbilityBuilder.extract();

    if (role === SUPER_ADMIN) {
        allow("view", "/isw");
    } else if (role === CUSTOMER_ADMIN) {
        allow("view", "/customer");
    } else if (role === VENDOR_ADMIN) {
        allow("view", "/vendor");
    }

    return new Ability(rules);
};

export default defineAbilityFor;

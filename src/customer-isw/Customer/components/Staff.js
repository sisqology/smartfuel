import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";

// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";

import Leye from "../../../assets/images/isw-imageBox.png";
import { Actions } from "../../../redux-flow-all/actions/index";
import Sidebar from "../../../reuse/sidebar";

import { IconAddContact, IconFilter } from "../../../reuse/svgs";
import { FAILURE, SUCCESS } from "../../../redux-flow-all/arsVariables";
import { history } from "../../../reuse/history";
import { customerTester } from "../../../reuse/authorization_test";
import Pagination from "../../../reuse/Pagination";
// Assets
class Staff extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            pending: false,
            errorShown: false,
            redirect: false,
            users: [],
            totalCount: 0,
            currentPage: 1,
            usersPerPage: 2
        };
    }

    componentDidMount() {
        const { dispatch, allowed, role, fakeAuth, payload } = this.props;
        // const { role: fromState } = this.props.location.state;
        console.log(this.props, "KAMSIIIIIIIII", {
            history,
            allowed,
            role
            // fromProps,
            // fromState
        });

        // const role = fromState || fromProps;

        if (role === allowed && fakeAuth.isAuthenticated) {
            this.setState({ pending: true, errorShown: false });
            dispatch(Actions.cus_getUsers());
        } else if (role !== allowed && fakeAuth.isAuthenticated) {
            this.setState({ redirect: true });
            console.log("HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    static showToast = (message, type) => {
        if (toast.isActive(Staff.toastId)) {
            return toast.update(Staff.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (Staff.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (Staff.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const { fakeAuth, get_users, get_users_data, payload, total } = props;

        console.log({ props, state, payload, total });

        // if get is successful
        if (get_users === SUCCESS && errorShown === false && pending === true) {
            // Staff.showToast(get_users_data, "success");
            props.dispatch(Actions.reset());
            console.log({ total, state });
            return {
                users: payload,
                totalCount: total,
                pending: false,
                errorShown: true
            };
        }

        // if get fails
        if (get_users === FAILURE && errorShown === false && pending === true) {
            if (get_users_data === null) {
                Staff.showToast("Failed to fetch list", "error");
            }
            Staff.showToast(get_users_data, "error");
            props.dispatch(Actions.reset());
            return {
                users: payload,
                pending: false,
                errorShown: true
            };
        }
        // if access denied
        if (get_users === FAILURE && errorShown === false && pending === true) {
            Staff.showToast(get_users_data, "error");
            props.dispatch(Actions.reset());

            return {
                users: payload,
                pending: false,
                errorShown: true
            };
        }

        return null;
    };

    goToCustomerInfo = id => {
        const { users } = this.state;
        const userFromId = users.filter(user => user.id === id);
        console.log(id, { userFromId });
        history.push({
            pathname: "/customer/staff-info",
            state: { userFromId }
        });
    };

    routeToCreateUser = () => {
        history.push({ pathname: "/customer/create-user" });
    };

    decrement = e => {
        e.preventDefault();
        this.setState(prevState => {
            return { ...prevState, currentPage: prevState.currentPage - 1 };
        });
    };

    handleClick = e => {
        e.preventDefault();
        this.setState({
            currentPage: Number(e.target.id)
        });
    };

    increment = e => {
        e.preventDefault();
        this.setState(prevState => {
            return { ...prevState, currentPage: prevState.currentPage + 1 };
        });
    };
    render() {
        const {
            users,
            redirect,
            totalCount,

            currentPage,
            usersPerPage
        } = this.state;

        console.log({ users, totalCount });
        const indexOfLastUser = currentPage * usersPerPage;
        const indexOfFirstUser = indexOfLastUser - usersPerPage + 1;

        if (redirect) {
            return <Redirect to={{ pathname: "/#/403" }} />;
        }
        return (
            <Fragment>
                {/* <div id="" /> */}

                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000} />
                    <aside className="isw-sideBar">
                        <Sidebar type="CUS" />
                    </aside>
                    <div />
                    <div className="isw-content--wrapper">
                        <div>
                            <div className="content" id="content-body">
                                <div className="container-fluid container-limited">
                                    <div className="row my-4">
                                        <div className="col-lg-6 isw-valign--middle mr-auto">
                                            <p className="isw-p mb-0">
                                                {totalCount > 0
                                                    ? `Staffs list (showing ${indexOfFirstUser}-${indexOfLastUser} of ${totalCount}
                                                records)`
                                                    : `Staffs list (showing ${totalCount}-${totalCount} of ${totalCount}
                                                records)`}
                                            </p>
                                        </div>

                                        <div className="col-lg-3 text-right">
                                            <button
                                                className="isw-btn border w-100"
                                                data-toggle="modal"
                                                data-target="#exampleModalCenter"
                                            >
                                                <IconFilter />
                                                <span>Filter Table</span>
                                            </button>
                                        </div>

                                        <div className="col-md text-right">
                                            <button className="isw-btn isw-btn--raised bg-primary text-white w-100">
                                                <IconAddContact />
                                                <span
                                                    onClick={
                                                        this.routeToCreateUser
                                                    }
                                                >
                                                    Add Staff
                                                </span>
                                            </button>
                                        </div>
                                    </div>

                                    <div className="row mb-4 mt-3">
                                        <div className="col-12">
                                            <div className="isw-table">
                                                <ul className="isw-table--head">
                                                    <li className="isw-table--headLi">
                                                        <div className="row">
                                                            <div className="col-lg-4">
                                                                <span className="isw-table--headItem">
                                                                    Name
                                                                </span>
                                                            </div>
                                                            <div className="col-lg-2">
                                                                <span className="isw-table--headItem">
                                                                    Phone Number
                                                                </span>
                                                            </div>
                                                            <div className="col-lg-3">
                                                                <span className="isw-table--headItem">
                                                                    Role
                                                                </span>
                                                            </div>
                                                            <div className="col-lg-3 text-right">
                                                                <span className="isw-table--headItem">
                                                                    Location
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div
                                            className="col-12"
                                            style={{
                                                display: "flex",
                                                flexDirection: "column"
                                            }}
                                        >
                                            {totalCount > 0 ? (
                                                <Pagination
                                                    data={users}
                                                    total={totalCount}
                                                    decrement={this.decrement}
                                                    increment={this.increment}
                                                    currentPage={currentPage}
                                                    handleClick={
                                                        this.handleClick
                                                    }
                                                    goToCustomerInfo={
                                                        this.goToCustomerInfo
                                                    }

                                                />
                                            ) : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    console.log({ state });

    const {
        get_users,
        get_users_data,
        payload,
        total
    } = state.cus_getUsers_reducer;
    console.log({ state, total });
    return {
        get_users,
        get_users_data,
        payload,
        total
    };
};

export default connect(mapStateToProps)(Staff);

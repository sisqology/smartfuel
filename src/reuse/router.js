import React, { Component } from "react";
import { Route } from "react-router-dom";
import { Router } from "react-router";
import { history } from "./history";
import App from "../App";
import { Authorization } from "./authorization";
import YourRoute from "./authorization";
import ISWRoutes from "../customer-isw/ISW/routes";
import { SUPER_ADMIN } from "../redux-flow-all/arsVariables";

const ISW = Authorization(SUPER_ADMIN);
const accesibleISWRoutes = ISW(ISWRoutes);
// // Router configuration
class ValidRoutes extends Component {
    render() {
        return (
            <Router history={history}>
                <Route path="/" component={App}>
                    <Route path="/isw/vendor" component={accesibleISWRoutes} />
                    {/* <Route path="feature" component={ISW(YourRoute)} /> */}
                    {/* </Route> */}
                </Route>
            </Router>
        );
    }
}

export default ValidRoutes;

import {
    GET_USERS_FAILURE,
    GET_USERS_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_getUsers_reducer = (state = {}, action) => {
    const { type, message, code, payloadReturned, totalCount } = action;
    switch (type) {
        case GET_USERS_SUCCESS:
            return {
                ...state,
                get_users: SUCCESS,
                get_users_data: message,
                payload: payloadReturned,
                total: totalCount
            };
        case GET_USERS_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                get_users: FAILURE,
                get_users_data: message ? message : "Bad Network Connectivity"
            };
        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_getUsers_reducer;

import * as OVHCONSTANTS from "../ovh-constants";

export function getFleetsReducer(state={}, action) {
    switch (action.type) {
        case OVHCONSTANTS.FETCH_FLEETS_PENDING:
            return {
                fetch_fleets_status: OVHCONSTANTS.FETCH_FLEETS_PENDING,
                fetch_fleets_data: action
            };
        case OVHCONSTANTS.FETCH_VENDOR_USERS_SUCCESS:
            return {
                fetch_fleets_status: OVHCONSTANTS.FETCH_FLEETS_SUCCESS,
                fetch_fleets_data: action
            };
        case OVHCONSTANTS.FETCH_VENDOR_USERS_FAILURE:
            return {
                fetch_fleets_status: OVHCONSTANTS.FETCH_FLEETS_FAILURE,
                fetch_fleets_data: action
            };

        default:
            return {
                fetch_fleets_status: OVHCONSTANTS.FETCH_VENDOR_USERS_PENDING,
                fetch_fleets_data: action
            };
    }
}

export function createFleetOwnerReducer(state={}, action) {
    switch (action.type) {
        case OVHCONSTANTS.CREATE_FLEET_OWNER_PENDING:
            return {
                create_fleet_status: OVHCONSTANTS.CREATE_FLEET_OWNER_PENDING,
                create_fleet_data: action
            };
        case OVHCONSTANTS.CREATE_FLEET_OWNER_SUCCESS:
            return {
                create_fleet_status: OVHCONSTANTS.CREATE_FLEET_OWNER_SUCCESS,
                create_fleet_data: action
            };
        case OVHCONSTANTS.CREATE_FLEET_OWNER_FAILURE:
            return {

                create_fleet_status: OVHCONSTANTS.CREATE_FLEET_OWNER_FAILURE,
                create_fleet_data: action
            };

        default:
            return {
                state,
            };
    }
}
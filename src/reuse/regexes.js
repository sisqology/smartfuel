// testing that input is + or digits
export const areDigits = input => {
    const re_digits = /^[+]?[0-9]*$/;
    return re_digits.test(input);
};

let re_phone;
export const validateNumber = number => {
    if (number.length === 14) {
        // testing +234 ....
        re_phone = /^[+](234)?\d{10}$/;
        return re_phone.test(number);
    } else {
        // testing normal phone number
        re_phone = /^[0]\d{10}$/;
        return re_phone.test(number);
    }
};

export const validateEmail = email => {
    const re_email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re_email.test(email);
};

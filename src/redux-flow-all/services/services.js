import {
    loginService,
    refreshTokenService,
    signupService,
    resetPasswordService,
    OTPService,
    verifyProfileService,
    forgotPasswordService,
    changePasswordService
} from "./cus_authServices";

import {
    cus_createUserService,
    cus_editBasicInfoService,
    cus_createLocationService,
    cus_getLocationsService,
    cus_getUsersService,
    cus_getCustomersService,
    cus_permissionsService,
    cus_rolesService
} from "./cus_appServices";

import { isw_createVendorService } from "./isw_appServices";

const Services = {
    loginService,
    refreshTokenService,
    signupService,
    resetPasswordService,
    OTPService,
    verifyProfileService,
    forgotPasswordService,
    changePasswordService,
    cus_createUserService,
    cus_editBasicInfoService,
    cus_createLocationService,
    cus_getLocationsService,
    cus_getUsersService,
    cus_getCustomersService,
    cus_permissionsService,
    cus_rolesService,
    isw_createVendorService
};

export default Services;

import * as React from "react";
import OvhHeader from "../templates/header";
import {ToastContainer} from "react-toastify";
import OvhMenu from "../templates/menu";
import {Fragment} from "react";
import connect from "react-redux/es/connect/connect";
import {getUsers} from "../redux-flow-ovh/actions/users.actions";
import * as OvhConstants from "../redux-flow-ovh/ovh-constants";

class VendorUserPermissions extends React.Component {
    constructor(props){
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.props.dispatch(getUsers());
        console.log(this.state);
    }

    renderUsers(){
        console.log(this.props.vendors.fetch_vendor_users_data);
        let props = this.props.vendors.fetch_vendor_users_data;
        let vendors = props.response;
        console.log(vendors);
        if(props.type === OvhConstants.FETCH_VENDOR_USERS_SUCCESS){
            console.error(vendors.response);
            return(
                <div className="list-group list-group-flush"
                     id="list-tab" role="tablist">
                    <a className="list-group-item list-group-item-action active"
                       id="list-home-list" data-toggle="list"
                       href="#list-home" role="tab"
                       aria-controls="home">Samuel Ejiro</a>
                    <a className="list-group-item list-group-item-action"
                       id="list-profile-list" data-toggle="list"
                       href="#list-profile" role="tab"
                       aria-controls="profile">Ciroma Biodun</a>
                    <a className="list-group-item list-group-item-action"
                       id="list-messages-list" data-toggle="list"
                       href="#list-messages" role="tab"
                       aria-controls="messages">Chioam Aregbede</a>
                    <a className="list-group-item list-group-item-action"
                       id="list-settings-list" data-toggle="list"
                       href="#list-settings" role="tab"
                       aria-controls="settings">Bayo Chukwuemeka</a>
                </div>
            );
        }

        // let status = object.users.fetch_vendor_users_data.type;
        // let resp = object.users.fetch_vendor_users_data;
    }

    render() {
        const props = this.props;
        let Renderusers = this.renderUsers();
        return (
            <Fragment>
                <div id="">
                    <OvhHeader />
                </div>
                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000}/>
                    <OvhMenu/>
                    <div></div>
                    <div className="isw-content--wrapper">
                        <div>
                            <form className="content position-relative" id="content-body">
                                <div className="container-fluid container-limited">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">Assign Permission</h3>
                                                    <div className="isw-card-p">Assign sets of permissions to user</div>
                                                </div>
                                                <div className="card-body isw-permission p-3">
                                                    <div className="row">
                                                        <div className="col-5">
                                                            <div className="isw-permission-name">
                                                                <div className="form-field">
                                                                    <div className="form-field__control">

                                                                        <input id="exampleField3" type="text" placeholder="Search Name"
                                                                               className="form-field__input"/>
                                                                    </div>
                                                                </div>

                                                                <Renderusers />
                                                            </div>
                                                        </div>
                                                        <div className="col-7">
                                                            <div className="">
                                                                <div className="form-select">
                                                                    <select className="select-text">
                                                                        <option value="1">All</option>
                                                                        <option value="2">Admin</option>
                                                                        <option value="3">User</option>
                                                                    </select>
                                                                    <span className="select-highlight"></span>
                                                                    <span className="select-bar"></span>
                                                                    <label className="select-label">Select Role</label>
                                                                </div>
                                                                <div className="tab-content isw-permission-list"
                                                                     id="nav-tabContent">
                                                                    <div className="tab-pane fade show active"
                                                                         id="list-home" role="tabpanel"
                                                                         aria-labelledby="list-home-list">

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Create
                                                                                            User</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Assign User
                                                                                            Roles</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Create/Renew
                                                                                            Subscription</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Create and
                                                                                            Edit Stores</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Set
                                                                                            Parameters For Email
                                                                                            Notifications</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Create
                                                                                            User</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Assign User
                                                                                            Roles</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Create/Renew
                                                                                            Subscription</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Create and
                                                                                            Edit Stores</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Create
                                                                                            User</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Assign User
                                                                                            Roles</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}>
                                                                                        <h6 className="mb-0">Create/Renew
                                                                                            Subscription</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}>
                                                                                    <input type="checkbox"/><i className="helper"></i>
                                                                                    <div style={{
                                                                                        marginTop: `${0.2}rem`
                                                                                    }}>
                                                                                        <h6 className="mb-0">Create and
                                                                                            Edit Stores</h6>
                                                                                        <small>Admin</small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="tab-pane fade" id="list-profile"
                                                                         role="tabpanel"
                                                                         aria-labelledby="list-profile-list">...
                                                                    </div>
                                                                    <div className="tab-pane fade" id="list-messages"
                                                                         role="tabpanel"
                                                                         aria-labelledby="list-messages-list">...
                                                                    </div>
                                                                    <div className="tab-pane fade" id="list-settings"
                                                                         role="tabpanel"
                                                                         aria-labelledby="list-settings-list">...
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12">
                                            <div className="isw-user-permission-action">
                                                <div className="isw-user-permission-btn">
                                                    <div className="container-fluid p-0">
                                                        <div className="row">
                                                            <div className="col-md">
                                                                <button
                                                                    className="isw-btn isw-btn--raised bg-primary text-white w-100">
                                                                    <span>Update User Permission</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

function mapStateToProps(state){
    console.log(state);
    return {
        vendors: state.ven_vendor_users,
    };
}

export default connect(mapStateToProps)(VendorUserPermissions);

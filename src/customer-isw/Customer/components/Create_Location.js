import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { ToastContainer, toast } from "react-toastify";

// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";
import Sidebar from "../../../reuse/sidebar";
import { Actions } from "../../../redux-flow-all/actions/index";
import { SUCCESS, FAILURE } from "../../../redux-flow-all/arsVariables";
import Leye from "../../../assets/images/isw-imageBox.png";
import Map from "../../../assets/images/isw-map.png";
import {
    areDigits,
    validateNumber,
    validateEmail
} from "../../../reuse/regexes";
import { history } from "../../../reuse/history";
// Assets
class CreateLocation extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            phone: "",
            branchId: "",
            latitude: "",
            longitude: "",
            branch: "",
            address: "",
            validEmail: null,
            validPhone: null,
            pending: false,
            errorShown: false,
            redirect: false,
            user: [],
            users: [],
            clicked: false,
            searchString: ""
        };
    }

    componentDidMount() {
        const { dispatch, allowed, role, fakeAuth, usersPayload } = this.props;
        console.log(this.props, "KAMSIIIIIIIII", { history, allowed, role });

        if (role === allowed && fakeAuth.isAuthenticated) {
            // this.setState({ pending: true, errorShown: false });
            this.setState({
                pending: true,
                errorShown: false
                // users: usersPayload
            });
            dispatch(Actions.cus_getUsers());
        } else if (role !== allowed && fakeAuth.isAuthenticated) {
            this.setState({ redirect: true });
            console.log("HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    static showToast = (message, type) => {
        if (toast.isActive(CreateLocation.toastId)) {
            return toast.update(CreateLocation.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (CreateLocation.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (CreateLocation.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const {
            create_location,
            create_location_data,
            get_users,
            get_users_data,
            usersPayload
        } = props;
        console.log({
            create_location_data,
            usersPayload,
            get_users,
            create_location,
            create_location_data
        });

        // if get users is successful
        if (get_users === SUCCESS && errorShown === false && pending === true) {
            console.log({ usersPayload });
            props.dispatch(Actions.reset());
            return {
                users: usersPayload,
                pending: false,
                errorShown: true
            };
        }

        // if get users fails
        if (get_users === FAILURE && pending === true && errorShown === false) {
            CreateLocation.showToast(get_users_data, "error");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }

        // if create is successful
        if (
            create_location === SUCCESS &&
            errorShown === false &&
            pending === true
        ) {
            CreateLocation.showToast(create_location_data, "success");
            props.dispatch(Actions.reset());
            return {
                email: "",
                phone: "",
                branchId: "",
                latitude: "",
                longitude: "",
                branch: "",
                address: "",
                validEmail: null,
                validPhone: null,

                redirect: false,

                clicked: false,
                searchString: "",
                pending: false,
                errorShown: true
            };
        }

        // if create fails
        if (
            create_location === FAILURE &&
            pending === true &&
            errorShown === false
        ) {
            CreateLocation.showToast(create_location_data, "error");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }
        return state;
    };

    onEmail_Change = ({ target }) => {
        const { value } = target;

        const isValidEmail = validateEmail(value);

        this.setState({
            email: value,
            validEmail: isValidEmail ? "true" : "false"
        });
    };

    onPhone_Change = ({ target }) => {
        const { value } = target;
        const isValidPhone = validateNumber(value);
        console.log(areDigits(value));
        if (areDigits(value)) {
            this.setState({
                phone: value,
                validPhone: isValidPhone ? "true" : "false"
            });
        }
    };

    onBranch_Name_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            branch: value
        });
    };

    onBranchId_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            branchId: value
        });
    };

    onLatitude_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            latitude: value
        });
    };
    onLongitude_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            longitude: value
        });
    };
    onAddress_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            address: value
        });
    };

    selectUser = () => {
        this.setState(prevState => {
            return { ...prevState, clicked: !prevState.clicked };
        });
    };

    userPicked = value => {
        console.log(value);
        this.setState({
            user: [value],
            clicked: false
        });
    };

    searching = ({ target }) => {
        const { value } = target;
        this.setState({ searchString: value });
    };

    onSave = e => {
        e.preventDefault();
        this.setState({ pending: true, errorShown: false });
        const {
            email,
            address,
            phone,
            branch,
            branchId,
            latitude,
            longitude,
            user,
            users
        } = this.state;

        const { dispatch } = this.props;

        console.log({ user, users }, user[0]);
        // dispatch(Actions.reset());
        dispatch(
            Actions.cus_createLocation({
                name: branch,
                emailaddress: email,
                phone,
                address,
                latitude,
                longitude,
                code: branchId,
                users: user.length > 0 ? [user[0].id] : []
            })
        );
    };
    render() {
        const {
            branch,
            latitude,
            longitude,
            phone,
            branchId,
            email,
            validEmail,
            validPhone,
            address,
            redirect,
            users,
            user,
            clicked,
            searchString
        } = this.state;

        const filteredUsers = users.filter(user =>
            user.firstName.toLowerCase().includes(searchString.toLowerCase())
        );

        console.log({ user, users });
        const userToRender = user => {
            const { firstName, lastName, id } = user;
            return (
                <Fragment>
                    <h3 className="isw-subtitle">
                        {firstName} {lastName}
                    </h3>
                    <p className="isw-p2 mb-0">{`ID: ${id}`}</p>
                    <div className="mt-2 small">
                        Role - <span>Branch Cordinator</span>
                    </div>
                </Fragment>
            );
        };
        if (redirect) {
            return <Redirect to={{ pathname: "/#/403" }} />;
        }
        return (
            <Fragment>
                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000} />
                    <aside className="isw-sideBar">
                        <Sidebar type="CUS" />
                    </aside>
                    <div />
                    <div className="isw-content--wrapper">
                        <div>
                            <form className="content" id="content-body">
                                <div className="container-fluid container-limited">
                                    <div className="row">
                                        <div className="col-lg-9">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">
                                                        New Branch
                                                    </h3>
                                                    <div className="isw-card-p">
                                                        Personal information of
                                                        the user managing this
                                                        account
                                                    </div>
                                                </div>
                                                <div
                                                    className="card-body p-3"
                                                    style={{
                                                        paddingTop: `${0} !important`
                                                    }}
                                                >
                                                    <div className="row">
                                                        <div className="col-lg-8">
                                                            <div className="row">
                                                                <div className="col-12">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField0"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Branch Name"
                                                                                onChange={
                                                                                    this
                                                                                        .onBranch_Name_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onBranch_Name_Change
                                                                                }
                                                                                value={
                                                                                    branch
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField1"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Branch ID"
                                                                                onChange={
                                                                                    this
                                                                                        .onBranchId_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onBranchId_Change
                                                                                }
                                                                                value={
                                                                                    branchId
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField2"
                                                                                type="text"
                                                                                placeholder="Phone Number"
                                                                                onChange={
                                                                                    this
                                                                                        .onPhone_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onPhone_Change
                                                                                }
                                                                                value={
                                                                                    phone
                                                                                }
                                                                                className={
                                                                                    validPhone ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validPhone ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="isw-divider my-4" />
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField3"
                                                                                type="text"
                                                                                className={
                                                                                    validEmail ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validEmail ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                                placeholder="Enter Email"
                                                                                onChange={
                                                                                    this
                                                                                        .onEmail_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onEmail_Change
                                                                                }
                                                                                value={
                                                                                    email
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField33"
                                                                                type="text"
                                                                                className={`form-field__input k_input`}
                                                                                placeholder="Address"
                                                                                onChange={
                                                                                    this
                                                                                        .onAddress_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onAddress_Change
                                                                                }
                                                                                value={
                                                                                    address
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField4"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Longitude"
                                                                                onChange={
                                                                                    this
                                                                                        .onLongitude_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onLongitude_Change
                                                                                }
                                                                                value={
                                                                                    longitude
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField5"
                                                                                type="text"
                                                                                value={
                                                                                    latitude
                                                                                }
                                                                                className={`form-field__input k_input`}
                                                                                placeholder="Latitude"
                                                                                onChange={
                                                                                    this
                                                                                        .onLatitude_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onLatitude_Change
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="isw-image">
                                                                        <img
                                                                            src={
                                                                                Map
                                                                            }
                                                                            alt="map of location"
                                                                        />
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="my-4">
                                                                        <div className="isw-card bg-iswLight2 p-2">
                                                                            <span className="font-weight-bold">
                                                                                Address
                                                                            </span>{" "}
                                                                            52
                                                                            Broad
                                                                            Street,
                                                                            Marina,
                                                                            Lagos
                                                                            Island,
                                                                            Nigeria.
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-md">
                                                            Picture
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md">
                                            <div className="d-block mb-2">
                                                <button
                                                    onClick={this.onSave}
                                                    className="isw-btn isw-btn--raised bg-primary text-white w-100"
                                                >
                                                    <span>Save</span>
                                                </button>
                                            </div>

                                            <div className="d-block">
                                                <button className="isw-btn isw-btn--raised text-primary w-100">
                                                    <span>Cancel</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row mt-4">
                                        <div className="col-lg-9">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3">
                                                    <h3 className="isw-card-h3">
                                                        Assign User
                                                    </h3>
                                                    <div className="isw-card-p">
                                                        Personal information of
                                                        the user managing this
                                                        account
                                                    </div>
                                                </div>
                                                {user.length === 0 ? null : (
                                                    <div className="card-body p-3 isw-hoverClick">
                                                        <div className="d-flex">
                                                            <div
                                                                className="isw-image"
                                                                style={{
                                                                    width: `${5}rem`,
                                                                    height: `${4}rem`,
                                                                    marginRight: `${1}rem`
                                                                }}
                                                            >
                                                                <img
                                                                    src={Leye}
                                                                    alt="assigned user"
                                                                />
                                                            </div>
                                                            <div className="d-inline-block">
                                                                {user.map(
                                                                    you => {
                                                                        const {
                                                                            firstName,
                                                                            lastName,
                                                                            id
                                                                        } = you;
                                                                        return (
                                                                            <Fragment
                                                                                key={id.toString()}
                                                                            >
                                                                                <h3 className="isw-subtitle">
                                                                                    {
                                                                                        firstName
                                                                                    }{" "}
                                                                                    {
                                                                                        lastName
                                                                                    }
                                                                                </h3>
                                                                                <p className="isw-p2 mb-0">{`ID: ${id}`}</p>
                                                                                <div className="mt-2 small">
                                                                                    Role
                                                                                    -{" "}
                                                                                    <span>
                                                                                        Branch
                                                                                        Cordinator
                                                                                    </span>
                                                                                </div>
                                                                            </Fragment>
                                                                        );
                                                                    }
                                                                )}
                                                            </div>
                                                        </div>
                                                    </div>
                                                )}

                                                <div className="card-footer bg-white">
                                                    <button
                                                        type="button"
                                                        onClick={
                                                            this.selectUser
                                                        }
                                                        className="isw-btn isw-btn--raised bg-primary text-white"
                                                        data-toggle="modal"
                                                        data-target="#exampleModalCenter"
                                                    >
                                                        <span>Select User</span>
                                                    </button>

                                                    <div
                                                        id="myDropdown"
                                                        className="dropdown-content"
                                                        style={{
                                                            display: clicked
                                                                ? "flex"
                                                                : "none",
                                                            flexDirection:
                                                                "column"
                                                        }}
                                                    >
                                                        {users.length > 0 ? (
                                                            <input
                                                                type="text"
                                                                placeholder="Search.."
                                                                id="myInput"
                                                                className="form-field__input"
                                                                onChange={
                                                                    this
                                                                        .searching
                                                                }
                                                            />
                                                        ) : (
                                                            "No created users yet"
                                                        )}
                                                        {filteredUsers.map(
                                                            user => {
                                                                const {
                                                                    firstName,
                                                                    lastName,
                                                                    id
                                                                } = user;
                                                                return (
                                                                    <span
                                                                        key={id.toString()}
                                                                        onClick={() =>
                                                                            this.userPicked(
                                                                                user
                                                                            )
                                                                        }
                                                                        value={`${firstName}' '${lastName}`}
                                                                    >
                                                                        {
                                                                            firstName
                                                                        }{" "}
                                                                        {
                                                                            lastName
                                                                        }
                                                                    </span>
                                                                );
                                                            }
                                                        )}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    const {
        create_location,
        create_location_data
    } = state.cus_createLocation_reducer;
    const {
        get_users,
        get_users_data,
        payload: usersPayload
    } = state.cus_getUsers_reducer;

    return {
        create_location,
        create_location_data,
        get_users,
        get_users_data,
        usersPayload
    };
};

export default connect(mapStateToProps)(CreateLocation);

import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import {
    PW_FAILURE,
    PW_SUCCESS,
    PW_RESET
} from "../../../../redux-flow-all/arsVariables";

function* resetPassword(payload) {
    try {
        const returnedData = yield call(Services.resetPasswordService, payload);
        const { error, status, data } = returnedData;
        const { code, description } = data;
        console.log({ data, returnedData });
        if (error === false && status === 200 && code === 1) {
            return yield put({ type: PW_SUCCESS, message: description });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({ type: PW_FAILURE, message: description });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({ type: PW_FAILURE, message: description });
        }
    } catch (error) {
        console.log({ error });
        const { response } = error;
        if (response) {
            const { data, status } = response;

            if (status === 400) {
                return yield put({
                    type: PW_FAILURE,
                    message: data.error_description
                });
            }
        } else if (response === undefined) {
            return yield put({
                type: PW_FAILURE,
                message: "Failed to connect"
            });
        }
    }
}

export default function* cus_resetPasswordSaga() {
    yield takeLatest(PW_RESET, resetPassword);
}

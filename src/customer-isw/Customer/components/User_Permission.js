import React, { Component, Fragment } from "react";

import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";
import Sidebar from "../../../reuse/sidebar";
import { Actions } from "../../../redux-flow-all/actions/index";
import { SUCCESS, FAILURE } from "../../../redux-flow-all/arsVariables";
import { history } from "../../../reuse/history";
import { customerTester } from "../../../reuse/authorization_test";
// Assets
class UserPermission extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            permissions: [],
            users: [],
            roles: [],
            pending: false,
            errorShown: false,
            redirect: false,
            searchString: ""
        };
    }

    componentDidMount() {
        const { dispatch, allowed, role, fakeAuth, usersPayload } = this.props;
        console.log(this.props, "KAMSIIIIIIIII", {
            history,
            allowed,
            role,
            usersPayload
        });
        // customerTester()
        if (role === allowed && fakeAuth.isAuthenticated) {
            this.setState({
                pending: true,
                errorShown: false
                // users: usersPayload
            });
            // dispatch(Actions.cus_roles());
            // dispatch(Actions.cus_permissions());
            dispatch(Actions.cus_getUsers());
        } else if (role !== allowed && fakeAuth.isAuthenticated) {
            this.setState({ redirect: true });
            console.log("HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            // history.push("/#/403");
        }
    }

    static showToast = (message, type) => {
        if (toast.isActive(UserPermission.toastId)) {
            return toast.update(UserPermission.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (UserPermission.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (UserPermission.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const {
            get_permissions,
            get_permissions_data,
            get_users,
            get_users_data,
            usersPayload,
            permissionsPayload,
            get_roles,
            get_roles_data,
            rolesPayload
        } = props;

        console.log("BEFORE MOUNTING");
        console.log({ state, usersPayload });

        if (state.users === []) {
            props.dispatch(Actions.cus_getUsers());
        }
        // // if get UserPermission is successful
        // if (
        //     get_permissions === SUCCESS &&
        //     errorShown === false &&
        //     pending === true
        // ) {
        //     props.dispatch(Actions.reset());
        //     return {
        //         permissions: permissionsPayload,
        //         pending: false,
        //         errorShown: true
        //     };
        // }

        // // if get UserPermission fails
        // if (
        //     get_permissions === FAILURE &&
        //     pending === true &&
        //     errorShown === false
        // ) {
        //     UserPermission.showToast(get_permissions_data, "error");
        //     props.dispatch(Actions.reset());
        //     return {
        //         pending: false,
        //         errorShown: true
        //     };
        // }

        // // if get roles is successful
        // if (get_roles === SUCCESS && errorShown === false && pending === true) {
        //     props.dispatch(Actions.reset());
        //     return {
        //         roles: rolesPayload,
        //         pending: false,
        //         errorShown: true
        //     };
        // }

        // // if get roles fails
        // if (get_roles === FAILURE && pending === true && errorShown === false) {
        //     UserPermission.showToast(get_roles_data, "error");
        //     props.dispatch(Actions.reset());
        //     return {
        //         pending: false,
        //         errorShown: true
        //     };
        // }

        // if get users is successful
        if (get_users === SUCCESS && errorShown === false && pending === true) {
            console.log({ usersPayload, state });
            props.dispatch(Actions.reset());
            return {
                users: usersPayload,
                pending: false,
                errorShown: true
            };
        }
        console.log({ usersPayload, state });
        // if get users fails
        if (get_users === FAILURE && pending === true && errorShown === false) {
            UserPermission.showToast(get_users_data, "error");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }
        return state;
    };

    handleUpdate = e => {
        e.preventDefault();
        // this.setState({ pending: true, errorShown: false });
        // const {  } = this.state;
        // const { dispatch } = this.props;
    };
    searching = ({ target }) => {
        const { value } = target;
        this.setState({ searchString: value });
    };
    render() {
        const { users, permissions, redirect, searchString } = this.state;

        // const { from } = location.state || {
        //     from: { pathname: "/customer/signup" }
        // };

        console.log(users);
        const filteredUsers = users.filter(user =>
            user.firstName.toLowerCase().includes(searchString.toLowerCase())
        );

        if (redirect) {
            return <Redirect to={{ pathname: "/#/403" }} />;
        }
        return (
            <Fragment>
                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000} />
                    <aside className="isw-sideBar">
                        <Sidebar type="CUS" />
                    </aside>

                    <div className="isw-content--wrapper">
                        <div>
                            <form
                                className="content position-relative"
                                id="content-body"
                            >
                                <div className="container-fluid container-limited">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">
                                                        Assign Permission
                                                    </h3>
                                                    <div className="isw-card-p">
                                                        Assign sets of
                                                        permissions to user
                                                    </div>
                                                </div>
                                                <div className="card-body isw-permission p-3">
                                                    <div className="row">
                                                        <div className="col-5">
                                                            <div className="isw-permission-name">
                                                                <div className="form-field">
                                                                    <div className="form-field__control">
                                                                        <input
                                                                            type="text"
                                                                            className="form-field__input"
                                                                            placeholder="SearchName"
                                                                            id="myInput"
                                                                            onChange={
                                                                                this
                                                                                    .searching
                                                                            }
                                                                        />
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    className="list-group list-group-flush"
                                                                    id="list-tab"
                                                                    role="tablist"
                                                                >
                                                                    {filteredUsers.map(
                                                                        user => {
                                                                            const {
                                                                                firstName,
                                                                                lastName,
                                                                                id
                                                                            } = user;
                                                                            return (
                                                                                <span
                                                                                    key={id.toString()}
                                                                                    className="list-group-item list-group-item-action active"
                                                                                    id="list-home-list"
                                                                                    style={{
                                                                                        margin: `${5}px`
                                                                                    }}
                                                                                >
                                                                                    {
                                                                                        firstName
                                                                                    }{" "}
                                                                                    {
                                                                                        lastName
                                                                                    }
                                                                                </span>
                                                                            );
                                                                        }
                                                                    )}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-7">
                                                            <div className="">
                                                                <div className="form-select">
                                                                    <select className="select-text">
                                                                        <option value="1">
                                                                            All
                                                                        </option>
                                                                        <option value="2">
                                                                            Admin
                                                                        </option>
                                                                        <option value="3">
                                                                            User
                                                                        </option>
                                                                    </select>
                                                                    <span className="select-highlight" />
                                                                    <span className="select-bar" />
                                                                    <label className="select-label">
                                                                        Select
                                                                        Role
                                                                    </label>
                                                                </div>
                                                                <div
                                                                    className="tab-content isw-permission-list"
                                                                    id="nav-tabContent"
                                                                >
                                                                    <div
                                                                        className="tab-pane fade show active"
                                                                        id="list-home"
                                                                        role="tabpanel"
                                                                        aria-labelledby="list-home-list"
                                                                    >
                                                                        <div className="isw-permission-item-right">
                                                                            <div className="checkbox w-100 p-2">
                                                                                <label
                                                                                    style={{
                                                                                        display:
                                                                                            "flex",
                                                                                        marginBottom: 0
                                                                                    }}
                                                                                >
                                                                                    <input type="checkbox" />
                                                                                    <i className="helper" />
                                                                                    <div
                                                                                        style={{
                                                                                            marginTop: `${0.2}rem`
                                                                                        }}
                                                                                    >
                                                                                        <h6 className="mb-0">
                                                                                            Create
                                                                                            User
                                                                                        </h6>
                                                                                        <small>
                                                                                            Admin
                                                                                        </small>
                                                                                    </div>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        className="tab-pane fade"
                                                                        id="list-profile"
                                                                        role="tabpanel"
                                                                        aria-labelledby="list-profile-list"
                                                                    >
                                                                        ...
                                                                    </div>
                                                                    <div
                                                                        className="tab-pane fade"
                                                                        id="list-messages"
                                                                        role="tabpanel"
                                                                        aria-labelledby="list-messages-list"
                                                                    >
                                                                        ...
                                                                    </div>
                                                                    <div
                                                                        className="tab-pane fade"
                                                                        id="list-settings"
                                                                        role="tabpanel"
                                                                        aria-labelledby="list-settings-list"
                                                                    >
                                                                        ...
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12">
                                            <div className="isw-user-permission-action">
                                                <div className="isw-user-permission-btn">
                                                    <div className="container-fluid p-0">
                                                        <div className="row">
                                                            <div className="col-md">
                                                                <button className="isw-btn isw-btn--raised bg-primary text-white w-100">
                                                                    <span>
                                                                        Update
                                                                        User
                                                                        Permission
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    const {
        get_users,
        get_users_data,
        payload: usersPayload
    } = state.cus_getUsers_reducer;

    console.log({ state, usersPayload });
    const {
        get_permissions,
        get_permissions_data,
        payload: permissionsPayload
    } = state.cus_permissions_reducer;

    const {
        get_roles,
        get_roles_data,
        payload: rolesPayload
    } = state.cus_roles_reducer;
    return {
        get_users,
        get_users_data,
        usersPayload,
        permissionsPayload,
        get_permissions,
        get_permissions_data,
        get_roles,
        get_roles_data,
        rolesPayload
    };
};

export default connect(mapStateToProps)(UserPermission);

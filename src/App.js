import React, {Component, Fragment} from "react";
import { connect } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
import { Actions } from "./redux-flow-all/actions/index";
import { Router } from "react-router";

// components
import Layout from "./reuse/layout";
import Login from "./Auth/Login/Login";
import Page_404 from "./reuse/404";
import Page_403 from "./reuse/403";
import Page_500 from "./reuse/500";
import VendorRoute from "./ovh-fleet/ovh/routes";
import CustomerRoutes from "./customer-isw/Customer/routes";
import LoadingPage from "./reuse/loadingpage";
import ForgotPW from "./customer-isw/pages/Forgot/ForgotPW";
import ResetPW from "./customer-isw/pages/Reset/ResetPW";
import ISWRoutes from "./customer-isw/ISW/routes";
import CreateVendor1 from "./customer-isw/ISW/components/Create_Vendor1";
import CreateVendor2 from "./customer-isw/ISW/components/Create_Vendor2";
import ChangePW from "./customer-isw/pages/ChangePW/ChangePW";
import Signup from "./customer-isw/pages/Signup/Signup";
import Staff from "./customer-isw/Customer/components/Staff";
import SetupProfile from "./customer-isw/pages/Signup/SetupProfile";
import ProfileInfo from "./customer-isw/Customer/components/Profile_Info";
import CreateUser from "./customer-isw/Customer/components/Create_User";
import CreateLocation from "./customer-isw/Customer/components/Create_Location";
import EnterOTP from "./reuse/enterOTP";
import UserPermission from "./customer-isw/Customer/components/User_Permission";
import CustomerInfo from "./customer-isw/Customer/components/CustomerInfo";

// variables
import {
    SUCCESS,
    FAILURE,
    SUPER_ADMIN,
    VENDOR_ADMIN,
    CUSTOMER_ADMIN
} from "./redux-flow-all/arsVariables";

import { history } from "./reuse/history";

import Dashboard from "./ovh-fleet/ovh/components/Dashboard";
import Logout from "./Auth/Logout";

/** */
// import { Authorization } from "./reuse/authorization";
/// tests
import Can from "./components/Can";
import Users from "./ovh-fleet/ovh/components/Users";
import FleetOwners from "./ovh-fleet/ovh/components/FleetOwners";
import CreateFleetOwner from "./ovh-fleet/ovh/components/CreateFleetOwner";
import CreateVendorUser from "./ovh-fleet/ovh/components/CreateUser";
import FleetOwnerDetail from "./ovh-fleet/ovh/components/FleetOwnerDetail";
import Profile from "./ovh-fleet/ovh/components/ProfileInfo";
import VendorUserPermissions from "./ovh-fleet/ovh/components/UserPermissions";
// const ISW = Authorization(SUPER_ADMIN);
// const accesibleISWRoutes = ISW(ISWRoutes);
const url = "/customer/";
// variable to hold auth status and also functions to convert it
export const fakeAuth = {
    isAuthenticated: false,
    authenticate() {
        return (this.isAuthenticated = true);
    },
    signout() {
        return (this.isAuthenticated = false);
    }
};

// Private router function
const PrivateRoute = ({
    component: Component,
    fakeAuth,
    fullScreen,
    allowed,
    role,
    ...rest
}) => {
    // const {role} = this.props
    console.log(fakeAuth);
    return (
        <Route
            {...rest}
            render={props =>
                fakeAuth.isAuthenticated === true ? (
                    <Layout
                        {...props}
                        fullScreen={fullScreen}
                        allowed={allowed}
                        role={role}
                    >
                        <Component
                            allowed={allowed}
                            role={role}
                            fakeAuth={fakeAuth}
                        />
                    </Layout>
                ) : (
                    <Redirect
                        to={{
                            pathname: "/signin",
                            state: { from: props.location }
                        }}
                    />
                )
            }
        />
    );
};

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(Actions.onInit());

        if (
            fakeAuth.isAuthenticated &&
            history.location.pathname.includes("403")
        ) {
            history.push({ path: history.location.pathname });
        }
    }

    static getDerivedStateFromProps = (props, state) => {
        const { storage, data, role, allowed } = props;
        const { loaded } = state;
        console.log({ data, storage, role, allowed }, "kamsi", history);
        // console.log(defineAbilityFor(role));
        if (storage === SUCCESS && loaded === false) {
            fakeAuth.authenticate();
            // console.log("My role is :" + role);
            if (role === SUPER_ADMIN && fakeAuth.isAuthenticated) {
                history.push({
                    pathname: "/isw/create-vendor",
                    state: { role }
                });
            } else if (role === CUSTOMER_ADMIN && fakeAuth.isAuthenticated) {
                history.push({
                    pathname: "/customer/user-list",
                    state: { role }
                });
            } else if (role === VENDOR_ADMIN && fakeAuth.isAuthenticated) {
                history.push({
                    pathname: "/vendor/dashboard",
                    state: { role }
                });
            }
            return {
                loaded: true
            };
        }
        if (storage === FAILURE && loaded === false) {
            fakeAuth.signout();
            props.dispatch(Actions.reset());
            return {
                loaded: true
            };
        }
        return state;
    };

    render() {
        const { loaded } = this.state;
        const { role } = this.props;
        if (loaded) {
            return (
                <Router history={history}>
                    <Switch>
                        <Route
                            exact
                            path="/signin"
                            render={props => (
                                <Login fakeAuth={fakeAuth} {...props} />
                            )}
                        />
                        <Route
                            path="/logout"
                            component={Logout}
                        />

                        <Route
                            exact
                            path="/forgotPassword"
                            render={props => (
                                <ForgotPW {...props} fakeAuth={fakeAuth} />
                            )}
                        />
                        <Route
                            exact
                            path="/resetPassword"
                            render={props => (
                                <ResetPW {...props} fakeAuth={fakeAuth} />
                            )}
                        />
                        <Route
                            // if it falls on the localhost:3000/ or www.smartfuel.netlify.com/
                            exact
                            path="/"
                            render={props =>
                                fakeAuth.isAuthenticated &&
                                role === CUSTOMER_ADMIN ? (
                                    <Redirect
                                        push
                                        to={{
                                            pathname: "/customer/user-list",
                                            state: {
                                                from: props.location,
                                                role
                                            }
                                        }}
                                    />
                                ) : fakeAuth.isAuthenticated &&
                                  role === VENDOR_ADMIN ? (
                                    <Redirect
                                        push
                                        to={{
                                            pathname: "/vendor/dashboard",
                                            state: {
                                                from: props.location,
                                                role
                                            }
                                        }}
                                    />
                                ) : fakeAuth.isAuthenticated &&
                                  role === SUPER_ADMIN ? (
                                    <Redirect
                                        push
                                        to={{
                                            pathname: "/isw/create-vendor",
                                            state: {
                                                from: props.location,
                                                role
                                            }
                                        }}
                                    />
                                ) : !fakeAuth.isAuthenticated &&
                                  history.location.pathname === "/" &&
                                  (history.location.state
                                      ? !history.location.state.from.hash.includes(
                                            "#"
                                        )
                                      : null) ? (
                                    <Redirect
                                        push
                                        to={{
                                            pathname: `/signin`,
                                            state: { from: props.location }
                                        }}
                                    />
                                ) : (
                                    <Redirect
                                        to={{
                                            pathname: `/signin`,
                                            state: { from: props.location }
                                        }}
                                    />
                                )
                            }
                        />


                        {/* <Route path="/customer"> */}
                        {/* <Can I="view" this="/customer"> */}
                        <Route
                            exact
                            path={`${url}signup`}
                            render={props => (
                                <Signup {...props} fakeAuth={fakeAuth} />
                            )}
                        />
                        <Route
                            exact
                            path={`${url}setup-profile`}
                            render={props => (
                                <SetupProfile fakeAuth={fakeAuth} {...props} />
                            )}
                        />
                        <Route
                            exact
                            path={`${url}verify-profile`}
                            render={props => (
                                <EnterOTP fakeAuth={fakeAuth} {...props} />
                            )}
                        />
                        <Route
                            exact
                            path={`/settings/changePassword`}
                            fakeAuth={fakeAuth}
                            component={ChangePW}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={`${url}user-list`}
                            component={Staff}
                            allowed={CUSTOMER_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={`${url}user-permission`}
                            component={UserPermission}
                            allowed={CUSTOMER_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={`${url}staff-info`}
                            component={CustomerInfo}
                            allowed={CUSTOMER_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={`${url}profile-info`}
                            component={ProfileInfo}
                            allowed={CUSTOMER_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            fakeAuth={fakeAuth}
                            exact
                            path={`${url}create-location`}
                            component={CreateLocation}
                            allowed={CUSTOMER_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            path={`${url}create-user`}
                            component={CreateUser}
                            fakeAuth={fakeAuth}
                            allowed={CUSTOMER_ADMIN}
                            role={role}
                        />
                        {/* </Can> */}
                        {/* <PrivateRoute
                            exact
                            path={`/settings/changePassword`}
                            fakeAuth={fakeAuth}
                            component={ChangePW}
                        /> */}
                        {/* <Route path="/isw"> */}
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path="/isw/create-vendor"
                            component={CreateVendor1}
                            allowed={SUPER_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path="/isw/create-vendor-admin"
                            component={CreateVendor2}
                            allowed={SUPER_ADMIN}
                            role={role}
                        />



                        {/* TODO: Come back and clear this */}
                        <PrivateRoute
                            exact
                            path={"/vendor/dashboard"}
                            fakeAuth={fakeAuth}
                            component={Dashboard}
                            allowed={VENDOR_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={"/vendor/users"}
                            component={Users}
                            allowed={VENDOR_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={"/vendor/create-vendoruser"}
                            component={CreateVendorUser}
                            allowed={VENDOR_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={"/vendor/fleet-owners"}
                            component={FleetOwners}
                            allowed={VENDOR_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={"/vendor/create-fleet-owner"}
                            component={CreateFleetOwner}
                            allowed={VENDOR_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={"/vendor/fleet-owner-detail"}
                            component={FleetOwnerDetail}
                            allowed={VENDOR_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={"/vendor/profile-info"}
                            component={Profile}
                            allowed={VENDOR_ADMIN}
                            role={role}
                        />
                        <PrivateRoute
                            exact
                            fakeAuth={fakeAuth}
                            path={"/vendor/vendor-user-permissions"}
                            component={VendorUserPermissions}
                            allowed={VENDOR_ADMIN}
                            role={role}
                        />
                        {/* <CustomerRoutes fakeAuth={fakeAuth} /> */}
                        {/* <VendorRoute /> */}
                        {/* <ISWRoutes fakeAuth={fakeAuth} /> */}
                        {/* </Route> */}
                        <Route eaxct path="/#/500" component={Page_500} />
                        <Route exact path="/#/403" component={Page_403} />
                        <Route exact path="/#/404" component={Page_404} />
                        {/*<Route*/}
                            {/*path="*"*/}
                            {/*render={props => (*/}
                                {/*<Redirect*/}
                                    {/*to={{*/}
                                        {/*pathname: "/#/404"*/}
                                    {/*}}*/}
                                    {/*{...props}*/}
                                {/*/>*/}
                            {/*)}*/}
                        {/*/>*/}
                        }
                    </Switch>
                </Router>
            );
        }
        return <LoadingPage text="Please check your internet connection..." />;
    }
}

const mapStateToProps = state => {
    console.log({ state });
    const { storage, data, role: roleFromStorage } = state.storage_reducer;
    const { role: roleFromLogin } = state.login_reducer;

    return {
        storage,
        data,
        role: roleFromLogin || roleFromStorage
    };
};

export default connect(mapStateToProps)(App);

import { GET_LOCATION } from "../../../../redux-flow-all/arsVariables";


const cus_getLocation = payload => ({
    type: GET_LOCATION,
    payload
});

export default cus_getLocation;

import {
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../arsVariables";

const login_reducer = (state = {}, action) => {
    const { type, role, message, access } = action;
    switch (type) {
        case LOGIN_SUCCESS:
            return { login: SUCCESS, data: "Login Successful", role: role };
        case LOGIN_FAILURE:
            // message here is the error message from the server
            return {
                login: FAILURE,
                data: message ? message : "Bad Network Connectivity",
                access: access ? access : null
            };
        case RESET:
            return {};
        default:
            return state;
    }
};

export default login_reducer;

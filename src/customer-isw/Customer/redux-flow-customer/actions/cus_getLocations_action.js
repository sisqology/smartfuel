import { GET_LOCATIONS } from "../../../../redux-flow-all/arsVariables";

const cus_getLocations = payload => ({
    type: GET_LOCATIONS,
    payload
});

export default cus_getLocations;

import * as React from "react";
import {connect} from "react-redux";
import {Actions} from "../redux-flow-all/actions";
import {history} from "../reuse/history";
import {toast, ToastOptions as CreateLocation} from "react-toastify";

class Logout extends React.Component {
    toastId = null;

    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false,
        };
        localStorage.clear();
        history.push(`/signin`)
        // this.props.dispatch(Actions.logoutUser());
    }

    componentDidMount() {
        // this.props.dispatch(Actions.logoutUser());
        // localStorage.clear();
        // history.push(`/signin`)
    }

    static showToast = message => {
        if (toast.isActive(CreateLocation.toastId)) {
            return null;
        }
        return (CreateLocation.toastId = toast.error(message, {
            position: toast.POSITION.TOP_RIGHT,
            className: "error"
        }));
    };

    render() {
        const { logout } = this.props;
        return (
            <h4>
                Logging out...

            </h4>
        );
    }
}


export default connect(null)(Logout);

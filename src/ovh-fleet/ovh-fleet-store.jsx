import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import { createLogger } from 'redux-logger';
import rootReducer from "./rootReducer";
import thunkMiddleware from 'redux-thunk';
import thunk from 'redux-thunk';

const loggerMiddleware = createLogger();

export const store = createStore(
    rootReducer,
    applyMiddleware(
        thunk,
        loggerMiddleware
    )
);
import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import {
    OTP_SUCCESS,
    OTP_FAILURE,
    RESEND_OTP
} from "../../../../redux-flow-all/arsVariables";

function* resendOTP({ payload }) {
    console.log("resend otp sgag", { payload });
    try {
        const returnedData = yield call(
            Services.OTPService,
            payload,
            RESEND_OTP
        );
        const { error, status, data, type } = returnedData;
        const {  code, description } = data;
        console.log({ data });
        if (type === RESEND_OTP) {
            if (error === false && status === 200 && code === 1) {
                return yield put({ type: OTP_SUCCESS, message: description });
            } else if (error === false && status === 200 && code === -2) {
                return yield put({
                    type: OTP_FAILURE,
                    message: description,
                    code
                });
            }
        }
    } catch (error) {
        if (error.type === RESEND_OTP) {
            const { response } = error;
            if (response) {
                const { data, status } = response;

                if (status === 400) {
                    return yield put({
                        type: OTP_FAILURE,
                        message: data.error_description
                    });
                }
            } else if (response === undefined) {
                return yield put({
                    type: OTP_FAILURE,
                    message: "Failed to connect"
                });
            }
        }
    }
}

export default function* cus_resendOTPSaga() {
    yield takeLatest(RESEND_OTP, resendOTP);
}

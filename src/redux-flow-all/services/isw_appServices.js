const axios = require("axios");
const qs = require("qs");
const baseurl = "https://feulapp.azurewebsites.net/api/";

const header = token => {
    if (token) {
        return {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };
    } else {
        return {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };
    }
};

// create vendor
export const isw_createVendorService = ({ payload }, token) => {
    console.log({ payload, token }, "for CREATING USER");
    return new Promise((resolve, reject) => {
        axios
            .post(
                `${baseurl}userprofiling/createvendor`,
                payload,
                header(token)
            )
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

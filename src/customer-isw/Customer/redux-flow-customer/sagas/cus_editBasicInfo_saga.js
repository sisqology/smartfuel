import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import {
    EDIT_BASIC_FAILURE,
    EDIT_BASIC_SUCCESS,
    EDIT_BASIC_INFO,
    ENCRYPT_USER
} from "../../../../redux-flow-all/arsVariables";
import { decryptAndRead } from "../../../../redux-flow-all/services/localStorageHelper";

function* editBasicInfo(payload) {
    const decryptedToken = decryptAndRead(ENCRYPT_USER).access_token;
    console.log({ decryptedToken });
    try {
        const returnedData = yield call(
            Services.cus_editBasicInfoService,
            payload,
            decryptedToken
        );
        const { error, data, status } = returnedData;
        const { code, description } = data;
        console.log({ data, returnedData });
        if (error === false && status === 200 && code === 1) {
            return yield put({
                type: EDIT_BASIC_SUCCESS,
                message: description
            });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({
                type: EDIT_BASIC_FAILURE,
                message: description
            });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({
                type: EDIT_BASIC_FAILURE,
                message: description
            });
        }
    } catch (error) {
        console.log({ error });
        const { response } = error;
        if (response) {
            const { data, status } = response;
            if (status === 400) {
                return yield put({
                    type: EDIT_BASIC_FAILURE,
                    message: data.error_description
                });
            }
        } else if (response === undefined) {
            return yield put({
                type: EDIT_BASIC_FAILURE,
                message: "Failed to connect"
            });
        }
    }
}

export default function* cus_editBasicInfoSaga() {
    yield takeLatest(EDIT_BASIC_INFO, editBasicInfo);
}

import { GET_PERMISSIONS } from "../../../../../../redux-flow-all/arsVariables";

const cus_permissions = payload => ({
    type: GET_PERMISSIONS,
    payload
});

export default cus_permissions;

import { RESEND_OTP } from "../../../../redux-flow-all/arsVariables";

const cus_resendOTP = payload => ({
    type: RESEND_OTP,
    payload
});

export default cus_resendOTP;

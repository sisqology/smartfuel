import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import {
    SIGNUP_FAILURE,
    SIGNUP_SUCCESS,
    SIGNUP_USER
} from "../../../../redux-flow-all/arsVariables";
import { Actions } from "../../../../redux-flow-all/actions/index";

const { localStorage } = window;

function* signupUser({ payload }) {
    const { emailAddress, phoneNo } = payload.customerAdmin;
    const descriptionToSend = `request to send OTP to ${emailAddress}`;
    try {
        const returnedData = yield call(Services.signupService, payload);
        const { error, status, data } = returnedData;
        const { code, description } = data;
        console.log({ returnedData, data });
        if (error === false && status === 200 && code === 1) {
            localStorage.setItem("email", emailAddress);
            localStorage.setItem("phone", phoneNo);
            yield put(
                Actions.cus_sendOTP({
                    email: emailAddress,
                    description: descriptionToSend
                })
            );
            return yield put({ type: SIGNUP_SUCCESS, message: description });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({ type: SIGNUP_FAILURE, message: description });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({ type: SIGNUP_FAILURE, message: description });
        }
    } catch (error) {
        console.log({ error });

        const { response } = error;
        if (response) {
            const { data, status } = response;
            if (status === 400) {
                return yield put({
                    type: SIGNUP_FAILURE,
                    message: data.error_description
                });
            }
        } else if (response === undefined) {
            return yield put({
                type: SIGNUP_FAILURE,
                message: "Failed to connect"
            });
        }
    }
}

export default function* cus_signupUserSaga() {
    yield takeLatest(SIGNUP_USER, signupUser);
}

import {routeApis} from "../../../services-constants";
import {ApiService} from "../../../api-service";
import * as OVHCONSTANTS from "../ovh-constants" ;
const axios = require('axios');


const header = token => {
    if (token) {
        return {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };
    } else {
        return {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };
    }
};

export const getUsers = (index, size) => {
    return (dispatch) => {
        let consume = ApiService.request(routeApis.VENDOR_USERS, "GET", {'PageIndex':index, 'PageSize':size});
        dispatch(request(consume));
        return consume
            .then(response => {
                // consume.log(response);
                dispatch(success(response));
            })
            .catch(error => {
                dispatch(failure(error.description));
            });
    };

    function request(request) { return { type:OVHCONSTANTS.FETCH_VENDOR_USERS_PENDING, request} }
    function success(response) { return {type:OVHCONSTANTS.FETCH_VENDOR_USERS_SUCCESS, response} }
    function failure(error) { return {type:OVHCONSTANTS.FETCH_VENDOR_USERS_FAILURE, error} }
};

export function postUser(payload){
    return (dispatch) => {
        // let payload = payload;
        // console.log(payload);
        let consume = ApiService.request(routeApis.CREATE_VENDOR_USER, "POST", payload);
        dispatch(request(consume));
        return consume
            .then(response => {
                if(response.data.code === -1){
                    // console.log(response.data.description);
                    dispatch(failure(response.data.description));
                }
                else{
                    dispatch(success(response));
                }
            })
            .catch(error => {
                dispatch(failure(error.description));
            });
    };

    function request(request) { return { type:OVHCONSTANTS.CREATE_VENDOR_USER_PENDING, request} }
    function success(response) { return {type:OVHCONSTANTS.CREATE_VENDOR_USER_SUCCESS, response} }
    function failure(error) { return {type:OVHCONSTANTS.CREATE_VENDOR_USER_FAILURE, error} }
}

// export const getUsers = () => {
//     // const { email, password } = payload;
//     // console.log({ email, password });
//     // const objectToSend = qs.stringify({
//     //     username: email,
//     //     password,
//     //     grant_type: "password"
//     // });
//     // console.log({ objectToSend });
//     return new Promise((resolve, reject) => {
//         axios
//             .get(
//                 routeApis.VENDOR_USERS,
//                 header()
//             )
//             .then(res => {
//                 return resolve({ ...res, error: false });
//             })
//             .catch(({ response }) => {
//                 if (response) {
//                     console.log({ response }, "if");
//                     return reject({ response: response, error: true });
//                 } else {
//                     console.log({ response });
//                     return reject({ error: true, response });
//                 }
//             });
//     });
// };

import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import {
    GET_USERS_FAILURE,
    GET_USERS_SUCCESS,
    GET_USERS,
    ENCRYPT_USER
} from "../../../../redux-flow-all/arsVariables";
import { decryptAndRead } from "../../../../redux-flow-all/services/localStorageHelper";

function* getUsers(payload) {
    const decryptedData = decryptAndRead(ENCRYPT_USER);
    // console.log({ decryptedData });
    const decryptedToken = decryptedData.access_token;
    try {
        const returnedData = yield call(
            Services.cus_getUsersService,
            payload,
            decryptedToken
        );
        const { error, data, status } = returnedData;
        const {
            code,
            description,
            payload: payloadReturned,
            totalCount
        } = data;
        console.log({ data, returnedData });
        if (error === false && status === 200 && code === 1) {
            return yield put({
                type: GET_USERS_SUCCESS,
                message: description,
                payloadReturned,
                totalCount
            });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({
                type: GET_USERS_FAILURE,
                message: description
            });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({
                type: GET_USERS_FAILURE,
                message: description
            });
        }
    } catch (error) {
        console.log({ error });
        const { response } = error;
        if (response) {
            const { data, status } = response;
            if (status === 400) {
                return yield put({
                    type: GET_USERS_FAILURE,
                    message: data.error_description
                });
            }
        } else if (response === undefined) {
            return yield put({
                type: GET_USERS_FAILURE,
                message: "Failed to connect"
            });
        }
    }
}

export default function* cus_getUsersSaga() {
    yield takeLatest(GET_USERS, getUsers);
}

import {
    GET_USER_PERMISSIONS_FAILURE,
    GET_USER_PERMISSIONS_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../../../redux-flow-all/arsVariables";

const cus_userPermissions_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case GET_USER_PERMISSIONS_SUCCESS:
            return {
                ...state,
                get_up: SUCCESS,
                get_up_data: message
            };
        case GET_USER_PERMISSIONS_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                get_up: FAILURE,
                get_up_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_userPermissions_reducer;

import React, { Component } from "react";
import { NavLink } from "react-router-dom";

// Styles
import "../scss/trumps.scss";
import "../scss/reset.scss";
import "../scss/loadingpage.scss";

// assets
// import Aliens from "../../assets/taken.svg";

export class ErrorHandler extends Component {
    state = {
        error: false,
        info: ""
    };

    componentDidCatch(error, info) {
        this.setState({ error, info });
    }

    render() {
        const { error, info } = this.state;
        if (error) {
            console.log({ error, info });
            return (
                <div className="container-404">
                    <section>
                        {/* <img src={Aliens} alt="illustration" /> */}
                        <h1>Whoops!!!, an error occurred</h1>
                        <span>
                            <NavLink to="/dashboard">
                                Click here to go back to dashboard
                            </NavLink>
                        </span>
                    </section>
                </div>
            );
        }
        return this.props.children;
    }
}

export default ErrorHandler;

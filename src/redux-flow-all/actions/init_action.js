import { READ_STORAGE } from "../arsVariables";

const readStorage = () => ({
    type: READ_STORAGE
});

export default readStorage;

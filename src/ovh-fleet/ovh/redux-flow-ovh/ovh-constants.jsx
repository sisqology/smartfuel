// export const

export const FETCH_VENDOR_USERS_PENDING = 'FETCH_VENDOR_USERS_PENDING';
export const FETCH_VENDOR_USERS_SUCCESS = 'FETCH_VENDOR_USERS_SUCCESS';
export const FETCH_VENDOR_USERS_FAILURE = 'FETCH_VENDOR_USERS_FAILURE';

export const CREATE_VENDOR_USER_PENDING = 'CREATE_VENDOR_USER_PENDING';
export const CREATE_VENDOR_USER_SUCCESS = 'CREATE_VENDOR_USER_SUCCESS';
export const CREATE_VENDOR_USER_FAILURE = 'CREATE_VENDOR_USER_FAILURE';

export const CREATE_FLEET_OWNER_PENDING = 'CREATE_FLEET_OWNER_PENDING';
export const CREATE_FLEET_OWNER_SUCCESS = 'CREATE_FLEET_OWNER_SUCCESS';
export const CREATE_FLEET_OWNER_FAILURE = 'CREATE_FLEET_OWNER_FAILURE';

export const FETCH_FLEETS_PENDING = 'FETCH_FLEETS_PENDING';
export const FETCH_FLEETS_SUCCESS = 'FETCH_FLEETS_SUCCESS';
export const FETCH_FLEETS_FAILURE = 'FETCH_FLEETS_FAILURE';

export const UPDATE_PROFILE_PENDING = 'UPDATE_PROFILE_PENDING';
export const UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS';
export const UPDATE_PROFILE_FAILURE = 'UPDATE_PROFILE_FAILURE';
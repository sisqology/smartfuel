import * as React from 'react';
import {Router} from "react-router";
import {history} from "./../../../reuse/history";
import {Fragment} from "react";

class OvhHeader extends React.Component {

    constructor(props){
        super(props);
        this.state = {};
    }

    logout(){
        history.push('/logout');
    }


    render() {
        const { logout } = this.props;
        return (
            <Fragment>
                <header
                    className="isw-nav--header"
                    style={{
                        display: "flex",
                        color: "#fff",
                        justifyContent: "space-between",
                        alignItems: "center",
                        height: `${43}px`,
                        backgroundColor: "#00425f"
                    }}
                >
                    <div className="">header</div>
                    <button
                        onClick={this.changePassword}
                        style={{
                            border: `${5}px`,
                            color: "white",
                            backgroundColor: "green",
                            height: `${30}px`,
                            cursor: "pointer",
                            marginLeft: `${200}px`
                        }}
                    >
                        Change Password
                    </button>
                    <button
                        onClick={this.logout}
                        style={{
                            border: `${5}px`,
                            color: "white",
                            backgroundColor: "#0082c8",
                            width: `${100}px`,
                            height: `${30}px`,
                            cursor: "pointer",
                            marginLeft: `${200}px`
                        }}
                    >
                        Signout
                    </button>
                </header>
            </Fragment>
        );
    }
}

export default OvhHeader;





import { put, takeLatest, takeEvery, call } from "redux-saga/effects";
import { decryptAndRead } from "../services/localStorageHelper";
import { encryptAndStore } from "../services/localStorageHelper";
import Services from "../services/services";
import {
    READ_FAILURE,
    READ_SUCCESS,
    READ_EXPIRED,
    READ_STORAGE,
    UPDATE_TOKEN,
    ENCRYPT_USER
} from "../arsVariables";
import jwtDecode from "jwt-decode";
function* readStorage() {
    try {
        const data = decryptAndRead(ENCRYPT_USER);
        const { expired, access_token, ...rest } = data;
        const decoded = jwtDecode(access_token);
        const userRole = decoded.role;
        console.log({ data, userRole, decoded }, "decrypted");
        if (expired === false) {
            return yield put({ type: READ_SUCCESS, role: userRole, ...rest });
        } else if (expired === true) {
            // if the time is expired, clear the storage and add effect to attempt refreshing storage in stead
            // clear();
            return yield put({ type: READ_EXPIRED });
        } else {
            return yield put({ type: READ_FAILURE });
        }
    } catch (error) {
        return yield put({ type: READ_FAILURE });
    }
}

function* refreshStorage() {
    try {
        const storage = decryptAndRead(ENCRYPT_USER);
        const { expired, ...rest } = storage;
        const newUser = yield call(Services.refreshTokenService, { ...rest });
        const { success, data, error } = newUser;
        if (error === false && success === false) {
            return yield put({ type: READ_FAILURE });
        }
        if (error === false && success === true) {
            return yield put({ type: UPDATE_TOKEN, data });
        }
        if (error === true) {
            console.log(error);

            return yield put({ type: READ_FAILURE });
        }
    } catch (error) {
        console.log(error);
        return yield put({ type: READ_FAILURE, ...error });
    }
}

function* updateToken({ data }) {
    const storage = decryptAndRead(ENCRYPT_USER);
    storage.data.token = data.token;
    delete storage.expired;
    encryptAndStore(ENCRYPT_USER, { ...storage }, true);
    return yield put({ type: READ_SUCCESS, ...storage });
}

export default function* storageSaga() {
    yield takeEvery(READ_STORAGE, readStorage);
    yield takeLatest(READ_EXPIRED, refreshStorage);
    yield takeEvery(UPDATE_TOKEN, updateToken);
}

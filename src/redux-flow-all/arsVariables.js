/*
this houses every variable used in
- Actions

- Reducers

- Sagas

thus `${ars}Variables` 😄
*/

// GENERIC variables
export const SUCCESS = "SUCCESS";
export const FAILURE = "FAILURE";
export const RESET = "RESET";
export const API_KEY = "AIzaSyDeuD95hbs39ZsZPLS0vICN4tbezVCK0Kg";
export const EXPIRY = "EXPIRY";
export const ENCRYPT_USER = "USER";
export const SUPER_ADMIN = "SUPER_ADMIN";
export const VENDOR_ADMIN = "VENDOR_ADMIN";
export const CUSTOMER_ADMIN = "CUSTOMER_ADMIN";

// login variables
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";
export const LOGIN_USER = "LOGIN_USER";

// logout variables
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILURE = "LOGOUT_FAILURE";
export const LOGOUT_USER = "LOGOUT_USER";

// storage variables
export const READ_SUCCESS = "READ_SUCCESS";
export const READ_FAILURE = "READ_FAILURE";
export const READ_STORAGE = "READ_STORAGE";
export const READ_EXPIRED = "READ_EXPIRED";
export const UPDATE_TOKEN = "UPDATE_TOKEN";

// signup variables
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const SIGNUP_FAILURE = "SIGNUP_FAILURE";
export const SIGNUP_USER = "SIGNUP_USER";

// reset variables
export const PW_RESET = "PW_RESET";
export const PW_SUCCESS = "PW_RESET_SUCCESS";
export const PW_FAILURE = "PW_RESET_FAILURE";

// forgotPassword variables
export const FORGOT_RESET = "FORGOT_RESET";
export const FORGOT_SUCCESS = "FORGOT_RESET_SUCCESS";
export const FORGOT_FAILURE = "FORGOT_RESET_FAILURE";

// OTP variables
export const SEND_OTP = "SEND_OTP";
export const RESEND_OTP = "RESEND_OTP";
export const OTP_SUCCESS = "SEND_OTP_SUCCESS";
export const OTP_FAILURE = "SEND_OTP_FAILURE";

// verifyProfile varibales
export const VERIFY_PROFILE = "VERIFY_PROFILE";
export const VERIFY_SUCCESS = "VERIFY_SUCCESS";
export const VERIFY_FAILURE = "VERIFY_FAILURE";

// change password variables
export const CHANGE_FAILURE = "CHANGE_FAILURE";
export const CHANGE_SUCCESS = "CHANGE_SUCCESS";
export const CHANGE_PW = "CHANGE_PW";

// create customer user variables
export const CREATE_CUS_USER = "CREATE_CUSTOMER_USER";
export const CREATE_CUS_SUCCESS = "CREATE_CUSTOMER_SUCCESS";
export const CREATE_CUS_FAILURE = "CREATE_CUSTOMER_FAILURE";

// create customer location variables
export const CREATE_LOC_USER = "CREATE_LOCATION_USER";
export const CREATE_LOC_SUCCESS = "CREATE_LOCATION_SUCCESS";
export const CREATE_LOC_FAILURE = "CREATE_LOCATION_FAILURE";

// edit profile basic info variables
export const EDIT_BASIC_INFO = "EDIT_BASIC_INFO";
export const EDIT_BASIC_SUCCESS = "EDIT_BASIC_SUCCESS";
export const EDIT_BASIC_FAILURE = "EDIT_BASIC_FAILURE";

// get location variables
export const GET_LOCATION = "GET_LOCATION";
export const GET_LOCATION_SUCCESS = "GET_LOCATION_SUCCESS";
export const GET_LOCATION_FAILURE = "GET_LOCATION_FAILURE";

// get locations variables
export const GET_LOCATIONS = "GET_LOCATIONS";
export const GET_LOCATIONS_SUCCESS = "GET_LOCATIONS_SUCCESS";
export const GET_LOCATIONS_FAILURE = "GET_LOCATIONS_FAILURE";

// get users created by the CUSTOMER ADMIN variables
export const GET_USERS = "GET_USERS";
export const GET_USERS_SUCCESS = "GET_USERS_SUCCESS";
export const GET_USERS_FAILURE = "GET_USERS_FAILURE";

// get users created by the CUSTOMER ADMIN variables
export const GET_CUSTOMERS = "GET_CUSTOMERS";
export const GET_CUSTOMERS_SUCCESS = "GET_CUSTOMERS_SUCCESS";
export const GET_CUSTOMERS_FAILURE = "GET_CUSTOMERS_FAILURE";

/**
 *
 * PERMISSIONs and ROLES variables BELOW
 *
 * */

// GET permissions
// get permissions
export const GET_PERMISSIONS = "GET_PERMISSIONS";
export const GET_PERMISSIONS_SUCCESS = "GET_PERMISSIONS_SUCCESS";
export const GET_PERMISSIONS_FAILURE = "GET_PERMISSIONS_FAILURE";

// get remove user permissions
export const GET_RM_USER_PERMISSIONS = "GET_REMOVE_USER_PERMISSIONS";
export const GET_RM_USER_PERMISSIONS_SUCCESS =
    "GET_REMOVE_USER_PERMISSIONS_SUCCESS";
export const GET_RM_USER_PERMISSIONS_FAILURE =
    "GET_REMOVE_USER_PERMISSIONS_FAILURE";

// get roles
export const GET_ROLES = "GET_ROLES";
export const GET_ROLES_SUCCESS = "GET_ROLES_SUCCESS";
export const GET_ROLES_FAILURE = "GET_ROLES_FAILURE";

// get user permissions
export const GET_USER_PERMISSIONS = "GET_USER_PERMISSIONS";
export const GET_USER_PERMISSIONS_SUCCESS = "GET_USER_PERMISSIONS_SUCCESS";
export const GET_USER_PERMISSIONS_FAILURE = "GET_USER_PERMISSIONS_FAILURE";

// POST permissions
// batch update user permissions
export const BATCH_UU_PERMISSIONS = "BATCH_UPDATE_USER_PERMISSIONS";
export const BATCH_UU_PERMISSIONS_SUCCESS =
    "BATCH_UPDATE_USER_PERMISSIONS_SUCCESS";
export const BATCH_UU_PERMISSIONS_FAILURE =
    "BATCH_UPDATE_USER_PERMISSIONS_FAILURE";

// change user role
export const CHANGE_USER_ROLE = "CHANGE_USER_ROLE";
export const CHANGE_USER_ROLE_SUCCESS = "CHANGE_USER_ROLE_SUCCESS";
export const CHANGE_USER_ROLE_FAILURE = "CHANGE_USER_ROLE_FAILURE";

// remove user permission
export const RM_USER_PERMISSION = "REMOVE_USER_PERMISSION";
export const RM_USER_PERMISSION_SUCCESS = "REMOVE_USER_PERMISSION_SUCCESS";
export const RM_USER_PERMISSION_FAILURE = "REMOVE_USER_PERMISSION_FAILURE";

// remove user permissions
export const RM_USER_PERMISSIONS = "REMOVE_USER_PERMISSIONS";
export const RM_USER_PERMISSIONS_SUCCESS = "REMOVE_USER_PERMISSIONS_SUCCESS";
export const RM_USER_PERMISSIONS_FAILURE = "REMOVE_USER_PERMISSIONS_FAILURE";

// update user permissions
export const UU_PERMISSIONS = "UPDATE_USER_PERMISSIONS";
export const UU_PERMISSIONS_SUCCESS = "UPDATE_USER_PERMISSIONS_SUCCESS";
export const UU_PERMISSIONS_FAILURE = "UPDATE_USER_PERMISSIONS_FAILURE";

/**
 *
 *
 * ISW variables below
 *
 */

// create vendor  variables
export const CREATE_VENDOR = "CREATE_VENDOR_";
export const CREATE_VENDOR_SUCCESS = "CREATE_VENDOR_SUCCESS";
export const CREATE_VENDOR_FAILURE = "CREATE_VENDOR_FAILURE";

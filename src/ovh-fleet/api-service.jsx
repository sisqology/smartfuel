import {history} from "./history";
import {decryptAndRead} from "../redux-flow-all/services/localStorageHelper";
import {ENCRYPT_USER} from "../redux-flow-all/arsVariables";
import {Actions} from "../redux-flow-all/actions";

const axios = require('axios');

const token = localStorage.getItem('USER');

const header = token => {

    if (token) {
        return {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };
    } else {
        return {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };
    }
};

export class ApiService {

    static request(url, type, data, noStringify = false) {
        let bodyData;
        let service;
        bodyData = noStringify ? JSON.stringify(data) : data;
        const decryptedToken = decryptAndRead(ENCRYPT_USER).access_token;
        if (type.toLowerCase() === 'get') {
            axios.defaults.headers.common['Authorization'] = 'Bearer '+decryptedToken;
            service = axios.get(url, {params: bodyData});
            return service.then(function (response) {
                return service;
            }).catch(function (error) {
                if (error.response.status === 401 || error.response.status.includes('401')) {
                    history.push('/signin');

                    // dispatch(Actions.logoutUser());
                } else {
                    return service;
                }
            });

        } else {
            let config = {
                    "Content-Type": "application/json",
                    "Authorization": 'Bearer '+decryptedToken
            };
            service = axios.post(url, bodyData, {headers: config});
            return service.then(function (response) {
                console.log(response);
                return service;
            })//.catch((err) => {console.log("REJECTED"); console.log(err.response.status);});
            .catch(function (error) {
                if (error.response.status === 401 || error.response.status.includes('401')) {
                    history.push('/signin');

                    // dispatch(Actions.logoutUser());
                } else {
                    return service;
                }
            })
        }
    }

}
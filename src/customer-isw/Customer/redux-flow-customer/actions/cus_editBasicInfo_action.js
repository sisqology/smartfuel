import { EDIT_BASIC_INFO } from "../../../../redux-flow-all/arsVariables";

const cus_editBasicInfo = payload => ({
    type: EDIT_BASIC_INFO,
    payload
});

export default cus_editBasicInfo;

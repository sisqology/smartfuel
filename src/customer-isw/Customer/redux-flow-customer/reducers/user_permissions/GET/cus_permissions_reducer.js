import {
    GET_PERMISSIONS_FAILURE,
    GET_PERMISSIONS_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../../../redux-flow-all/arsVariables";

const cus_permissions_reducer = (state = {}, action) => {
    const { type, message, payloadReturned } = action;
    switch (type) {
        case GET_PERMISSIONS_SUCCESS:
            return {
                ...state,
                get_permissions: SUCCESS,
                get_permissions_data: message,
                payload: payloadReturned
            };
        case GET_PERMISSIONS_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                get_permissions: FAILURE,
                get_permissions_data: message
                    ? message
                    : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_permissions_reducer;

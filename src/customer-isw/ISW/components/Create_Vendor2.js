import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { ToastContainer, toast } from "react-toastify";

// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";
import Sidebar from "../../../reuse/sidebar";
import { Actions } from "../../../redux-flow-all/actions/index";
import {
    areDigits,
    validateEmail,
    validateNumber
} from "../../../reuse/regexes";
import { history } from "../../../reuse/history";
import { SUCCESS, FAILURE } from "../../../redux-flow-all/arsVariables";
// Assets
class CreateVendor2 extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            phone: "",
            f_name: "",
            l_name: "",
            password: "",
            validEmail: null,
            validPhone: null,
            validPassword: null,
            role: null,
            pending: false,
            errowShown: false,
            redirect: false
        };
    }

    componentDidMount() {
        const { allowed, role, fakeAuth } = this.props;
        const { location } = history;
        console.log(this.props, {
            location
        });

        if (role !== allowed && fakeAuth.isAuthenticated) {
            this.setState({ redirect: true });
            console.log("HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        } else {
            if (location) {
                const { STATE } = location.state;
                console.log({ STATE }, this.state);
                this.setState({
                    ...this.state,
                    ...STATE
                });
            }
        }
    }

    static showToast = (message, type) => {
        if (toast.isActive(CreateVendor2.toastId)) {
            return toast.update(CreateVendor2.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (CreateVendor2.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (CreateVendor2.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const { create_vendor, create_vendor_data } = props;
        // console.log({ create_vendor_data });

        // if create_vendor is successful
        if (
            create_vendor === SUCCESS &&
            errorShown === false &&
            pending === true
        ) {
            CreateVendor2.showToast(create_vendor_data, "success");
            return {
                pending: false,
                errorShown: true
            };
        }

        // if create_vendor fails
        if (
            create_vendor === FAILURE &&
            pending === true &&
            errorShown === false
        ) {
            CreateVendor2.showToast(create_vendor_data, "error");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }
        return state;
    };

    onEmail_Change = ({ target }) => {
        const { value } = target;
        const isValidEmail = validateEmail(value);

        this.setState({
            email: value,
            validEmail: isValidEmail ? "true" : "false"
        });
    };
    onFirstname_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            f_name: value,
            validFN: value.length > 1 ? "true" : "false"
        });
    };
    onLastname_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            l_name: value,
            validLN: value.length > 1 ? "true" : "false"
        });
    };
    onPhone_Change = ({ target }) => {
        const { value } = target;

        const isValidPhone = validateNumber(value);

        if (areDigits(value))
            this.setState({
                phone: value,
                validPhone: isValidPhone ? "true" : "false"
            });
    };

    onPassword_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            password: value,
            validPassword: value.length > 6 ? "true" : "false"
        });
    };

    onSubmit = e => {
        e.preventDefault();
        const {
            email,
            f_name,
            l_name,
            phone,
            password,
            company_name,
            company_email,
            company_contact,
            company_phone,
            company_address,
            company_website
        } = this.state;

        this.setState({ pending: true, errorShown: false });

        const vendorInfo = {
            title: company_name,
            emailAddress: company_email,
            contactName: company_contact,
            phoneNo: company_phone,
            address: company_address,
            websiteUrl: company_website
        };

        const vendorAdmin = {
            emailAddress: email,
            firstName: f_name,
            lastName: l_name,
            phoneNo: phone,
            password: password,
            role: 1
        };

        this.props.dispatch(
            Actions.isw_createVendor({
                vendorAdmin,
                vendorInfo
            })
        );
    };

    goBack = () => {
        const STATE = this.state;
        history.push({
            pathname: "/isw/create-vendor",
            state: {
                STATE
            }
        });
    };
    render() {
        const {
            validEmail,
            validPhone,
            email,
            phone,
            password,
            f_name,
            l_name,
            validPassword,
            redirect
        } = this.state;

        const conditions =
            validPhone === "true" &&
            validPassword === "true" &&
            validEmail === "true" &&
            (f_name && l_name) !== "";

        if (redirect) {
            return <Redirect to={{ pathname: "/#/403" }} />;
        }
        return (
            <Fragment>
                {/* <div id="">
                    <Header logout={this.logout} />
                </div> */}

                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000} />
                    <aside className="isw-sideBar">
                        <Sidebar type="ISW" />
                    </aside>
                    <div />
                    <div className="isw-content--wrapper">
                        <div>
                            <form className="content" id="content-body">
                                <div className="container-fluid container-limited">
                                    <div
                                        className="row"
                                        style={{
                                            justifyContent: "center"
                                        }}
                                    >
                                        <div className="col-lg-9">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">
                                                        New Staff
                                                    </h3>
                                                    <div className="isw-card-p">
                                                        Personal information of
                                                        the user managing this
                                                        account
                                                    </div>
                                                </div>
                                                <div
                                                    className="card-body p-3"
                                                    style={{
                                                        paddingTop: `${0} !important`
                                                    }}
                                                >
                                                    <div className="row">
                                                        <div className="col-lg-8">
                                                            <div className="row">
                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField1"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="First Name"
                                                                                onChange={
                                                                                    this
                                                                                        .onFirstname_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onFirstname_Change
                                                                                }
                                                                                value={
                                                                                    f_name
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField2"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Last Name"
                                                                                onChange={
                                                                                    this
                                                                                        .onLastname_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onLastname_Change
                                                                                }
                                                                                value={
                                                                                    l_name
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-12">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField4"
                                                                                type="text"
                                                                                className={
                                                                                    validEmail ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validEmail ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                                placeholder="Email Address"
                                                                                onChange={
                                                                                    this
                                                                                        .onEmail_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onEmail_Change
                                                                                }
                                                                                value={
                                                                                    email
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField3"
                                                                                type="text"
                                                                                placeholder="Phone Number"
                                                                                onChange={
                                                                                    this
                                                                                        .onPhone_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onPhone_Change
                                                                                }
                                                                                value={
                                                                                    phone
                                                                                }
                                                                                className={
                                                                                    validPhone ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validPhone ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField222"
                                                                                onChange={
                                                                                    this
                                                                                        .onPassword_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onPassword_Change
                                                                                }
                                                                                onFocus={
                                                                                    this
                                                                                        .onPassword_Change
                                                                                }
                                                                                value={
                                                                                    password
                                                                                }
                                                                                className={
                                                                                    validPassword ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validPassword ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                                type="password"
                                                                                placeholder="Enter password"
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <input
                                                            className="col-lg-4"
                                                            placeholder="Profile Image"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        className="isw-card mt-4"
                                        style={{
                                            justifyContent: "center",
                                            display: "flex",
                                            maxWidth: `${74}%`,
                                            margin: `0 auto`,
                                            padding: 0
                                        }}
                                    >
                                        <div className="card-body p-3 row">
                                            <div className="col-lg-4">
                                                <button
                                                    onClick={this.onSubmit}
                                                    disabled={
                                                        conditions
                                                            ? false
                                                            : true
                                                    }
                                                    className={
                                                        conditions
                                                            ? `isw-btn isw-btn--raised bg-primary text-white w-100`
                                                            : `isw-btn  bg-primary text-white w-100 false`
                                                    }
                                                >
                                                    <span>Create</span>
                                                </button>
                                            </div>

                                            <div className="col-lg-4">
                                                <button
                                                    onClick={this.goBack}
                                                    className="isw-btn isw-btn--raised text-primary w-100"
                                                >
                                                    <span>Back</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    const {
        create_vendor,
        create_vendor_data
    } = state.isw_createVendor_reducer;
    return {
        create_vendor,
        create_vendor_data
    };
};

export default connect(mapStateToProps)(CreateVendor2);

import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { Actions } from "../../redux-flow-all/actions/index";
import { ToastContainer, toast } from "react-toastify";

// styles
import "../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";
import "../../scss/pending.scss";

import Static from "../../reuse/Static";
import {
    VENDOR_ADMIN,
    CUSTOMER_ADMIN,
    SUPER_ADMIN,
    SUCCESS,
    FAILURE
} from "../../redux-flow-all/arsVariables";
import { history } from "../../reuse/history";
import { validateEmail } from "../../reuse/regexes";
// Assets
class Login extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            pending: false,
            redirectToReferrer: false,
            errorShown: false,
            email: "",
            password: ""
        };
    }

    componentDidMount() {
        const { fakeAuth, history } = this.props;
        console.log({ fakeAuth, history });
        if (fakeAuth.isAuthenticated) {
            history.goBack(history.location.pathname);
        }
    }

    static showToast = message => {
        if (toast.isActive(Login.toastId)) {
            return toast.update(Login.toastId, message);
        }
        return (Login.toastId = toast.error(message, {
            position: toast.POSITION.TOP_RIGHT,
            className: "error"
        }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown, email } = state;
        const { login, data, fakeAuth, role, access } = props;

        console.log({ props });
        // if login is successful
        if (login === SUCCESS && errorShown === false && pending === true) {
            fakeAuth.authenticate();
            if (role === SUPER_ADMIN) {
                history.push({
                    pathname: "/isw/create-vendor",
                    state: { role }
                });
            } else if (role === CUSTOMER_ADMIN) {
                history.push({
                    pathname: "/customer/user-list",
                    state: { role }
                });
            } else if (role === VENDOR_ADMIN) {
                history.push({
                    pathname: "/vendor/dashboard",
                    state: { role }
                });
            }
            // props.dispatch(Actions.reset());
            return {
                redirectToReferrer: true,
                pending: false,
                errorShown: true
            };
        }

        // if login fails
        if (
            login === FAILURE &&
            errorShown === false &&
            pending === true &&
            access === null
        ) {
            Login.showToast(data);
            props.dispatch(Actions.reset());
            return {
                redirectToReferrer: false,
                pending: false,
                errorShown: true
            };
        }
        // if access denied
        if (
            login === "FAILURE" &&
            errorShown === false &&
            pending === true &&
            access
        ) {
            Login.showToast(data);
            // props.dispatch(Actions.reset());
            setTimeout(() => {
                history.push({
                    pathname: "/customer/verify-profile",
                    state: {
                        email
                    }
                });
            }, 2000);
            return {
                redirectToReferrer: false,
                pending: false,
                errorShown: true
            };
        }
        return null;
    };

    // handle email input function
    onEmailChange = e => {
        this.setState({ email: e.target.value });
    };

    // handle password input function
    onPasswordChange = e => {
        this.setState({ password: e.target.value });
    };

    onLogin = e => {
        e.preventDefault();
        const { email, password } = this.state;
        this.setState({ pending: true, errorShown: false });

        const isValidEmail = validateEmail(email);

        if (isValidEmail && password !== "") {
            return this.props.dispatch(Actions.loginUser({ email, password }));
        } else {
            this.setState({ pending: false, errorShown: true });
            return Login.showToast("Input a valid Email/Password");
        }
    };

    render() {
        const { location } = this.props;
        const { redirectToReferrer, email, pending, password } = this.state;
        // const { from } = location.state || {
        //     from: { pathname: "/customer" }
        // };

        // if (redirectToReferrer) {
        //     return <Redirect push to={from} />;
        // }

        return (
            // <main role="main">
            <div className="isw-login">
                <ToastContainer autoClose={5000} />
                <div className="container-fluid p-0">
                    <div className="row no-gutters">
                        <Static />

                        <div className="col-lg-6">
                            <div
                                className="isw-login--middle"
                                style={{
                                    overflowY: "auto"
                                    // displaY: "flex",
                                    // justifyContent: "center"
                                }}
                            >
                                <form
                                    className="row"
                                    style={{
                                        maxWidth: `${30}rem`,
                                        margin: `${0} auto`
                                    }}
                                    action="#"
                                >
                                    <div className="col-12">
                                        <header>
                                            <h1 className="text-primary">
                                                Welcome to Smartfuel
                                            </h1>
                                            <p>
                                                Sign in by entering the
                                                information below
                                            </p>
                                        </header>
                                    </div>

                                    <div className="col-12">
                                        <div className="form-field mb-5">
                                            <div className="form-field__control">
                                                <input
                                                    className="form-field__input k_input"
                                                    id="exampleField0"
                                                    type="email"
                                                    placeholder="Enter email"
                                                    onChange={
                                                        this.onEmailChange
                                                    }
                                                    onBlur={this.onEmailChange}
                                                    autoFocus={
                                                        email === ""
                                                            ? true
                                                            : false
                                                    }
                                                    value={email}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-12">
                                        <div className="form-field mb-4">
                                            <div className="form-field__control">
                                                <input
                                                    className="form-field__input k_input"
                                                    id="exampleField3"
                                                    type="password"
                                                    placeholder="Enter password"
                                                    onChange={
                                                        this.onPasswordChange
                                                    }
                                                    onBlur={
                                                        this.onPasswordChange
                                                    }
                                                    value={password}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-12 mb-4">
                                        <div className="d-flex justify-content-between">
                                            {pending ? (
                                                <div className="d-inline-block">
                                                    Forgot Password?
                                                </div>
                                            ) : (
                                                <div className="d-inline-block">
                                                    <Link
                                                        to="/forgotPassword"
                                                        style={{
                                                            color: "#00425f"
                                                        }}
                                                    >
                                                        Forgot Password?
                                                    </Link>
                                                </div>
                                            )}
                                        </div>
                                    </div>

                                    <div className="col-12">
                                        {pending ? (
                                            <button
                                                className="dot isw-btn bg-primary text-white w-100"
                                                disabled
                                            >
                                                Loading
                                                <span>.</span>
                                                <span>.</span>
                                                <span>.</span>
                                            </button>
                                        ) : (
                                            <button
                                                onClick={this.onLogin}
                                                className="isw-btn isw-btn--raised bg-primary text-white w-100"
                                            >
                                                <span>Sign in</span>
                                            </button>
                                        )}

                                        <div className="mt-4 text-center">
                                            <p>
                                                Don't have an account yet?{" "}
                                                {pending ? (
                                                    <span className="text-primary font-weight-bold">
                                                        Sign up
                                                    </span>
                                                ) : (
                                                    <Link
                                                        className="text-primary font-weight-bold"
                                                        to="/customer/signup"
                                                    >
                                                        Sign up
                                                    </Link>
                                                )}
                                            </p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div className="col-lg-2">
                            <div className="isw-column--right" />
                        </div>
                    </div>
                </div>
            </div>
            // </main>
        );
    }
}

const mapStateToProps = state => {
    console.log({ state });
    const { login, data, role, access } = state.login_reducer;
    return {
        role,
        login,
        data,
        access
    };
};

export default connect(mapStateToProps)(Login);

import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../../../redux-flow-all/services/services";

import {
    CHANGE_USER_ROLE,
    CHANGE_USER_ROLE_FAILURE,
    CHANGE_USER_ROLE_SUCCESS
} from "../../../../../../redux-flow-all/arsVariables";

function* changeUserRole({ payload }) {
    try {
        const returnedData = yield call(Services.signupService, payload);
        const { error, status, data } = returnedData;
        const { code, description } = data;
        console.log({ returnedData, data });
        if (error === false && status === 200 && code === 1) {
            return yield put({
                type: CHANGE_USER_ROLE_SUCCESS,
                message: description
            });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({
                type: CHANGE_USER_ROLE_FAILURE,
                message: description
            });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({
                type: CHANGE_USER_ROLE_FAILURE,
                message: description
            });
        }
    } catch (error) {
        console.log({ error });

        const { response } = error;
        if (response) {
            const { data, status } = response;
            if (status === 400) {
                return yield put({
                    type: CHANGE_USER_ROLE_FAILURE,
                    message: data.error_description
                });
            }
        } else if (response === undefined) {
            return yield put({
                type: CHANGE_USER_ROLE_FAILURE,
                message: "Failed to connect"
            });
        }
    }
}

export default function* cus_changeUserRoleSaga() {
    yield takeLatest(CHANGE_USER_ROLE, changeUserRole);
}

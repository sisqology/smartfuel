
import { BATCH_UU_PERMISSIONS } from "../../../../../../redux-flow-all/arsVariables";

const cus_batchUpdateUserPermissions = payload => ({
    type: BATCH_UU_PERMISSIONS,
    payload
});

export default cus_batchUpdateUserPermissions;

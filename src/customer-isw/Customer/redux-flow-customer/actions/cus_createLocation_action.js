import { CREATE_LOC_USER } from "../../../../redux-flow-all/arsVariables";

const cus_createLocation = payload => ({
    type: CREATE_LOC_USER,
    payload
});

export default cus_createLocation;

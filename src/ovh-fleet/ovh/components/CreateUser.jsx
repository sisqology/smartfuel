import * as React from "react";
import {Fragment} from "react";
import OvhHeader from "../templates/header";
import {ToastContainer} from "react-toastify";
import OvhMenu from "../templates/menu";
import { connect } from "react-redux";
import {postUser} from "../redux-flow-ovh/actions/users.actions";
import {showToast} from "../../shared/Toast";
import * as OvhConstants from "../redux-flow-ovh/ovh-constants";
import {history} from "../../../reuse/history";


class CreateVendorUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        const { dispatch } = this.props;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });

    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { firstName, lastName, phone, email, usertype } = this.state;
        const { dispatch } = this.props;
        if (firstName && lastName && email && phone && usertype) {
            let payload = {
                firstName: firstName,
                lastName: lastName,
                PhoneNo: phone,
                emailAddress: email,
                role: usertype
            };
            dispatch(postUser(payload));

            // {showToast("Unable to fetch users", "error")}
        }
        else{
            this.setState({ submitted: false });
        }
    }

    checkStatus(resp){
        if(this.state.submitted) {
            if (resp.create_vendor.create_vendor_status === OvhConstants.CREATE_VENDOR_USER_FAILURE) {
                this.setState({ submitted: false });
                showToast(resp.create_vendor.create_vendor_data.error, "error");
            }
            if (resp.create_vendor.create_vendor_status === OvhConstants.CREATE_VENDOR_USER_SUCCESS) {
                showToast(resp.create_vendor.create_vendor_data.response.data.description, "success");
                history.push('/vendor/users');
            }
        }
    }

    render() {
        const resp = this.props;
        console.error(resp);
        this.checkStatus(resp);
        let submitted = this.state.submitted;
        return (
            <Fragment>
                <div id="">
                    <OvhHeader />
                </div>

                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000} />
                    <OvhMenu />
                    <div></div>
                    <div className="isw-content--wrapper">
                        <div>
                            <form className="content" id="content-body" onSubmit={this.handleSubmit}>
                                <div className="container-fluid container-limited">
                                    <div className="row">
                                        <div className="col-lg-9 mx-auto">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">New User</h3>
                                                    <div className="isw-card-p">Enter users details</div>
                                                </div>
                                                <div className="card-body p-3" style={{ paddingTop: 0 }}>
                                                    <div className="row">
                                                        <div className="col-lg-12">
                                                            <div className="row">

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">

                                                                            <input id="exampleField1" type="text" placeholder="First Name"
                                                                                   className="form-field__input" name="firstName" onChange={this.handleChange}/>
                                                                            {submitted && !this.state.firstName &&
                                                                            <div className="text-danger">First Name is required</div>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">

                                                                            <input id="exampleField2" type="text" placeholder="Last Name"
                                                                                   className="form-field__input" name="lastName" onChange={this.handleChange}/>
                                                                            {submitted && !this.state.lastName &&
                                                                            <div className="text-danger">Last Name is required</div>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">

                                                                            <input id="exampleField3" type="text" placeholder="Phone Number"
                                                                                   className="form-field__input" name="phone" onChange={this.handleChange}/>
                                                                            {submitted && !this.state.phone &&
                                                                            <div className="text-danger">Phone is required</div>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">

                                                                            <input id="exampleField4" type="text" placeholder="Email Address"
                                                                                   className="form-field__input" name="email" onChange={this.handleChange}/>
                                                                            {submitted && !this.state.email &&
                                                                            <div className="text-danger">Email is required</div>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-12">
                                                                    <div className="mb-4">
                                                                        <div className="form-select">
                                                                            <select className="select-text" name="usertype" onChange={this.handleChange}>
                                                                                <option value="">Select User Type</option>
                                                                                <option value="5">Procurement</option>
                                                                                <option value="4">Logistics</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="isw-card mt-4">
                                                <div className="card-body p-3 row">
                                                    <div className="col-lg-4">
                                                        <button disabled={submitted}
                                                            className="isw-btn isw-btn--raised bg-primary text-white w-100">
                                                            <span>{ submitted ? "Processing..." : "Save" }</span>
                                                        </button>
                                                    </div>

                                                    <div className="col-lg-4">
                                                        <button
                                                            className="isw-btn isw-btn--outlined text-primary w-100">
                                                            <span>Cancel</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state){
    return {
        create_vendor: state. ven_create_vendor,
    };
}

export default connect(mapStateToProps)(CreateVendorUser);
// export default connect(null)(CreateUser);

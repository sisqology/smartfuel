import {
    RM_USER_PERMISSIONS_FAILURE,
    RM_USER_PERMISSIONS_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../../../redux-flow-all/arsVariables";

const cus_removeUserPermissions_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case RM_USER_PERMISSIONS_SUCCESS:
            return {
                ...state,
                remove_ups: SUCCESS,
                remove_ups_data: message
            };
        case RM_USER_PERMISSIONS_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                remove_ups: FAILURE,
                remove_ups_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_removeUserPermissions_reducer;

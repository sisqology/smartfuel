import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { Actions } from "../redux-flow-all/actions/index";
import { ToastContainer, toast } from "react-toastify";
import {
    SUCCESS,
    FAILURE,
    SUPER_ADMIN,
    CUSTOMER_ADMIN,
    VENDOR_ADMIN
} from "../redux-flow-all/arsVariables";
import { history } from "../reuse/history";
import { fakeAuth } from "../App";
import Header from "./header";

class Layout extends Component {
    toastId = null;

    state = { redirect: false };

    componentDidMount() {
        const { dispatch, allowed, role } = this.props;
        dispatch(Actions.onInit());
        const { location } = history;
        console.log({ history, fakeAuth, allowed });
        // if (location.state.from === undefined) {
        //     return;
        // } else {
        //     if (location.state.from.hash.includes("#")) {
        //         history.push("/#/404");
        //     }
        // }

        // if (allowed !== role && fakeAuth.isAuthenticated) {
        //     history.push("/#/403");
        // }
        // if (allowed !== role) {
        //     history.push("/#/403");
        // }

        if (
            fakeAuth.isAuthenticated &&
            history.location.pathname.includes("403")
        ) {
            history.push({ path: history.location.pathname });
        }
    }

    logout = () => {
        this.props.dispatch(Actions.logoutUser());
    };

    static showToast = (message, type) => {
        if (toast.isActive(Layout.toastId)) {
            return toast.update(Layout.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (Layout.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (Layout.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { storage, logout, role, allowed } = props;
        console.log({ storage, fakeAuth, logout, role, allowed });

        const { location } = history;
        console.log({ history, fakeAuth });
        // if (location.state.from === undefined) {
        //     return;
        // } else {
        //     if (location.state.from.hash.includes("#")) {
        //         history.push("/#/404");
        //     }
        // }

        console.log({ props });
        if (storage === SUCCESS && logout === undefined) {
            console.log(props, history);
            if (role !== allowed) {
                return { redirect: true };
            }
            // if (fakeAuth.isAuthenticated) {
            //     if (role === SUPER_ADMIN) {
            //         history.goBack("/isw/vendor");
            //     } else if (role === CUSTOMER_ADMIN) {
            //         history.goBack("/customer/user-list");
            //     } else if (role === VENDOR_ADMIN) {
            //         history.goBack("/vendor/");
            //     }
            // }
            return state;
        }
        if (logout === SUCCESS) {
            console.log("na 💀");
            Layout.showToast("Logout Successful", "success");
            setTimeout(() => {
                window.location.reload(true);
            }, 500);
            // props.dispatch(Actions.reset());
        }
        if (logout === FAILURE) {
            console.log("na 💀");
            Layout.showToast("Logout UnSuccessful", "error");
            props.dispatch(Actions.reset());
        }
        return state;
    };

    render() {
        // component and props passed are accessed
        const { children, fullScreen } = this.props;

        if (this.state.redirect) {
            return <Redirect to={{ pathname: "/#/403" }} />;
        }
        if (fullScreen === true) {
            return (
                <Fragment>
                    <ToastContainer autoClose={5000} />

                    {/* component is rendered as children */}
                    {children}
                </Fragment>
            );
        } else {
            return (
                <Fragment>
                    <Header logout={this.logout} />

                    <ToastContainer autoClose={5000} />
                    {/* component is rendered as children */}
                    {children}
                </Fragment>
            );
        }
    }
}

const mapStateToProps = state => {
    const { storage, data, role: roleFromStorage } = state.storage_reducer;
    const { role: roleFromLogin } = state.login_reducer;
    const { logout } = state.logout_reducer;

    return {
        storage,
        data,
        logout,
        role: roleFromLogin || roleFromStorage
    };
};

export default connect(mapStateToProps)(Layout);

import {
    CREATE_LOC_SUCCESS,
    CREATE_LOC_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_createLocation_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case CREATE_LOC_SUCCESS:
            return {
                ...state,
                create_location: SUCCESS,
                create_location_data: message
            };
        case CREATE_LOC_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                create_location: FAILURE,
                create_location_data: message
                    ? message
                    : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_createLocation_reducer;

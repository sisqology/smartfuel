import {
    EDIT_BASIC_SUCCESS,
    EDIT_BASIC_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_editBasicInfo_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case EDIT_BASIC_SUCCESS:
            return {
                ...state,
                edit_basic_info: SUCCESS,
                edit_basic_info_data: message
            };
        case EDIT_BASIC_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                edit_basic_info: FAILURE,
                edit_basic_info_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_editBasicInfo_reducer;

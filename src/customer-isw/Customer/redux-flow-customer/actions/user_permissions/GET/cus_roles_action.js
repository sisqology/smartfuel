import { GET_ROLES } from "../../../../../../redux-flow-all/arsVariables";

const cus_roles = payload => ({
    type: GET_ROLES,
    payload
});

export default cus_roles;

import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import createSagaMiddleware from "redux-saga";

import indexReducer from "./reducers/index";
import rootSaga from "./sagas/sagas";
import rootReducer from "../ovh-fleet/rootReducer";
import thunkMiddleware from "redux-thunk";

const sagaMiddleware = createSagaMiddleware();

export default function(data) {
    const finalCreateStore = applyMiddleware(sagaMiddleware, thunkMiddleware, logger)(
        createStore
    );
    const store = finalCreateStore(rootReducer, data);
    sagaMiddleware.run(rootSaga);

    return store;
}

import { CREATE_CUS_USER } from "../../../../redux-flow-all/arsVariables";

const cus_createUser = payload => ({
    type: CREATE_CUS_USER,
    payload
});

export default cus_createUser;

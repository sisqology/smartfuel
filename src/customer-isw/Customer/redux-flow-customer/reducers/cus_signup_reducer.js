import {
    SIGNUP_SUCCESS,
    SIGNUP_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";
const { localStorage } = window;

const cus_signup_reducer = (state = {}, action) => {
    const { type, message } = action;
    console.log({ message });
    switch (type) {
        case SIGNUP_SUCCESS:
            return {
                ...state,
                signup: SUCCESS,
                data: message,
                email: localStorage.getItem("email"),
                phone: localStorage.getItem("phone")
            };
        case SIGNUP_FAILURE:
            return {
                ...state,
                signup: FAILURE,
                data: message ? message : "Bad Network Connectivity"
            };
        case RESET:
            return {
                email: localStorage.getItem("email"),
                phone: localStorage.getItem("phone")
            };
        default:
            return state;
    }
};

export default cus_signup_reducer;

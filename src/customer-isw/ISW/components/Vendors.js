import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";

// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";

import Leye from "../../../assets/images/isw-imageBox.png";
import { Actions } from "../../../redux-flow-all/actions/index";
import Sidebar from "../../../reuse/sidebar";

import { IconAddContact, IconFilter } from "../../../reuse/svgs";
import { FAILURE, SUCCESS } from "../../../redux-flow-all/arsVariables";
import { history } from "../../../reuse/history";
import { customerTester } from "../../../reuse/authorization_test";
// Assets
class Vendor extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            pending: false,
            errorShown: false,
            redirect: false,
            users: []
        };
    }

    componentDidMount() {
        const { dispatch, allowed, role, fakeAuth } = this.props;
        // const { role: fromState } = this.props.location.state;
        console.log(this.props, "KAMSIIIIIIIII", {
            history,
            allowed,
            role
            // fromProps,
            // fromState
        });

        // const role = fromState || fromProps;

        if (role === allowed && fakeAuth.isAuthenticated) {
            this.setState({ pending: true, errorShown: false });
            dispatch(Actions.cus_getUsers());
        } else if (role !== allowed && fakeAuth.isAuthenticated) {
            this.setState({ redirect: true });
            console.log("HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    static showToast = (message, type) => {
        if (toast.isActive(Vendor.toastId)) {
            return toast.update(Vendor.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (Vendor.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (Vendor.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const { fakeAuth, get_users, get_users_data, payload } = props;

        console.log({ props, state, payload });

        // if create is successful
        if (get_users === SUCCESS && errorShown === false && pending === true) {
            // Vendor.showToast(get_users_data, "success");
            props.dispatch(Actions.reset());
            return {
                users: payload,
                pending: false,
                errorShown: true
            };
        }

        // if create fails
        if (get_users === FAILURE && errorShown === false && pending === true) {
            if (get_users_data === null) {
                Vendor.showToast("Failed to fetch list", "error");
            }
            Vendor.showToast(get_users_data, "error");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }
        // if access denied
        if (get_users === FAILURE && errorShown === false && pending === true) {
            Vendor.showToast(get_users_data, "error");
            props.dispatch(Actions.reset());

            return {
                redirectToReferrer: false,
                pending: false,
                errorShown: true
            };
        }

        return null;
    };

    routeToCreateVendor = () => {
        history.push("/customer/create-vendor");
    };
    render() {
        const { users, redirect } = this.state;

        if (redirect) {
            return <Redirect to={{ pathname: "/#/403" }} />;
        }
        return (
            <Fragment>
                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000} />
                    <aside className="isw-sideBar">
                        <Sidebar type="ISW" />
                    </aside>
                    <div />
                    <div className="isw-content--wrapper">
                        <div>
                            <div className="content" id="content-body">
                                <div className="container-fluid container-limited">
                                    <div className="row my-4">
                                        <div className="col-lg-6 isw-valign--middle mr-auto">
                                            <p className="isw-p mb-0">
                                                {`Vendors list (showing 1-${
                                                    users.length
                                                } of ${users.length}
                                                records)`}
                                            </p>
                                        </div>

                                        <div className="col-lg-3 text-right">
                                            <button
                                                className="isw-btn border w-100"
                                                data-toggle="modal"
                                                data-target="#exampleModalCenter"
                                            >
                                                <IconFilter />
                                                <span>Filter Table</span>
                                            </button>
                                        </div>

                                        <div className="col-md text-right">
                                            <button className="isw-btn isw-btn--raised bg-primary text-white w-100">
                                                <IconAddContact />
                                                <span
                                                    onClick={
                                                        this.routeToCreateVendor
                                                    }
                                                >
                                                    Add Vendor
                                                </span>
                                            </button>
                                        </div>
                                    </div>

                                    <div className="row mb-4 mt-3">
                                        <div className="col-12">
                                            <div className="isw-table">
                                                <ul className="isw-table--head">
                                                    <li className="isw-table--headLi">
                                                        <div className="row">
                                                            <div className="col-lg-4">
                                                                <span className="isw-table--headItem">
                                                                    Name
                                                                </span>
                                                            </div>
                                                            <div className="col-lg-2">
                                                                <span className="isw-table--headItem">
                                                                    Phone Number
                                                                </span>
                                                            </div>
                                                            <div className="col-lg-3">
                                                                <span className="isw-table--headItem">
                                                                    Role
                                                                </span>
                                                            </div>
                                                            <div className="col-lg-3 text-right">
                                                                <span className="isw-table--headItem">
                                                                    Location
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>

                                                <ul className="isw-table--body">
                                                    {users.map(user => (
                                                        <li
                                                            key={user.id.toString()}
                                                            className="isw-table--bodyLi"
                                                        >
                                                            <a
                                                                href="https://www.google.com"
                                                                className="row isw-table--bodyA"
                                                            >
                                                                <div className="col-lg-4 isw-valign--middle">
                                                                    <div className="isw-table--bodyItem isw-table--bodyItem-img">
                                                                        <div
                                                                            className="isw-image isw-valign--middle mr-3"
                                                                            style={{
                                                                                height: `${2.5}rem`,
                                                                                width: `${2.5}rem`,
                                                                                borderRadius: `${100}%`,
                                                                                overflow:
                                                                                    "hidden"
                                                                            }}
                                                                        >
                                                                            <img
                                                                                src={
                                                                                    Leye
                                                                                }
                                                                                alt="leye"
                                                                            />
                                                                        </div>
                                                                        <div className="isw-valign--middle">
                                                                            <h3 className="isw-subtitle">
                                                                                {
                                                                                    user.firstName
                                                                                }{" "}
                                                                                {
                                                                                    user.lastName
                                                                                }
                                                                            </h3>
                                                                            <p className="isw-p2 mb-0">
                                                                                leye@smartware.com
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-2 isw-valign--middle">
                                                                    <span className="isw-p2">
                                                                        {
                                                                            user.phoneNumber
                                                                        }
                                                                    </span>
                                                                </div>
                                                                <div className="col-lg-3 isw-valign--middle">
                                                                    <span className="isw-p2">
                                                                        Role
                                                                    </span>
                                                                </div>
                                                                <div className="col-lg-3 isw-valign--middle text-right">
                                                                    <span className="isw-p2">
                                                                        Location
                                                                    </span>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    ))}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12">
                                            <nav
                                                aria-label="pagination"
                                                className="float-right"
                                            >
                                                <ul className="pagination">
                                                    <li className="page-item disabled">
                                                        <span className="page-link">
                                                            Previous
                                                        </span>
                                                    </li>
                                                    <li className="page-item">
                                                        <a
                                                            className="page-link"
                                                            href="https://www.google.com"
                                                        >
                                                            1
                                                        </a>
                                                    </li>
                                                    <li
                                                        className="page-item active"
                                                        aria-current="page"
                                                    >
                                                        <span className="page-link">
                                                            2
                                                            <span className="sr-only">
                                                                (current)
                                                            </span>
                                                        </span>
                                                    </li>
                                                    <li className="page-item">
                                                        <a
                                                            className="page-link"
                                                            href="https://www.google.com"
                                                        >
                                                            3
                                                        </a>
                                                    </li>
                                                    <li className="page-item">
                                                        <a
                                                            className="page-link"
                                                            href="https://www.google.com"
                                                        >
                                                            Next
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    console.log({ state });

    const { get_users, get_users_data, payload } = state.cus_getUsers_reducer;
    return {
        get_users,
        get_users_data,
        payload
    };
};

export default connect(mapStateToProps)(Vendor);

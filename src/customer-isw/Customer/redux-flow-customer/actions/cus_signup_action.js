import { SIGNUP_USER } from "../../../../redux-flow-all/arsVariables";

const cus_signupUser = payload => ({
    type: SIGNUP_USER,
    payload
});

export default cus_signupUser;

import React, { Component } from "react";
import { Router } from "react-router";
import { history } from "./history";
import App from "../App";
import { SUPER_ADMIN } from "../redux-flow-all/arsVariables";
import ISWRoutes from "../customer-isw/ISW/routes";
// Route handler
class YourRoute extends Component {
    render() {
        return <div>/* the rest of your page */</div>;
    }
}

export default YourRoute;

// Authorization HOC
export const Authorization = allowedRoles => WrappedComponent =>
    class WithAuthorization extends Component {
        constructor(props) {
            super(props);

            // In this case the user is hardcoded, but it could be loaded from anywhere.
            // Redux, MobX, RxJS, Backbone...
            this.state = {
                user: {
                    name: "vcarl",
                    role: SUPER_ADMIN
                }
            };
        }
        render() {
            const { role } = this.state.user;
            if (allowedRoles.includes(role)) {
                return <WrappedComponent {...this.props} />;
            } else {
                return <h1>No page for you!</h1>;
            }
        }
    };

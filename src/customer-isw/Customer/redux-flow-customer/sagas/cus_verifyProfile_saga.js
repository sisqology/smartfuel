import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import { encryptAndStore } from "../../../../redux-flow-all/services/localStorageHelper";
import {
    VERIFY_SUCCESS,
    VERIFY_FAILURE,
    VERIFY_PROFILE,
    ENCRYPT_USER
} from "../../../../redux-flow-all/arsVariables";
// import { clear } from "../../../../redux-flow-all/services/localStorageHelper";
const { localStorage } = window;

function* verifyProfile(payload) {
    try {
        const returnedData = yield call(Services.verifyProfileService, payload);
        const { error, status, data } = returnedData;
        const { access_token, code, description, token_type } = data;
        console.log({ data, returnedData }, "from verify saga");
        if (error === false && status === 200 && code === 1) {
            encryptAndStore(
                ENCRYPT_USER,
                { access_token, expires_in: 3600, token_type },
                true
            );
            // localStorage.removeItem("email");
            // localStorage.removeItem("phone");
            return yield put({ type: VERIFY_SUCCESS, message: description });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({
                type: VERIFY_FAILURE,
                message: description
            });
        } else if (error === false && status === 200 && code === -2) {
            // clear();
            return yield put({
                type: VERIFY_FAILURE,
                message: description,
                code: code
            });
        }
    } catch (error) {
        console.log({ error });
        return yield put({ type: VERIFY_FAILURE, failure_data: error });

        // const { data, status } = error.response;
        // if (status === 400) {
        //     return yield put({
        //         type: VERIFY_FAILURE,
        //         message: data.error_description
        //     });
        // }
    }
}

export default function* cus_verifyProfileSaga() {
    yield takeLatest(VERIFY_PROFILE, verifyProfile);
}

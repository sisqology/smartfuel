const path = require("path");
// const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
// const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const webpackMerge = require("webpack-merge");
const modeConfig = env => require(`./build-utils/webpack.${env}`)(env);

module.exports = ({ mode, presets } = { mode: "production", presets: [] }) => {
    console.log(`mode: ${mode}`);

    return webpackMerge(
        {
            mode,
            entry: "./src/index.js",
            devServer: {
                historyApiFallback: true,
                contentBase: "./",
                open: true
            },
            module: {
                rules: [
                    {
                        test: /\.(jpe?g|png|ico)$/,
                        exclude: /node_modules/,
                        loader: ["url-loader", "file-loader"]
                    },
                    {
                        test: /\.(js|jsx|mjs)$/,
                        exclude: /node_modules/,
                        use: "babel-loader"
                    }
                ]
            },

            output: {
                publicPath: "/",
                path: path.resolve(__dirname, "build"),
                filename: "bundle.js"
            },
            plugins: [
                new HtmlWebpackPlugin({
                    template: "./public/index.html"
                })

                // new FaviconsWebpackPlugin({ logo: "./public/image.png" })
            ]
        },
        modeConfig(mode)
    );
};

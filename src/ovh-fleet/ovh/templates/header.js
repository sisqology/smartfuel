import * as React from 'react';
import {Router} from "react-router";
import {history} from "./../../../reuse/history";
import {Fragment} from "react";
import IswImage from "./../../../assets/images/isw-image.svg";
import Leye from "../../../assets/images/isw-imageBox.png";
import {NavLink} from "react-router-dom";

class OvhHeader extends React.Component {

    constructor(props){
        super(props);
        this.state = {};
        this.toggleButton = this.toggleButton.bind(this);
    }

    toggleButton(){
        this.setState({ buttonToggle: !this.state.buttonToggle })
    }

    logout(){
        history.push('/logout');
    }


    render() {
        const { logout } = this.props;
        const url = "/vendor/";

        return (
            <Fragment>
                    <header className="isw-nav--header">
                        <div className="isw-nav--pad">
                            <div className="isw-logo-menu">
                                <div className="isw-nav-logo">SmartFuel</div>
                                <div className="isw-nav-button">
                                    <button id="menu-close" className="menu-btn">
                                        <i className="material-icons">close</i>
                                    </button>

                                    <button id="menu-reveal" className="menu-btn">
                                        <i className="material-icons">menu</i>
                                    </button>
                                </div>
                            </div>

                            <div className="d-flex align-items-center">
                                <div className="isw-image" style={{maxWidth: `${2.1}rem`, marginRight:`${1}rem`}}>
                                    <img src={IswImage} style={{maxWidth: `${2.1}rem`}}/>
                                </div>
                                <div className="dropdown dropdown-toggle">
                                    <a role="button" id="dropUserNav" data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false" onClick={ this.toggleButton }>
                                        <span className="text-white font-weight-bold">Corporate Admin</span>
                                    </a>
                                    <div className="dropdown-menu" aria-labelledby="dropUserNav">
                                        <a className="dropdown-item" href="#">
                                            <p className="mb-0">Adeleye Samuel</p>
                                            <small>Admin</small>
                                        </a>
                                        <div className="dropdown-divider"></div>
                                        <NavLink to={`${url}profile-info`} className="dropdown-item">Profile</NavLink>
                                        <a className="dropdown-item" onClick={this.changePassword}>Change Password</a>
                                        <a className="dropdown-item text-danger" onClick={this.logout}>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
            </Fragment>
        );
    }
}

export default OvhHeader;





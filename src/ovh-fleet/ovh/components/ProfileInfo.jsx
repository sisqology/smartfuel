import * as React from "react";
import OvhHeader from "../templates/header";
import {ToastContainer} from "react-toastify";
import {Fragment} from "react";
import OvhMenu from "../templates/menu";
import {connect} from "react-redux";
import * as OvhConstants from "../redux-flow-ovh/ovh-constants";
import {showToast} from "../../shared/Toast";
import {updateProfile} from "../redux-flow-ovh/actions/profile.actions";


class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });

    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { firstName, lastName, phone, email } = this.state;
        const { dispatch } = this.props;
        if (firstName && lastName && email && phone) {
            let payload = {
                firstName: firstName,
                lastName: lastName,
                PhoneNo: phone,
                emailAddress: email,
            };
            dispatch(updateProfile(payload));

            // {showToast("Unable to fetch users", "error")}
        }
        else{
            this.setState({ submitted: false });
        }
    }

    checkStatus(resp){
        if(this.state.submitted) {
            if (resp.update_profile.update_profile_status === OvhConstants.UPDATE_PROFILE_FAILURE) {
                this.setState({ submitted: false });
                showToast(resp.update_profile.update_profile_data.error, "error");
            }
            if (resp.update_profile.update_profile_status === OvhConstants.UPDATE_PROFILE_SUCCESS) {
                showToast(resp.update_profile.update_profile_data.response.data.description, "success");
                this.setState({ submitted: false });
            }
        }
    }

    render() {
        const resp = this.props;
        console.error(resp);
        this.checkStatus(resp);
        let submitted = this.state.submitted;
        return (
            <Fragment>
                <div id="">
                    <OvhHeader/>
                </div>

                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000}/>
                    <OvhMenu/>
                    <div></div>
                    <div className="isw-content--wrapper">
                        <div>
                            <form className="content" id="content-body" onSubmit={this.handleSubmit}>
                                <div className="container-fluid container-limited">
                                    <div className="row">
                                        <div className="col-lg-9">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">
                                                        Basic Information
                                                    </h3>
                                                    <div className="isw-card-p">
                                                        Edit your Personal Information
                                                    </div>
                                                </div>
                                                <div
                                                    className="card-body p-3"
                                                    style={{
                                                        paddingTop: `${0} !important`
                                                    }}
                                                >
                                                    <div className="row">
                                                        <div className="col-lg-6">
                                                            <div className="form-field mb-4">
                                                                <div className="form-field__control">

                                                                    <input id="exampleField1" type="text"
                                                                           placeholder="First Name"
                                                                           className="form-field__input"
                                                                           name="firstName"
                                                                           onChange={this.handleChange}/>
                                                                    {submitted && !this.state.firstName &&
                                                                    <div className="text-danger">First Name is
                                                                        required</div>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-lg-6">
                                                            <div className="form-field mb-4">
                                                                <div className="form-field__control">

                                                                    <input id="exampleField2" type="text"
                                                                           placeholder="Last Name"
                                                                           className="form-field__input" name="lastName"
                                                                           onChange={this.handleChange}/>
                                                                    {submitted && !this.state.lastName &&
                                                                    <div className="text-danger">Last Name is
                                                                        required</div>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-lg-6">
                                                            <div className="form-field mb-4">
                                                                <div className="form-field__control">

                                                                    <input id="exampleField3" type="text"
                                                                           placeholder="Phone Number"
                                                                           className="form-field__input" name="phone"
                                                                           onChange={this.handleChange}/>
                                                                    {submitted && !this.state.phone &&
                                                                    <div className="text-danger">Phone is required</div>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-lg-6">
                                                            <div className="form-field mb-4">
                                                                <div className="form-field__control">

                                                                    <input id="exampleField4" type="text"
                                                                           placeholder="Email Address"
                                                                           className="form-field__input" name="email"
                                                                           onChange={this.handleChange}/>
                                                                    {submitted && !this.state.email &&
                                                                    <div className="text-danger">Email is required</div>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="isw-card mt-4">
                                                <div className="card-body p-3 row">
                                                    <div className="col-lg-4">
                                                        <button disabled={submitted}
                                                                className="isw-btn isw-btn--raised bg-primary text-white w-100">
                                                            <span>{ submitted ? "Processing..." : "Save" }</span>
                                                        </button>
                                                    </div>

                                                    <div className="col-lg-4">
                                                        <button
                                                            className="isw-btn isw-btn--outlined text-primary w-100">
                                                            <span>Cancel</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}


function mapStateToProps(state){
    return {
        update_profile: state.ven_update_profile,
    };
}

export default connect(mapStateToProps)(Profile);

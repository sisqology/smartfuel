import React, { Component, Fragment } from "react";
// import { connect } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
// import { Actions } from "./customer-isw/Customer/redux-flow-customer/actions/index";
import { Router } from "react-router";
import { history } from "../../reuse/history";

// components
import Layout from "../../reuse/layout";
import Signup from "../pages/Signup/Signup";
import Staff from "../Customer/components/Staff";
import SetupProfile from "../pages/Signup/SetupProfile";
import ProfileInfo from "../Customer/components/Profile_Info";
import CreateUser from "../Customer/components/Create_User";
import CreateLocation from "../Customer/components/Create_Location";
import EnterOTP from "../../reuse/enterOTP";
import ChangePW from "../pages/ChangePW/ChangePW";
import UserPermission from "./components/User_Permission";
import { CUSTOMER_ADMIN } from "../../redux-flow-all/arsVariables";
const url = "/customer/";
// Private router function
const PrivateRoute = ({
    component: Component,
    fakeAuth,
    fullScreen,
    ...rest
}) => {
    console.log(fakeAuth);
    return (
        <Route
            {...rest}
            render={props =>
                fakeAuth.isAuthenticated === true ? (
                    <Layout {...props} fullScreen={fullScreen}>
                        <Component />
                    </Layout>
                ) : (
                    <Redirect
                        push
                        to={{
                            pathname: "/signin",
                            state: {
                                from: props.location,
                                error: props.location !== "/" ? true : false
                            }
                        }}
                    />
                )
            }
        />
    );
};

class CustomerRoutes extends Component {
    // componentDidMount() {
    //     const { location } = this.props;
    //     console.log(this.props, history);

    //     if (location.state.role === CUSTOMER_ADMIN) {
    //         history.push(
    //             location.pathname
    //             // ? location.pathname : `${url}user-list`
    //         );
    //     }

    // }

    render() {
        const { fakeAuth } = this.props;
        return (
            <Fragment>
                {/* // <Switch> */}
                <Route
                    exact
                    path={`${url}signup`}
                    render={props => <Signup {...props} fakeAuth={fakeAuth} />}
                />
                <Route
                    exact
                    path={`${url}setup-profile`}
                    render={props => (
                        <SetupProfile fakeAuth={fakeAuth} {...props} />
                    )}
                />
                <Route
                    exact
                    path={`${url}verify-profile`}
                    render={props => (
                        <EnterOTP fakeAuth={fakeAuth} {...props} />
                    )}
                />
                <PrivateRoute
                    exact
                    path={`${url}settings/changePassword`}
                    fakeAuth={fakeAuth}
                    component={ChangePW}
                    fullScreen
                />
                <PrivateRoute
                    exact
                    fakeAuth={fakeAuth}
                    path={`${url}user-list`}
                    component={Staff}
                />
                <PrivateRoute
                    exact
                    fakeAuth={fakeAuth}
                    path={`${url}user-permission`}
                    component={UserPermission}
                />
                <PrivateRoute
                    exact
                    fakeAuth={fakeAuth}
                    path={`${url}profile-info`}
                    component={ProfileInfo}
                />
                <PrivateRoute
                    fakeAuth={fakeAuth}
                    exact
                    path={`${url}create-location`}
                    component={CreateLocation}
                />
                <PrivateRoute
                    exact
                    path={`${url}create-user`}
                    component={CreateUser}
                    fakeAuth={fakeAuth}
                />
                {/* </Switch> */}
            </Fragment>
        );
    }
}

export default CustomerRoutes;

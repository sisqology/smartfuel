import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { history } from "../../../reuse/history";
// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";
import "../../../scss/pending.scss";

import Static from "../../../reuse/Static";
import ProgressBar from "../../../reuse/progressBar";
import {
    validateNumber,
    areDigits,
    validateEmail
} from "../../../reuse/regexes";
import { CUSTOMER_ADMIN } from "../../../redux-flow-all/arsVariables";

// Assets

class Signup extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            phone: "",
            f_name: "",
            l_name: "",
            password: "",
            validEmail: null,
            validPhone: null,
            validFN: null,
            validLN: null,
            validPassword: null
        };
    }

    componentDidMount() {
        console.log(this.props.location.state);

        const { fakeAuth, history, location } = this.props;
        console.log({ fakeAuth, history });
        if (
            fakeAuth.isAuthenticated
            // &&
            // this.props.location.state.role === CUSTOMER_ADMIN
        ) {
            history.goBack("/customer/user-list");
        }

        if (location.state) {
            const { STATE } = location.state;
            console.log(this.state, { STATE });
            this.setState({
                ...this.state,
                ...STATE
            });
        }
    }

    onEmail_Change = ({ target }) => {
        const { value } = target;
        const isValidEmail = validateEmail(value);

        this.setState({
            email: value,
            validEmail: isValidEmail ? "true" : "false"
        });
    };
    onFirstname_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            f_name: value,
            validFN: value.length > 1 ? "true" : "false"
        });
    };
    onLastname_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            l_name: value,
            validLN: value.length > 1 ? "true" : "false"
        });
    };
    onPhone_Change = ({ target }) => {
        const { value } = target;

        const isValidPhone = validateNumber(value);

        if (areDigits(value))
            this.setState({
                phone: value,
                validPhone: isValidPhone ? "true" : "false"
            });
    };

    onPassword_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            password: value,
            validPassword: value.length > 6 ? "true" : "false"
        });
    };

    Continue = e => {
        e.preventDefault();
        const STATE = this.state;

        history.push({
            pathname: "/customer/setup-profile",
            state: {
                STATE
            }
        });
    };

    render() {
        const { location } = this.props;
        const {
            redirectToReferrer,
            email,
            password,
            f_name,
            l_name,
            phone,
            validEmail,
            validFN,
            validLN,
            validPassword,
            validPhone
        } = this.state;
        // const { from } = location.state || {
        //     from: { pathname: "/customer/signup" }
        // };

        // if (redirectToReferrer) {
        //     return <Redirect push to={from} />;
        // }

        const conditions =
            validEmail === "true" &&
            validFN === "true" &&
            validLN === "true" &&
            validPassword === "true" &&
            validPhone === "true";

        return (
            <div className="isw-login">
                <div className="container-fluid p-0">
                    <div className="row no-gutters">
                        <Static />

                        <div className="col-lg-6">
                            <div
                                className="isw-login--middle"
                                style={{
                                    overflowY: "auto",
                                    displaY: "flex",
                                    justifyContent: "center"
                                }}
                            >
                                <div className="isw-login--middle-form">
                                    <form
                                        className="row"
                                        style={{
                                            maxWidth: `${30}rem`,
                                            margin: `${0} auto`
                                        }}
                                    >
                                        <div className="col-12">
                                            <header>
                                                <h1 className="text-primary">
                                                    Let’s Get Started!
                                                </h1>
                                                <p>
                                                    Sign up by entering the
                                                    information below
                                                </p>
                                            </header>
                                        </div>

                                        <div className="col-12">
                                            <div className="pb-3">
                                                <ProgressBar />
                                                <h3>
                                                    Enter Personal information
                                                    details
                                                </h3>
                                            </div>
                                        </div>

                                        <div className="col-lg-6">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField1"
                                                        onChange={
                                                            this
                                                                .onFirstname_Change
                                                        }
                                                        onBlur={
                                                            this
                                                                .onFirstname_Change
                                                        }
                                                        onFocus={
                                                            this
                                                                .onFirstname_Change
                                                        }
                                                        value={f_name}
                                                        className={
                                                            validFN === "true"
                                                                ? `form-field__input k_input green`
                                                                : validFN ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Enter first name"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-6">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField2"
                                                        onChange={
                                                            this
                                                                .onLastname_Change
                                                        }
                                                        onBlur={
                                                            this
                                                                .onLastname_Change
                                                        }
                                                        onFocus={
                                                            this
                                                                .onLastname_Change
                                                        }
                                                        value={l_name}
                                                        className={
                                                            validLN === "true"
                                                                ? `form-field__input k_input green`
                                                                : validLN ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Enter last name"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField0"
                                                        onChange={
                                                            this.onEmail_Change
                                                        }
                                                        onBlur={
                                                            this.onEmail_Change
                                                        }
                                                        onFocus={
                                                            this.onEmail_Change
                                                        }
                                                        className={
                                                            validEmail ===
                                                            "true"
                                                                ? `form-field__input k_input green`
                                                                : validEmail ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Enter email"
                                                        value={email}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-6">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField22"
                                                        onChange={
                                                            this.onPhone_Change
                                                        }
                                                        onBlur={
                                                            this.onPhone_Change
                                                        }
                                                        onFocus={
                                                            this.onPhone_Change
                                                        }
                                                        value={phone}
                                                        className={
                                                            validPhone ===
                                                            "true"
                                                                ? `form-field__input k_input green`
                                                                : validPhone ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="text"
                                                        placeholder="Enter phone number"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-6">
                                            <div className="form-field mb-4">
                                                <div className="form-field__control">
                                                    <input
                                                        id="exampleField222"
                                                        onChange={
                                                            this
                                                                .onPassword_Change
                                                        }
                                                        onBlur={
                                                            this
                                                                .onPassword_Change
                                                        }
                                                        onFocus={
                                                            this
                                                                .onPassword_Change
                                                        }
                                                        value={password}
                                                        className={
                                                            validPassword ===
                                                            "true"
                                                                ? `form-field__input k_input green`
                                                                : validPassword ===
                                                                  "false"
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        type="password"
                                                        placeholder="Enter password"
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <button
                                                onClick={this.Continue}
                                                disabled={
                                                    conditions ? false : true
                                                }
                                                className={
                                                    conditions
                                                        ? `isw-btn isw-btn--raised bg-primary text-white w-100`
                                                        : `isw-btn  bg-primary text-white w-100 false`
                                                }
                                            >
                                                <span>Continue</span>
                                            </button>

                                            <div className="mt-4 text-center">
                                                <p>
                                                    Do you have an account
                                                    already?{" "}
                                                    <Link
                                                        to="/signin"
                                                        className="text-primary font-weight-bold"
                                                    >
                                                        Sign in
                                                    </Link>{" "}
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-2">
                            <div className="isw-column--right" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    console.log({ state });
    const { signup, data } = state.cus_signup_reducer;
    return {
        signup,
        data
    };
};

export default connect(mapStateToProps)(Signup);

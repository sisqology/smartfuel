import { CREATE_VENDOR } from "../../../../redux-flow-all/arsVariables";

const isw_createVendor = payload => ({
    type: CREATE_VENDOR,
    payload
});

export default isw_createVendor;

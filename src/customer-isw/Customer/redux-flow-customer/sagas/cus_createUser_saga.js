import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import {
    CREATE_CUS_FAILURE,
    CREATE_CUS_SUCCESS,
    CREATE_CUS_USER,
    ENCRYPT_USER
} from "../../../../redux-flow-all/arsVariables";
import { decryptAndRead } from "../../../../redux-flow-all/services/localStorageHelper";
function* createUser(payload) {
    const decryptedToken = decryptAndRead(ENCRYPT_USER).access_token;
    console.log({ decryptedToken });
    try {
        const returnedData = yield call(
            Services.cus_createUserService,
            payload,
            decryptedToken
        );
        const { error, data, status } = returnedData;
        const { code, description } = data;
        console.log({ data, returnedData });
        if (error === false && status === 200 && code === 1) {
            return yield put({
                type: CREATE_CUS_SUCCESS,
                message: description
            });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({
                type: CREATE_CUS_FAILURE,
                message: description
            });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({
                type: CREATE_CUS_FAILURE,
                message: description
            });
        }
    } catch (error) {
        console.log({ error });
        const { response } = error;
        if (response) {
            const { data, status } = response;
            if (status === 400) {
                return yield put({
                    type: CREATE_CUS_FAILURE,
                    message: data.error_description
                });
            }
        } else if (response === undefined) {
            return yield put({
                type: CREATE_CUS_FAILURE,
                message: "Failed to connect"
            });
        }
    }
}

export default function* cus_createUserSaga() {
    yield takeLatest(CREATE_CUS_USER, createUser);
}

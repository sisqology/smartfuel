import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import { SEND_OTP } from "../../../../redux-flow-all/arsVariables";

function* sendOTP({ payload }) {
    console.log("otp sgag", { payload });
    try {
        const returnedData = yield call(Services.OTPService, payload, SEND_OTP);
        const { error, status, data } = returnedData;
        const { code } = data;
        console.log({ data });
        if (error === false && status === 200 && code === 1) {
            return yield put({ type: "SENT OTP" /*message: description*/ });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({
                type: "OTP FAILED TO SEND"
                // message: description,
                // code
            });
        }
    } catch (error) {
        console.log({ error });
        return yield put({ type: "OTP FAILED TO SEND" });
        // const { data, status } = error.response;
        // if (status === 400) {
        //     return yield put({
        //         type: OTP_FAILURE,
        //         message: data.error_description
        //     });
        // }
    }
}

export default function* cus_sendOTPSaga() {
    yield takeLatest(SEND_OTP, sendOTP);
}

import * as OVHCONSTANTS from "../ovh-constants";

export function getUsersReducer(state={}, action) {
    switch (action.type) {
        case OVHCONSTANTS.FETCH_VENDOR_USERS_PENDING:
            return {
                fetch_vendor_users_status: OVHCONSTANTS.FETCH_VENDOR_USERS_PENDING,
                fetch_vendor_users_data: action
            };
        case OVHCONSTANTS.FETCH_VENDOR_USERS_SUCCESS:
            return {
                fetch_vendor_users_status: OVHCONSTANTS.FETCH_VENDOR_USERS_SUCCESS,
                fetch_vendor_users_data: action
            };
        case OVHCONSTANTS.FETCH_VENDOR_USERS_FAILURE:
            return {
                fetch_vendor_users_status: OVHCONSTANTS.FETCH_VENDOR_USERS_FAILURE,
                fetch_vendor_users_data: action
            };

        default:
            return {
                fetch_vendor_users_status: OVHCONSTANTS.FETCH_VENDOR_USERS_PENDING,
                fetch_vendor_users_data: action
            };
    }
}

export function createVendorReducer(state={}, action) {
    switch (action.type) {
        case OVHCONSTANTS.CREATE_VENDOR_USER_PENDING:
            return {
                create_vendor_status: OVHCONSTANTS.CREATE_VENDOR_USER_PENDING,
                create_vendor_data: action
            };
        case OVHCONSTANTS.CREATE_VENDOR_USER_SUCCESS:
            return {
                create_vendor_status: OVHCONSTANTS.CREATE_VENDOR_USER_SUCCESS,
                create_vendor_data: action
            };
        case OVHCONSTANTS.CREATE_VENDOR_USER_FAILURE:
            return {

                create_vendor_status: OVHCONSTANTS.CREATE_VENDOR_USER_FAILURE,
                create_vendor_data: action
            };

        default:
            return {
                state,
            };
    }
}

// import * as UserReducers from "./ovh/redux-flow-ovh/reducers/users.reducers";
import {combineReducers} from "redux";
import {
    createVendorReducer,
    getUsersReducer
} from "./ovh/redux-flow-ovh/reducers/users.reducers";
import {createFleetOwnerReducer, getFleetsReducer} from "./ovh/redux-flow-ovh/reducers/fleet.reducers";
import {updateProfileReducer} from "./ovh/redux-flow-ovh/reducers/profile.reducers";

// const rootReducer = combineReducers({
//     vendor_users: getUsersReducer,
//     create_vendor: createVendorReducer
// });
const rootReducer = {
    ven_vendor_users: getUsersReducer,
    ven_create_vendor: createVendorReducer,
    ven_vendor_fleets: getFleetsReducer,
    ven_create_fleet: createFleetOwnerReducer,
    ven_update_profile: updateProfileReducer,
};

export default rootReducer;
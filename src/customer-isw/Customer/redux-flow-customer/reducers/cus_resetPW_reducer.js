import {
    PW_SUCCESS,
    PW_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables.js";

const cus_resetPW_reducer = (state = {}, action) => {
    const { type, message, code } = action;
    switch (type) {
        case PW_SUCCESS:
            return {
                ...state,
                reset: SUCCESS,
                pwReset_data: message
            };
        case PW_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                reset: FAILURE,
                pwReset_data: message ? message : "Bad Network Connectivity",
                code: code ? code : null
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_resetPW_reducer;

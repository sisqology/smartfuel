import { all } from "redux-saga/effects";
import logoutSaga from "./logoutSaga";
import loginSaga from "./loginSaga";
import storageSaga from "./storageSaga";

// sagas for CUSTOMER admin
import cus_signupSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_signupSaga";
import cus_resetPasswordSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_pwReset_saga";
import cus_sendOTPSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_sendOTP_saga";
import cus_resendOTPSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_resendOTP_saga";
import cus_verifyProfileSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_verifyProfile_saga";
import cus_forgotPasswordSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_forgotPW_saga";
import cus_changePasswordSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_changePW_saga";

/**
 *
 * saga for CUSTOMER user and user permission below
 *
 */
// user
import cus_createUserSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_createUser_saga";
import cus_createLocationSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_createLocation_saga";
import cus_editBasicInfoSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_editBasicInfo_saga";
import cus_getLocationSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_getLocation_saga";
import cus_getLocationsSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_getLocations_saga";
import cus_getUsersSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_getUsers_saga";
import cus_getCustomersSaga from "../../customer-isw/Customer/redux-flow-customer/sagas/cus_getCustomers_saga";

// user permissions
import cus_permissions_saga from "../../customer-isw/Customer/redux-flow-customer/sagas/user_permissions/GET/cus_permissions_saga";
import cus_userPermissions_saga from "../../customer-isw/Customer/redux-flow-customer/sagas/user_permissions/GET/cus_userPermissions_saga";
import cus_roles_saga from "../../customer-isw/Customer/redux-flow-customer/sagas/user_permissions/GET/cus_roles_saga";
import cus_getRemoveUserPermissions_saga from "../../customer-isw/Customer/redux-flow-customer/sagas/user_permissions/GET/cus_getRemoveUserPermissions_saga";
import cus_batchUpdateUserPermissions_saga from "../../customer-isw/Customer/redux-flow-customer/sagas/user_permissions/POST/cus_batchUpdateUserPermissions_saga";
import cus_changeUserRole_saga from "../../customer-isw/Customer/redux-flow-customer/sagas/user_permissions/POST/cus_changeUserRole_saga";
import cus_removeUserPermission_saga from "../../customer-isw/Customer/redux-flow-customer/sagas/user_permissions/POST/cus_removeUserPermission_saga";
import cus_removeUserPermissions_saga from "../../customer-isw/Customer/redux-flow-customer/sagas/user_permissions/POST/cus_removeUserPermissions_saga";
import cus_updateUserPermissions_saga from "../../customer-isw/Customer/redux-flow-customer/sagas/user_permissions/POST/cus_updateUserPermissions_saga";

/**
 *
 * ISW sagas
 *
 */
import isw_createVendor_saga from "../../customer-isw/ISW/redux-flow-isw/sagas/isw_createVendor_saga";
// single entry point to start all Sagas at once
export default function* rootSaga() {
    yield all([
        cus_signupSaga(),
        cus_resetPasswordSaga(),
        cus_sendOTPSaga(),
        cus_resendOTPSaga(),
        cus_verifyProfileSaga(),
        storageSaga(),
        logoutSaga(),
        loginSaga(),
        cus_forgotPasswordSaga(),
        cus_changePasswordSaga(),
        cus_createUserSaga(),
        cus_editBasicInfoSaga(),
        cus_createLocationSaga(),
        cus_getLocationSaga(),
        cus_getLocationsSaga(),
        cus_getUsersSaga(),
        cus_getCustomersSaga(),
        // GET
        cus_permissions_saga(),
        cus_userPermissions_saga(),
        cus_roles_saga(),
        cus_getRemoveUserPermissions_saga(),

        // POST
        cus_batchUpdateUserPermissions_saga(),
        cus_changeUserRole_saga(),
        cus_removeUserPermission_saga(),
        cus_removeUserPermissions_saga(),
        cus_updateUserPermissions_saga(),

        // everything ISW below
        isw_createVendor_saga()
    ]);
}

import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../services/services";
import { encryptAndStore } from "../services/localStorageHelper";
import { history } from "../../reuse/history";

import {
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGIN_USER,
    ENCRYPT_USER,
    SEND_OTP,
    CUSTOMER_ADMIN
} from "../arsVariables";

import jwtDecode from "jwt-decode";
import { Actions } from "../actions";

function* loginUser(payload) {
    const { email } = payload.payload;
    try {
        const returnedData = yield call(Services.loginService, payload);
        const { error, status, data } = returnedData;
        const { access_token, code, description, ...rest } = data;
        console.log({ data, returnedData });
        const decoded = jwtDecode(access_token);
        const userRole = decoded.role;
        console.error({ decoded, userRole });
        if (error === false && status === 200) {
            console.log({ history, userRole });
            localStorage.setItem("email", email);
            localStorage.setItem("userID", decoded.sub);
            encryptAndStore(ENCRYPT_USER, { access_token, ...rest }, true);

            // if (userRole === CUSTOMER_ADMIN) {
            //     yield put(Actions.cus_getUsers());
            //     yield put(Actions.cus_roles());
            //     yield put(Actions.cus_permissions());
            //     yield put(Actions.cus_getLocations());
            // }
            return yield put({ type: LOGIN_SUCCESS, role: userRole });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({ type: LOGIN_FAILURE, message: description });
        }
    } catch (err) {
        console.log({ err });
        const { response, error } = err;
        if (response && error) {
            const { data, status } = response;
            if (status === 400 && data.error !== "access_denied") {
                return yield put({
                    type: LOGIN_FAILURE,
                    message: data.error_description
                });
            } else if (status === 400 && data.error === "access_denied") {
                yield put({
                    type: SEND_OTP,
                    payload: email
                });
                return yield put({
                    type: LOGIN_FAILURE,
                    message: data.error_description,
                    access: data.error
                });
            }
        } else if (response === undefined && error) {
            return yield put({
                type: LOGIN_FAILURE,
                message: "Failed to connect"
            });
        } else if (err) {
            return yield put({
                type: LOGIN_FAILURE,
                message: "Invalid Token"
            });
        }
    }
}

export default function* loginUserSaga() {
    yield takeLatest(LOGIN_USER, loginUser);
}

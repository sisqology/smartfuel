import { GET_RM_USER_PERMISSIONS } from "../../../../../../redux-flow-all/arsVariables";

const cus_getRemoveUserPermissions = payload => ({
    type: GET_RM_USER_PERMISSIONS,
    payload
});

export default cus_getRemoveUserPermissions;

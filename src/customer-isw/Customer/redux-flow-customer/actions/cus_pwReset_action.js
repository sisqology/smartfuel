import { PW_RESET } from "../../../../redux-flow-all/arsVariables";

const cus_resetPW = payload => ({
    type: PW_RESET,
    payload
});

export default cus_resetPW;

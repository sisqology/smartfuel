import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { ToastContainer, toast } from "react-toastify";

// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";
import Sidebar from "../../../reuse/sidebar";
import { Actions } from "../../../redux-flow-all/actions/index";
import Header from "../../../reuse/header";
import {
    areDigits,
    validateEmail,
    validateNumber
} from "../../../reuse/regexes";
import { history } from "../../../reuse/history";
// Assets
class CreateVendor1 extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            company_name: "",
            company_email: "",
            company_contact: "",
            company_phone: "",
            company_address: "",
            company_website: "",

            validEmail: null,
            validNumber: null,

            pending: false,
            errorShown: false,
            redirect: false
        };
    }

    componentDidMount() {
        const { dispatch, allowed, role, fakeAuth } = this.props;
        console.log(this.props, "KAMSIIIIIIIII", { history, allowed, role });

        if (role !== allowed && fakeAuth.isAuthenticated) {
            this.setState({ redirect: true });
            console.log("HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    static showToast = message => {
        if (toast.isActive(CreateVendor1.toastId)) {
            return null;
        }
        return (CreateVendor1.toastId = toast.error(message, {
            position: toast.POSITION.TOP_RIGHT,
            className: "error"
        }));
    };

    companyName_Handler = ({ target }) => {
        const { value } = target;
        this.setState({
            company_name: value
        });
    };

    companyEmail_Handler = ({ target }) => {
        const { value } = target;
        const isvalidMail = validateEmail(value);

        this.setState({
            company_email: value,
            validEmail: isvalidMail ? "true" : "false"
        });
    };

    companyAddress_Handler = ({ target }) => {
        const { value } = target;
        this.setState({
            company_address: value
        });
    };

    companyContact_Handler = ({ target }) => {
        const { value } = target;
        this.setState({
            company_contact: value
        });
    };

    companyWebsite_Handler = ({ target }) => {
        const { value } = target;
        this.setState({
            company_website: value
        });
    };

    companyPhone_Handler = ({ target }) => {
        const { value } = target;
        const isvalidNumber = validateNumber(value);
        console.log(areDigits(value));
        if (areDigits(value)) {
            this.setState({
                company_phone: value,
                validNumber: isvalidNumber ? "true" : "false"
            });
        }
    };

    onNext = e => {
        e.preventDefault();
        const STATE = this.state;
        console.log({ STATE });

        history.push({
            pathname: "/isw/create-vendor-admin",
            state: {
                STATE
            }
        });
    };
    render() {
        const {
            company_address,
            company_contact,
            company_email,
            company_name,
            company_phone,
            company_website,
            validEmail,
            validNumber,

            pending,
            redirect
        } = this.state;

        const conditions =
            validNumber === "true" &&
            (company_address &&
                company_contact &&
                company_name &&
                company_website) !== "" &&
            validEmail === "true";

        if (redirect) {
            return <Redirect to={{ pathname: "/#/403" }} />;
        }
        return (
            <Fragment>
                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000} />
                    <aside className="isw-sideBar">
                        <Sidebar type="ISW" />
                    </aside>
                    <div />
                    <div className="isw-content--wrapper">
                        <div>
                            <form className="content" id="content-body">
                                <div className="container-fluid container-limited">
                                    <div
                                        className="row"
                                        style={{
                                            justifyContent: "center"
                                        }}
                                    >
                                        <div className="col-lg-9">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">
                                                        New Vendor
                                                    </h3>
                                                    <div className="isw-card-p">
                                                        Enter company's details
                                                        in the form below
                                                    </div>
                                                </div>
                                                <div
                                                    className="card-body p-3"
                                                    style={{
                                                        paddingTop: `${0} !important`
                                                    }}
                                                >
                                                    <div className="row">
                                                        <div className="col-lg-8">
                                                            <div className="row">
                                                                <div className="col-12">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField1"
                                                                                onChange={
                                                                                    this
                                                                                        .companyName_Handler
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .companyName_Handler
                                                                                }
                                                                                onFocus={
                                                                                    this
                                                                                        .companyName_Handler
                                                                                }
                                                                                value={
                                                                                    company_name
                                                                                }
                                                                                className={`form-field__input k_input`}
                                                                                type="text"
                                                                                placeholder="Company Name"
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField21"
                                                                                onChange={
                                                                                    this
                                                                                        .companyContact_Handler
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .companyContact_Handler
                                                                                }
                                                                                onFocus={
                                                                                    this
                                                                                        .companyContact_Handler
                                                                                }
                                                                                value={
                                                                                    company_contact
                                                                                }
                                                                                className={`form-field__input k_input`}
                                                                                type="text"
                                                                                placeholder="Contact Name"
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-6">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField22"
                                                                                onChange={
                                                                                    this
                                                                                        .companyEmail_Handler
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .companyEmail_Handler
                                                                                }
                                                                                onFocus={
                                                                                    this
                                                                                        .companyEmail_Handler
                                                                                }
                                                                                value={
                                                                                    company_email
                                                                                }
                                                                                className={
                                                                                    validEmail ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validEmail ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                                type="text"
                                                                                placeholder="Email Address"
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField0"
                                                                                onChange={
                                                                                    this
                                                                                        .companyPhone_Handler
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .companyPhone_Handler
                                                                                }
                                                                                onFocus={
                                                                                    this
                                                                                        .companyPhone_Handler
                                                                                }
                                                                                value={
                                                                                    company_phone
                                                                                }
                                                                                className={
                                                                                    validNumber ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validNumber ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                                type="text"
                                                                                placeholder="Phone Number"
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField222"
                                                                                onChange={
                                                                                    this
                                                                                        .companyAddress_Handler
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .companyAddress_Handler
                                                                                }
                                                                                onFocus={
                                                                                    this
                                                                                        .companyAddress_Handler
                                                                                }
                                                                                value={
                                                                                    company_address
                                                                                }
                                                                                className={`form-field__input k_input`}
                                                                                type="text"
                                                                                placeholder="Office Address"
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="form-field mb-4">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField2222"
                                                                                onChange={
                                                                                    this
                                                                                        .companyWebsite_Handler
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .companyWebsite_Handler
                                                                                }
                                                                                onFocus={
                                                                                    this
                                                                                        .companyWebsite_Handler
                                                                                }
                                                                                value={
                                                                                    company_website
                                                                                }
                                                                                className={`form-field__input k_input`}
                                                                                type="text"
                                                                                placeholder="Company Website"
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-md">
                                                            Picture
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                className="col-3"
                                                style={{ padding: "0" }}
                                            >
                                                <div className="d-block mb-2 mt-3">
                                                    <button
                                                        onClick={this.onNext}
                                                        disabled={
                                                            conditions
                                                                ? false
                                                                : true
                                                        }
                                                        className={
                                                            conditions
                                                                ? `isw-btn isw-btn--raised bg-primary text-white w-100`
                                                                : `isw-btn  bg-primary text-white w-100 false`
                                                        }
                                                    >
                                                        <span>Next</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default connect(null)(CreateVendor1);

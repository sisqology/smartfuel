import * as React from "react";
import { history } from "./../../reuse/history";
import Layout from "./../../reuse/layout";
import { Redirect, Route, Switch } from "react-router-dom";
import { Router } from "react-router";
import {Component} from "react";
import Signup from "../../customer-isw/pages/Signup/Signup";
import ChangePW from "../../customer-isw/pages/ChangePW/ChangePW";
import Dashboard from "./components/Dashboard";
import {fakeAuth} from "../../App";
import CreateVendorUser from "./components/CreateUser";
import Users from "./components/Users";
import FleetOwners from "./components/FleetOwners";
import CreateFleetOwner from "./components/CreateFleetOwner";
import FleetOwnerDetail from "./components/FleetOwnerDetail";
import {Fragment} from "react";

/* /vendor/.. all vendor routes*/
const url = "/vendor/";

// const PrivateRoute = ({ component: Component, fakeAuth, ...rest }) => {
//     console.log(fakeAuth);
//     return (
//         <Route
//             {...rest}
//             render={props =>
//                 fakeAuth.isAuthenticated === true ? (
//                     <Layout {...props}>
//                         <Component />
//                     </Layout>
//                 ) : (
//                     <Redirect
//                         push
//                         to={{
//                             pathname: "/",
//                             state: {
//                                 from: props.location,
//                                 error: props.location !== "/" ? true : false
//                             }
//                         }}
//                     />
//                 )
//             }
//         />
//     );
// };

// Private router function
const PrivateRoute = ({
                          component: Component,
                          fakeAuth,
                          fullScreen,
                          ...rest
                      }) => {
    console.log(fakeAuth);
    return (
        <Route
            {...rest}
            render={props =>
                fakeAuth.isAuthenticated === true ? (
                    <Layout {...props} fullScreen={fullScreen}>
                        <Component />
                    </Layout>
                ) : (
                    <Redirect
                        push
                        to={{
                            pathname: "/signin",
                            state: {
                                from: props.location,
                                error: props.location !== "/" ? true : false
                            }
                        }}
                    />
                )
            }
        />
    );
};

// class vendorRoutes extends Component{
//     componentDidMount() {
//         const { location } = this.props;
//         console.log(this.props);
//         history.push(location.pathname ? location.pathname : `${url}dashboard`);
//     }
//
//     render() {
//         const { fakeAuth } = this.props;
//         console.log("Didi we come here");
//         return (
//             <Router history={history}>
//                 <Switch>
//                     <Route
//                         exact
//                         path={`${url}signup`}
//                         render={props => (
//                             <Signup {...props} fakeAuth={fakeAuth} />
//                         )}
//                     />
//                     <Route
//                         exact
//                         path="/"
//                         render={props =>
//                             fakeAuth.isAuthenticated === true ? (
//                                 <Redirect
//                                     push
//                                     to={{
//                                         pathname: `${url}dashboard`,
//                                         state: { from: props.location }
//                                     }}
//                                 />
//                             ) : (
//                                 <Redirect
//                                     push
//                                     to={{
//                                         pathname: `/signin`,
//                                         state: { from: props.location }
//                                     }}
//                                 />
//                             )
//                         }
//                     />
//                     <PrivateRoute
//                         exact
//                         path={`${url}dashboard`}
//                         fakeAuth={fakeAuth}
//                         component={Dashboard}
//                     />
//                 </Switch>
//             </Router>
//         );
//     }
// }

class vendorRoutes extends Component {
    // componentDidMount() {
    //     const { location, fakeAuth } = this.props;
    //     console.log(this.props, { fakeAuth, location, history });
    //     console.log(this);
    //     history.push(location.pathname);
    // }
    render() {
        const { fakeAuth } = this.props;
        return (
            <Fragment>
                <PrivateRoute
                    exact
                    path={`${url}dashboard`}
                    fakeAuth={fakeAuth}
                    component={Dashboard}
                />
                <PrivateRoute
                    exact
                    fakeAuth={fakeAuth}
                    path={`${url}users`}
                    component={Users}
                />
                <PrivateRoute
                    exact
                    fakeAuth={fakeAuth}
                    path={`${url}create-vendoruser`}
                    component={CreateVendorUser}
                />
                <PrivateRoute
                    exact
                    fakeAuth={fakeAuth}
                    path={`${url}fleet-owners`}
                    component={FleetOwners}
                />
                <PrivateRoute
                    exact
                    fakeAuth={fakeAuth}
                    path={`${url}create-fleet-owner`}
                    component={CreateFleetOwner}
                />
                <PrivateRoute
                    exact
                    fakeAuth={fakeAuth}
                    path={`${url}fleet-owner-detail`}
                    component={FleetOwnerDetail}
                />
            </Fragment>
        );
    }
}

//
// const vendorRoutes = props => (
//     <Router history={history}>
//         <Switch>
//             <Route path={`${url}dashboard`} fakeAuth={fakeAuth} component={Dashboard} />
//             <Route path={`${url}users`} fakeAuth={fakeAuth} component={Users} />
//             <Route path={`${url}create-user`} fakeAuth={fakeAuth} component={CreateUser} />
//             <Route path={`${url}fleet-owners`} fakeAuth={fakeAuth} component={FleetOwners} />
//             <Route path={`${url}create-fleet-owner`} fakeAuth={fakeAuth} component={CreateFleetOwner} />
//             <Route path={`${url}fleet-owner-detail`} fakeAuth={fakeAuth} component={FleetOwnerDetail} />
//         </Switch>
//     </Router>
// );


export default vendorRoutes;

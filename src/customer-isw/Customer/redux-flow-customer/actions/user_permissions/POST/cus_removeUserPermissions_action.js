import { RM_USER_PERMISSIONS } from "../../../../../../redux-flow-all/arsVariables";

const cus_removeUserPermissions = payload => ({
    type: RM_USER_PERMISSIONS,
    payload
});

export default cus_removeUserPermissions;

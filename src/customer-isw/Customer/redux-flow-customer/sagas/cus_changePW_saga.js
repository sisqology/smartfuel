import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import {
    CHANGE_FAILURE,
    CHANGE_SUCCESS,
    CHANGE_PW
} from "../../../../redux-flow-all/arsVariables";

function* changePassword(payload) {

    try {
        const returnedData = yield call(
            Services.changePasswordService,
            payload
        );
        const { error, data, status } = returnedData;
        const { code, description } = data;
        console.log({ data, returnedData });
        if (error === false && status === 200 && code === 1) {
            return yield put({ type: CHANGE_SUCCESS, message: description });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({ type: CHANGE_FAILURE, message: description });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({ type: CHANGE_FAILURE, message: description });
        }
    } catch (error) {
        console.log({ error });
        const { response } = error;
        if (response) {
            const { data, status } = response;
            if (status === 400) {
                return yield put({
                    type: CHANGE_FAILURE,
                    message: data.error_description
                });
            }
        } else if (response === undefined) {
            return yield put({
                type: CHANGE_FAILURE,
                message: "Failed to connect"
            });
        }
    }
}

export default function* cus_changePasswordSaga() {
    yield takeLatest(CHANGE_PW, changePassword);
}

import {
    CHANGE_USER_ROLE_FAILURE,
    CHANGE_USER_ROLE_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../../../redux-flow-all/arsVariables";

const cus_changeUserRole_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case CHANGE_USER_ROLE_SUCCESS:
            return {
                ...state,
                change_ur: SUCCESS,
                change_ur_data: message
            };
        case CHANGE_USER_ROLE_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                change_ur: FAILURE,
                change_ur_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_changeUserRole_reducer;

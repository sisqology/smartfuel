import {
    CREATE_CUS_SUCCESS,
    CREATE_CUS_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_createUser_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case CREATE_CUS_SUCCESS:
            return {
                ...state,
                createuser: SUCCESS,
                createuser_data: message
            };
        case CREATE_CUS_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                createuser: FAILURE,
                createuser_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_createUser_reducer;

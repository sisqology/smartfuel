import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Actions } from "../../../redux-flow-all/actions/index";
import { ToastContainer, toast } from "react-toastify";

// styles
import "react-toastify/dist/ReactToastify.min.css";
import "../../../scss/pending.scss";
import { SUCCESS, FAILURE } from "../../../redux-flow-all/arsVariables";
// import { history } from "../../../reuse/history";
import Static from "../../../reuse/Static";
// Assets

class ResetPW extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            token: "",
            password: "",
            confirmPassword: "",
            validPassword: null,
            pending: false,
            errorShown: false,
            firstPassInput: false,
            firstCPInput: false,
            upper: false,
            lower: false,
            num: false,
            minEight: false
        };
    }
    componentDidMount() {
        const { fakeAuth, history } = this.props;
        console.log({ fakeAuth, history });
        if (fakeAuth.isAuthenticated) {
            history.goBack("/customer/user-list");
        }
        const params = history.location.search;
        const paramsObj = params.split(/=|&/);
        const email = paramsObj[1];
        const token = paramsObj[3];
        this.setState({
            email,
            token
        });
    }

    static showToast = (message, type) => {
        if (toast.isActive(ResetPW.toastId)) {
            return null;
        }
        return type === "error"
            ? (ResetPW.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (ResetPW.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static redirectToLogin = props => {
        setTimeout(() => {
            props.history.push("/signin");
        }, 3000);
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown, email, token } = state;
        const { reset, pwReset_data } = props;
        console.log({ pwReset_data, email, token });

        // if reset is successful
        if (reset === SUCCESS && errorShown === false && pending === true) {
            ResetPW.showToast(pwReset_data, "success");
            props.dispatch(Actions.reset());
            ResetPW.redirectToLogin(props);
            return {
                pending: false,
                errorShown: true
            };
        }

        // if reset fails
        if (reset === FAILURE && pending === true && errorShown === false) {
            ResetPW.showToast(pwReset_data, "error");
            props.dispatch(Actions.reset());
            ResetPW.redirectToLogin(props);
            return {
                pending: false,
                errorShown: true
            };
        }
        return state;
    };

    onPassword_Change = ({ target }) => {
        const { value } = target;
        const reUpperCase = /[A-Z]/;
        const reLowerCase = /[a-z]/;
        const reNumber = /[0-9]/;
        const hasUpper = reUpperCase.test(value);
        const hasLower = reLowerCase.test(value);
        const hasNumber = reNumber.test(value);
        const { confirmPassword } = this.state;
        if (value.length > 0) {
            this.setState(prevState => {
                return {
                    ...prevState,
                    firstPassInput: true,
                    password: value,
                    upper: hasUpper ? true : false,
                    lower: hasLower ? true : false,
                    num: hasNumber ? true : false,
                    minEight: value.length >= 8 ? true : false,
                    // testing validity when there is a change
                    validPassword: this._validatePassword(confirmPassword, value)
                        ? "true"
                        : "false"
                };
            });
        } else if (value.length === 0) {
            this.setState({
                password: value,
                validPassword: "false",
                upper: false,
                lower: false,
                num: false,
                minEight: false
            });
        }
    };

    onConfirmPassword = ({ target }) => {
        const { value } = target;
        const { password } = this.state;
        if (value.length > 0) {
            this.setState(prevState => {
                return {
                    ...prevState,
                    firstCPInput: true,
                    confirmPassword: value,
                    // testing validity when there is a change
                    validPassword: this._validatePassword(password, value)
                        ? "true"
                        : "false"
                };
            });
        } else if (value.length === 0) {
            this.setState({
                confirmedPassword: value,
                validPassword: "false"
            });
        }
    };

    _validatePassword = (confirmer, password) => {
        // confirmer is existing value in state used to confirm the input is legit
        if (confirmer === "" && password.length >= 8) {
            return false;
        } else if (
            confirmer !== "" &&
            password === confirmer &&
            password.length >= 8
        ) {
            return true;
        }
    };

    onSubmit = e => {
        e.preventDefault();
        this.setState({ pending: true, errorShown: false });
        const { email, token, password } = this.state;

        this.props.dispatch(
            Actions.cus_resetPW({
                email,
                token,
                password,
                rtPassword: password
            })
        );
    };

    render() {
        const {
            email,
            firstPassInput,
            validPassword,
            firstCPInput,
            lower,
            upper,
            num,
            minEight,
            password,
            confirmPassword
        } = this.state;

        const condition = lower && upper && num && minEight;
        return (
            <div className="isw-login">
                <div className="container-fluid p-0">
                    <div className="row no-gutters bg-primary">
                        <Static />
                        <div
                            className="col-lg-6"
                            style={
                                {
                                    // margin: `${0}px auto`,
                                    // height: `${100}vh`
                                }
                            }
                        >
                            <ToastContainer autoClose={5000} />

                            <div
                                className="isw-login--middle"
                                style={{
                                    overflowY: "auto"
                                    // display: "flex",
                                    // justifyContent: "center",
                                    // alignItems: "center",
                                    // height: `${100}%`
                                }}
                            >
                                <div className="isw-login--middle-form">
                                    <form
                                        className="row"
                                        style={{
                                            maxWidth: `${30}rem`,
                                            margin: `${0} auto`
                                        }}
                                    >
                                        <div className="col-12">
                                            <header>
                                                <h1 className="text-primary">
                                                    Change Password
                                                </h1>
                                                <p>
                                                    Hi {email}, please enter
                                                    your new password to
                                                    continue
                                                </p>
                                            </header>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-3">
                                                <div className="form-field__control">
                                                    <input
                                                        className={
                                                            condition &&
                                                            firstPassInput
                                                                ? `form-field__input k_input green`
                                                                : !condition &&
                                                                  firstPassInput
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        id="exampleField0"
                                                        type="password"
                                                        placeholder="New Password"
                                                        onChange={
                                                            this
                                                                .onPassword_Change
                                                        }
                                                        onBlur={
                                                            this
                                                                .onPassword_Change
                                                        }
                                                        onFocus={
                                                            this
                                                                .onPassword_Change
                                                        }
                                                        value={password}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div
                                            className={
                                                !condition && firstPassInput
                                                    ? `validate-cont visible`
                                                    : `validate-cont`
                                            }
                                        >
                                            <span className="validate">
                                                <p
                                                    className={
                                                        lower ? `met` : "red"
                                                    }
                                                >
                                                    One lowercase letter
                                                </p>
                                                <p
                                                    className={
                                                        upper ? `met` : "red"
                                                    }
                                                >
                                                    One uppercase letter
                                                </p>
                                                <p
                                                    className={
                                                        num ? `met` : "red"
                                                    }
                                                >
                                                    One number
                                                </p>
                                                <p
                                                    className={
                                                        minEight ? `met` : "red"
                                                    }
                                                >
                                                    Minimum of 8 characters
                                                </p>
                                            </span>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-5">
                                                <div className="form-field__control">
                                                    <input
                                                        className={
                                                            validPassword ===
                                                                "true" &&
                                                            firstCPInput
                                                                ? `form-field__input k_input green`
                                                                : validPassword ===
                                                                      "false" &&
                                                                  firstCPInput
                                                                ? `form-field__input k_input red`
                                                                : `form-field__input k_input`
                                                        }
                                                        id="exampleField1"
                                                        type="password"
                                                        placeholder="Confirm Password"
                                                        onChange={
                                                            this
                                                                .onConfirmPassword
                                                        }
                                                        onBlur={
                                                            this
                                                                .onConfirmPassword
                                                        }
                                                        onFocus={
                                                            this
                                                                .onConfirmPassword
                                                        }
                                                        value={confirmPassword}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <button
                                                onClick={this.onSubmit}
                                                disabled={
                                                    condition &&
                                                    validPassword === "true"
                                                        ? false
                                                        : true
                                                }
                                                className={
                                                    condition &&
                                                    validPassword === "true"
                                                        ? `isw-btn isw-btn--raised bg-primary text-white w-100`
                                                        : `isw-btn  bg-primary text-white w-100 false`
                                                }
                                            >
                                                <span>Change Password</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-2">
                            <div className="isw-column--right" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { reset, pwReset_data, redirectTL } = state.cus_resetPW_reducer;
    return {
        reset,
        pwReset_data,
        redirectTL
    };
};

export default connect(mapStateToProps)(ResetPW);

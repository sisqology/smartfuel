// import { createStore, applyMiddleware } from "redux";
import logger, {createLogger} from "redux-logger";
import createSagaMiddleware from "redux-saga";
import thunkMiddleware from 'redux-thunk';

import indexReducer from "./redux-flow-all/reducers/index";
import rootSaga from "./redux-flow-all/sagas/sagas";
//
// const sagaMiddleware = createSagaMiddleware();
//
// export default function(data) {
//     const finalCreateStore = applyMiddleware(sagaMiddleware, thunkMiddleware, logger)(
//         createStore
//     );
//     const store = finalCreateStore(indexReducer, data);
//     sagaMiddleware.run(rootSaga);
//
//     return store;
// }



import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

const loggerMiddleware = createLogger();
const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
    indexReducer,
    applyMiddleware(
        sagaMiddleware,
        thunkMiddleware,
        loggerMiddleware
    )
);
sagaMiddleware.run(rootSaga);


// export default const store = createStore(
//     indexReducer,
//     applyMiddleware(sagaMiddleware, thunk)
// );
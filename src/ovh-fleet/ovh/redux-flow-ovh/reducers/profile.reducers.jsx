import * as OVHCONSTANTS from "../ovh-constants";


export function updateProfileReducer(state={}, action) {
    switch (action.type) {
        case OVHCONSTANTS.UPDATE_PROFILE_PENDING:
            return {
                update_profile_status: OVHCONSTANTS.UPDATE_PROFILE_PENDING,
                update_profile_data: action
            };
        case OVHCONSTANTS.UPDATE_PROFILE_SUCCESS:
            return {
                update_profile_status: OVHCONSTANTS.UPDATE_PROFILE_SUCCESS,
                update_profile_data: action
            };
        case OVHCONSTANTS.UPDATE_PROFILE_FAILURE:
            return {

                update_profile_status: OVHCONSTANTS.UPDATE_PROFILE_FAILURE,
                update_profile_data: action
            };

        default:
            return {
                state,
            };
    }
}
import * as React from 'react';
import {Router} from "react-router";
import {history} from "./../../../reuse/history";
import {Fragment} from "react";
import {NavLink} from "react-router-dom";
import {IconFolder} from "../../../reuse/svgs";

class OvhMenu extends React.Component {

    componentDidMount() {
        
    }

    render() {
        const url = "/fleet-owner/";

        return (
            <Fragment>
                <aside className="isw-sideBar">
                    <div className="isw-sideBar-innerScroll" style={{ overflowY: "scroll" }}>
                        <div className="isw-sideBar--wrapper">
                            <div className="isw-sideBar--linkBox">
                                <ul>
                                    <span className="isw-sideBar-ulSpan" />
                                    <li className="isw-sideBar-li" role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}dashboard`}
                                            className="isw-sideBar-a"
                                            tabIndex="1"
                                        >
                                            <IconFolder />
                                            <span className="isw-sideBar-spanText">
                                        Dashboard
                                    </span>
                                        </NavLink>
                                    </li>
                                </ul>

                                <ul>
                                    <span className="isw-sideBar-ulSpan" />
                                    <li className="isw-sideBar-li " role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}users`}
                                            className="isw-sideBar-a"
                                            tabIndex="2"
                                        >
                                            <IconFolder />
                                            <span className="isw-sideBar-spanText">
                                        Truck Management
                                    </span>
                                        </NavLink>
                                    </li>

                                    <li className="isw-sideBar-li" role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}fleet-owners`}
                                            className="isw-sideBar-a"
                                            tabIndex="3"
                                        >
                                            <IconFolder />
                                            <span className="isw-sideBar-spanText">
                                        Driver Management
                                    </span>
                                        </NavLink>
                                    </li>

                                    <li className="isw-sideBar-li" role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}profile-info`}
                                            className="isw-sideBar-a"
                                            tabIndex="5"
                                        >
                                            <IconFolder />
                                            <span className="isw-sideBar-spanText">
                                        Profile Info
                                    </span>
                                        </NavLink>
                                    </li>

                                    <li className="isw-sideBar-li" role="menuitem">
                                        <NavLink
                                            activeClassName="isw-sideBar-liActive"
                                            to={`${url}user-permission`}
                                            className="isw-sideBar-a"
                                            tabIndex="5"
                                        >
                                            <IconFolder />
                                            <span className="isw-sideBar-spanText">
                                        User Permission
                                    </span>
                                        </NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </aside>
            </Fragment>
        );
    }
}

export default OvhMenu;





import { CHANGE_USER_ROLE } from "../../../../../../redux-flow-all/arsVariables";

const cus_changeUserRole = payload => ({
    type: CHANGE_USER_ROLE,
    payload
});

export default cus_changeUserRole;

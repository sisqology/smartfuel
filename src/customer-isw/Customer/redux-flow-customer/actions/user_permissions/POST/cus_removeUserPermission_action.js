import { RM_USER_PERMISSION } from "../../../../../../redux-flow-all/arsVariables";



const cus_removeUserPermission = payload => ({
    type: RM_USER_PERMISSION,
    payload
});

export default cus_removeUserPermission;

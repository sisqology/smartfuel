import { GET_USERS } from "../../../../redux-flow-all/arsVariables";

const cus_getUsers = payload => ({
    type: GET_USERS,
    payload
});

export default cus_getUsers;

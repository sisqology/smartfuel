import React from "react";

const ProgressBar = () => {
    return (
        <div className="pb-3">
            <div
                className="progress"
                style={{
                    height: `${0.3}rem`,
                    marginBottom: `${1}rem`
                }}
            >
                <div
                    className="progress-bar"
                    role="progressbar"
                    style={{
                        width: `${100}%`
                    }}
                    aria-valuenow="100"
                    aria-valuemin="0"
                    aria-valuemax="100"
                />
            </div>
        </div>
    );
};

export default ProgressBar;

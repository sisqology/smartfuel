import {
    VERIFY_SUCCESS,
    VERIFY_FAILURE,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_verifyProfile_reducer = (state = {}, action) => {
    const { type, data, message, code } = action;
    console.log({ code, message, data });
    switch (type) {
        case VERIFY_SUCCESS:
            return { ...state, verify: SUCCESS, verify_data: message };
        case VERIFY_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                verify: FAILURE,
                verify_data: message ? message : "Bad Network Connectivity",
                verify_error_code: code
            };
        case RESET:
            return {};

        default:
            return state;
    }
};

export default cus_verifyProfile_reducer;

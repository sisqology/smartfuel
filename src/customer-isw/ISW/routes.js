import React, { Component, Fragment } from "react";
// import { connect } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
// import { Actions } from "./customer-isw/Customer/redux-flow-customer/actions/index";
import { Router } from "react-router";
import { history } from "../../reuse/history";

// components
import Layout from "../../reuse/layout";
import CreateVendor from "./components/Create_Vendor1";
import ChangePW from "../pages/ChangePW/ChangePW";

const url = "/isw/";

// Private router function
const PrivateRoute = ({
    component: Component,
    fakeAuth,
    fullScreen,
    ...rest
}) => {
    console.log(fakeAuth);
    return (
        <Route
            {...rest}
            render={props =>
                fakeAuth.isAuthenticated === true ? (
                    <Layout {...props} fullScreen={fullScreen}>
                        <Component />
                    </Layout>
                ) : (
                    // : rest.path !== "/" ? (
                    //     <Redirect
                    //         to={{
                    //             pathname: "*"
                    //         }}
                    //     />
                    // ) :

                    <Redirect
                        push
                        to={{
                            pathname: "/signin",
                            state: {
                                from: props.location,
                                error: props.location !== "/" ? true : false
                            }
                        }}
                    />
                )
            }
        />
    );
};

class ISWRoutes extends Component {
    // componentDidMount() {
    //     const { location, fakeAuth } = this.props;
    //     console.log(this.props, { fakeAuth, location, history });
    //     console.log(this);
    //     history.push(location.pathname);
    // }
    render() {
        const { fakeAuth } = this.props;
        return (
            <Fragment>
                <PrivateRoute
                    exact
                    path={`${url}settings/changePassword`}
                    fakeAuth={fakeAuth}
                    component={ChangePW}
                />
                <PrivateRoute
                    exact
                    fakeAuth={fakeAuth}
                    path="/isw/vendor"
                    component={CreateVendor}
                />
            </Fragment>
        );
    }
}

export default ISWRoutes;

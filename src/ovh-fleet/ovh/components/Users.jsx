import * as React from "react";
import {Fragment} from "react";
import OvhHeader from "../templates/header";
import {ToastContainer} from "react-toastify";
import OvhMenu from "../templates/menu";
import { connect } from "react-redux";
import {NavLink} from "react-router-dom";
import Leye from "../../../assets/images/isw-imageBox.png";
import {IconAddContact, IconFilter} from "../../../reuse/svgs";
import {getUsers} from "../redux-flow-ovh/actions/users.actions";
import {history} from "../../../reuse/history";
import {FAILURE, SUCCESS} from "../../../redux-flow-all/arsVariables";
import {Actions} from "../../../redux-flow-all/actions";
import * as OvhConstants from "../redux-flow-ovh/ovh-constants";
import loading from "../../../assets/images/loading.gif";
import {showToast} from "../../shared/Toast";
import Pagination from "react-js-pagination";

const url = "/vendor/";

class Users extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            buttonToggle: false,
            itemsPerPage: 10,
            activePage: 1
        };
        this.toggleButton = this.toggleButton.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.goToAddUser = this.goToAddUser.bind(this);
    }

    toggleButton(){
        this.setState({ buttonToggle: !this.state.buttonToggle })
    }

    componentDidMount() {
        // this.fetchUsers();
        this.props.dispatch(getUsers(this.state.activePage, this.state.itemsPerPage));
    }

    goToAddUser(){
        history.push(`create-vendoruser`);
    }

    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage: pageNumber});
        this.props.dispatch(getUsers(pageNumber, this.state.itemsPerPage));
    }


    renderElements(object){
        let status = object.users.fetch_vendor_users_data.type;
        let resp = object.users.fetch_vendor_users_data;
        const show = (this.state.buttonToggle) ? "show" : "" ;
        const totalCount = 10;
        const currentPage = 1;

        // console.log(users.response.data.payload);
        switch(status){
            case OvhConstants.FETCH_VENDOR_USERS_PENDING:
                return(
                    <div className="row mb-4 mt-3">
                        <div className="col-12">
                            <div className="isw-table text-center">
                                <p>&nbsp;</p>
                                <img src={loading} />
                            </div>
                        </div>
                    </div>
                );
            case OvhConstants.FETCH_VENDOR_USERS_SUCCESS:
                let users = resp.response.data.payload;
                let totalCount = resp.response.data.totalCount;
                console.error(totalCount);
                let activePage = 1;
                // console.error(users);
                return (
                    <Fragment>
                    <div className="row mb-4 mt-3">
                        <div className="col-12">
                            <div className="isw-table">
                                <ul className="isw-table--head">
                                    <li className="isw-table--headLi">
                                        <div className="row">
                                            <div className="col-lg-4">
                                                <span className="isw-table--headItem">Name</span>
                                            </div>
                                            <div className="col-lg-3">
                                                                <span
                                                                    className="isw-table--headItem">Phone Number</span>
                                            </div>
                                            <div className="col-lg-3">
                                                <span className="isw-table--headItem">Role</span>
                                            </div>


                                            <div className="col-lg-2 text-right">
                                                <span className="isw-table--headItem">Actions</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>


                                <ul className="isw-table--body">
                                    {users.map(user => (
                                    <li className="isw-table--bodyLi" key={user.phoneNo}>
                                        <a href="#" className="row isw-table--bodyA">
                                            <div className="col-lg-4 isw-valign--middle">
                                                <div
                                                    className="isw-table--bodyItem isw-table--bodyItem-img">
                                                    <div className="isw-image isw-valign--middle mr-3"
                                                         style={{
                                                             height: `${2.5}rem`,
                                                             width: `${2.5}rem`,
                                                             borderRadius: `${100}%`,
                                                             overflow:
                                                                 "hidden"
                                                         }}>
                                                        <img
                                                            src={
                                                                Leye
                                                            }
                                                            alt="leye"
                                                        />

                                                    </div>
                                                    <div className="isw-valign--middle">
                                                        <h3 className="isw-subtitle">{user.fullName}</h3>
                                                        <p className="isw-p2 mb-0">{user.username || user.emailAddress}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-lg-3 isw-valign--middle">
                                                <span className="isw-p2">{user.phoneNo}</span>
                                            </div>
                                            <div className="col-lg-3 isw-valign--middle">
                                                <span className="isw-p2">{user.role}</span>
                                            </div>

                                        </a>
                                        <div className="col-lg-2 isw-valign--middle text-right pull-right">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false" onClick={ this.toggleButton }>
                                                    Action
                                                </button>
                                                <div className={"dropdown-menu " + show}>
                                                    <button className="dropdown-item" href="#">Activate/Disable</button>
                                                    <button className="dropdown-item" href="#">Edit User Permissions</button>

                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <nav aria-label="pagination" className="float-right">
                                <Pagination
                                    hideDisabled
                                    hideNavigation
                                    innerClass="pagination"
                                    itemClass="page-item"
                                    disabledClass="disabled"
                                    linkClass="page-link"
                                    activeClass="page-item active"
                                    activePage={activePage}
                                    itemsCountPerPage={this.state.itemsPerPage}
                                    totalItemsCount={totalCount}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange}
                                />
                            </nav>
                        </div>
                    </div>
                    </Fragment>
                );
            case OvhConstants.FETCH_VENDOR_USERS_FAILURE:
                return(

                    <div className="row mb-4 mt-3">
                        {showToast("Unable to fetch users", "error")}
                        <div className="col-12">
                            <div className="isw-table">
                                <ul className="isw-table--head">
                                    <li className="isw-table--headLi">
                                        <div className="row">
                                            <div className="col-lg-4">
                                                <span className="isw-table--headItem">Name</span>
                                            </div>
                                            <div className="col-lg-2">
                                                                <span
                                                                    className="isw-table--headItem">Phone Number</span>
                                            </div>
                                            <div className="col-lg-2">
                                                <span className="isw-table--headItem">Role</span>
                                            </div>

                                            <div className="col-lg-2 text-right">
                                                <span className="isw-table--headItem">Status</span>
                                            </div>
                                            <div className="col-lg-2 text-right">
                                                <span className="isw-table--headItem">Actions</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                );
        }

    }

    render() {
        let objects  = this.props;
        return (
            <Fragment>
                <div id="">
                    <OvhHeader/>
                </div>

                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000}/>
                    <OvhMenu/>
                    <div></div>
                    <div className="isw-content--wrapper">
                        <div>
                            <div className="content" id="content-body">
                                <div className="container-fluid container-limited">

                                    <div className="row my-4">
                                        <div className="col-lg-6 isw-valign--middle mr-auto">
                                            <p className="isw-p mb-0">Users list (showing 1-8 of 26 records)</p>
                                        </div>

                                        <div className="col-lg-3 text-right">
                                            <button className="isw-btn border w-100" data-toggle="modal"
                                                    data-target="#exampleModalCenter">
                                                <IconFilter /><span>Filter Table</span>
                                            </button>
                                        </div>

                                        <div className="col-md text-right">
                                            <button className="isw-btn isw-btn--raised bg-primary text-white w-100" onClick={this.goToAddUser}>
                                                <IconAddContact /><span>Add User</span>
                                            </button>
                                        </div>
                                    </div>
                                    {this.renderElements(objects)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state){
    return {
        users: state.ven_vendor_users,
    };
}

export default connect(mapStateToProps)(Users);

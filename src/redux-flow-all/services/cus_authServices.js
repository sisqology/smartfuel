const axios = require("axios");
const qs = require("qs");
const baseurl = "https://feulapp.azurewebsites.net/api/";

const header = token => {
    if (token) {
        return {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };
    } else {
        return {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        };
    }
};

// Signup User
export const signupService = payload => {
    const { customerAdmin, customerInfo } = payload;
    console.log({ payload });
    const objectToSend = { customerAdmin, customerInfo };
    console.log({ objectToSend }, "for signing up");
    return new Promise((resolve, reject) => {
        axios
            .post(`${baseurl}userprofiling/createcustomer`, objectToSend)
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// Login User
export const loginService = ({ payload }) => {
    const { email, password } = payload;
    console.log({ email, password });
    const objectToSend = qs.stringify({
        username: email,
        password,
        grant_type: "password",
        client_id: "smartfuelwebapp",
        client_secret: "c12edcdb-35c4-4a0d-9fe3-caf4174fe57c"
    });
    console.log({ objectToSend });
    return new Promise((resolve, reject) => {
        axios
            .post(
                `https://feulapp.azurewebsites.net/api/auth/token`,
                objectToSend,
                header()
            )
            .then(res => {
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({ response: response, error: true });
                } else {
                    console.log({ response });
                    return reject({ error: true, response });
                }
            });
    });
};

// send OTP
export const OTPService = (payload, type) => {
    console.log({ payload, type }, "for OTP");

    return new Promise((resolve, reject) => {
        axios
            .post(`${baseurl}Auth/SendOTP`, payload)
            .then(res => {
                console.log({ res });
                return resolve({ ...res, error: false, type });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true,
                        type
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response, type });
                }
            });
    });
};

// verify profile
export const verifyProfileService = ({ payload }) => {
    console.log({ payload }, "for verification");

    return new Promise((resolve, reject) => {
        axios
            .post(`${baseurl}Auth/VerifyProfile`, payload)
            .then(res => {
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// refresh Token
export const refreshTokenService = ({ data }) => {
    console.log(data);
    const { token, user } = data;
    return new Promise((resolve, reject) => {
        axios
            .put(`${baseurl}/users/token`, { token, _id: user._id })
            .then(res => {
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// reset password
export const resetPasswordService = ({ payload }) => {
    console.log({ payload });

    return new Promise((resolve, reject) => {
        axios
            .post(`${baseurl}Auth/ResetPasssword`, payload)
            .then(res => {
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// change password
export const changePasswordService = ({ payload }) => {
    console.log({ payload });
    const { token, currentPW, password, rtPassword } = payload;
    const objectToSend = {
        currentPassword: currentPW,
        newPassword: password,
        confirmPassword: rtPassword
    };
    console.log(header(token));
    return new Promise((resolve, reject) => {
        axios
            .post(`${baseurl}Auth/ChangePassword`, objectToSend, header(token))
            .then(res => {
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

// forgot password
export const forgotPasswordService = ({ payload }) => {
    console.log({ payload });
    return new Promise((resolve, reject) => {
        axios
            .get(`${baseurl}Auth/ForgotPassword?${qs.stringify(payload)}`)
            .then(res => {
                return resolve({ ...res, error: false });
            })
            .catch(({ response }) => {
                if (response) {
                    console.log({ response }, "if");
                    return reject({
                        response: response,
                        error: true
                    });
                } else {
                    console.log({ response }, "else");
                    return reject({ error: true, response });
                }
            });
    });
};

import {
    CREATE_VENDOR_FAILURE,
    CREATE_VENDOR_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const isw_createVendor_reducer = (state = {}, action) => {
    const { type, message } = action;
    switch (type) {
        case CREATE_VENDOR_SUCCESS:
            return {
                ...state,
                create_vendor: SUCCESS,
                create_vendor_data: message
            };
        case CREATE_VENDOR_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                create_vendor: FAILURE,
                create_vendor_data: message
                    ? message
                    : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default isw_createVendor_reducer;

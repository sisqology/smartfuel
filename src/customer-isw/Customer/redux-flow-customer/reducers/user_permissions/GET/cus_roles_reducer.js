import {
    GET_ROLES_FAILURE,
    GET_ROLES_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../../../redux-flow-all/arsVariables";

const cus_roles_reducer = (state = {}, action) => {
    const { type, message, payloadReturned } = action;
    switch (type) {
        case GET_ROLES_SUCCESS:
            return {
                ...state,
                get_roles: SUCCESS,
                get_roles_data: message,
                payload: payloadReturned
            };
        case GET_ROLES_FAILURE:
            // message here is the error message from the server
            return {
                ...state,
                get_roles: FAILURE,
                get_roles_data: message ? message : "Bad Network Connectivity"
            };

        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_roles_reducer;

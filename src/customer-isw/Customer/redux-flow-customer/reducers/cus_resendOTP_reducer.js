import {
    OTP_FAILURE,
    OTP_SUCCESS,
    SUCCESS,
    FAILURE,
    RESET
} from "../../../../redux-flow-all/arsVariables";

const cus_resendOTP_reducer = (state = {}, action) => {
    const { type, message, code } = action;
    switch (type) {
        case OTP_SUCCESS:
            return { OTP: SUCCESS, resend_data: message };
        case OTP_FAILURE:
            // message here is the error message from the server
            return {
                OTP: FAILURE,
                resend_data: message
                    ? message
                    : "Bad Network Connectivity",
                resend_error_code: code ? code : ""
            };
        case RESET:
            return {};
        default:
            return state;
    }
};

export default cus_resendOTP_reducer;

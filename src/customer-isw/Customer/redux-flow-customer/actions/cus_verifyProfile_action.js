import { VERIFY_PROFILE } from "../../../../redux-flow-all/arsVariables";

const cus_verifyProfile = payload => ({
    type: VERIFY_PROFILE,
    payload
});

export default cus_verifyProfile;

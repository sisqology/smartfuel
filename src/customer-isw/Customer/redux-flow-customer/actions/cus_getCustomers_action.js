import { GET_CUSTOMERS } from "../../../../redux-flow-all/arsVariables";

const cus_getCustomers = payload => ({
    type: GET_CUSTOMERS,
    payload
});

export default cus_getCustomers;

import React, { Component } from "react";

// Styles
import "../scss/trumps.scss";
import "../scss/reset.scss";
import "../scss/loadingpage.scss";
import { history } from "./history";
import { fakeAuth } from "../App";


class NotFound extends Component {
    componentDidMount() {
        console.log(this.props, { fakeAuth });
        // if (fakeAuth.isAuthenticated === false) {
        //     history.goBack("/signin");
        // }
    }

    goBack = () => {
        console.log("404");
        history.goBack("/signin");
    };
    render() {
        return (
            <div className="container-404">
                <section>
                    <h1>ERROR 404</h1>
                    <h1>
                        We're under attack!
                        <span role="img" aria-label="runner">
                            😱
                        </span>
                        , Aliens
                        <span role="img" aria-label="runner">
                            👽
                        </span>{" "}
                        seem to be sucking up everything on this page
                    </h1>
                    <button onClick={this.goBack}>Go back</button>
                </section>
            </div>
        );
    }
}

export default NotFound;

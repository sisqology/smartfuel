import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { ToastContainer, toast } from "react-toastify";

// styles
import "../../../assets/stylesheet/app.scss";
import "react-toastify/dist/ReactToastify.min.css";
import Sidebar from "../../../reuse/sidebar";
import { Actions } from "../../../redux-flow-all/actions/index";

import {
    areDigits,
    validateNumber,
    validateEmail
} from "../../../reuse/regexes";
import { history } from "../../../reuse/history";
import { SUCCESS, FAILURE } from "../../../redux-flow-all/arsVariables";
// Assets

class ProfileInfo extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            phone1: "",
            phone2: "",
            f_name: "",
            l_name: "",
            username: "",
            validEmail: null,
            validPhone1: null,
            validPhone2: null,
            State: "",
            LGA: "",
            RC: "",
            corp_name: "",
            address1: "",
            address2: "",
            redirect: false,
            pending: false,
            errorShown: false
        };
    }

    componentDidMount() {
        const { dispatch, allowed, role, fakeAuth } = this.props;
        console.log(this.props, "KAMSIIIIIIIII", { history, allowed, role });

        this.setState({ pending: true, errorShown: false });

        if (role === allowed && fakeAuth.isAuthenticated) {
            return;
            // this.setState({ pending: true, errorShown: false });
            // dispatch(Actions.cus_getUsers());
        } else if (role !== allowed && fakeAuth.isAuthenticated) {
            this.setState({ redirect: true });
            console.log("HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    static showToast = (message, type) => {
        if (toast.isActive(ProfileInfo.toastId)) {
            return toast.update(ProfileInfo.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (ProfileInfo.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (ProfileInfo.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const { fakeAuth, edit_basic_info, edit_basic_info_data } = props;

        console.log({ props, state });

        // if edit is successful
        if (
            edit_basic_info === SUCCESS &&
            errorShown === false &&
            pending === true
        ) {
            ProfileInfo.showToast(edit_basic_info_data, "success");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }

        // if edit fails
        if (
            edit_basic_info === FAILURE &&
            errorShown === false &&
            pending === true
        ) {
            ProfileInfo.showToast(edit_basic_info_data, "error");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }
        // if access denied
        if (
            edit_basic_info === FAILURE &&
            errorShown === false &&
            pending === true
        ) {
            ProfileInfo.showToast(edit_basic_info_data, "error");
            props.dispatch(Actions.reset());

            return {
                redirectToReferrer: false,
                pending: false,
                errorShown: true
            };
        }

        return null;
    };

    // handlers for basic information

    onEmail_Change = ({ target }) => {
        const { value } = target;

        const isValidEmail = validateEmail(value);

        this.setState({
            email: value,
            validEmail: isValidEmail ? "true" : "false"
        });
    };

    onFirstname_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            f_name: value,
            validFN: value.length > 1 ? "true" : "false"
        });
    };
    onLastname_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            l_name: value,
            validLN: value.length > 1 ? "true" : "false"
        });
    };
    onPhone1_Change = ({ target }) => {
        const { value } = target;
        const isValidPhone = validateNumber(value);
        console.log(areDigits(value));
        if (areDigits(value)) {
            this.setState({
                phone1: value,
                validPhone1: isValidPhone ? "true" : "false"
            });
        }
    };

    submitBasicInfo = e => {
        e.preventDefault();

        this.setState({ pending: true, errorShown: false });
        const { l_name, f_name, phone1, email } = this.state;
        this.props.dispatch(
            Actions.cus_editBasicInfo({
                firstName: f_name,
                lastName: l_name,
                emailAddress: email,
                phoneNo: phone1
            })
        );
    };

    // handlers for business information

    onPhone2_Change = ({ target }) => {
        const { value } = target;
        const isValidPhone = validateNumber(value);
        if (areDigits(value)) {
            this.setState({
                phone2: value,
                validPhone2: isValidPhone ? "true" : "false"
            });
        }
    };

    onLGA_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            LGA: value
        });
    };

    onState_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            state: value
        });
    };
    onAddress1_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            address1: value
        });
    };
    onAddress2_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            address2: value
        });
    };
    onRC_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            RC: value
        });
    };
    onCorpName_Change = ({ target }) => {
        const { value } = target;
        this.setState({
            corp_name: value
        });
    };

    render() {
        const {
            validEmail,
            validPhone2,
            validPhone1,
            email,
            phone1,
            phone2,
            username,
            f_name,
            l_name,
            LGA,
            State,
            address1,
            address2,
            RC,
            corp_name,
            redirect
        } = this.state;

        if (redirect) {
            return <Redirect to={{ pathname: "/#/403" }} />;
        }
        return (
            <Fragment>
                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000} />
                    <aside className="isw-sideBar">
                        <Sidebar type="CUS" />
                    </aside>
                    <div />
                    <div className="isw-content--wrapper">
                        <div>
                            <form className="content" id="content-body">
                                <div className="container-fluid container-limited">
                                    <div className="row">
                                        <div className="col-lg-9">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">
                                                        Basic Information
                                                    </h3>
                                                    <div className="isw-card-p">
                                                        Personal information of
                                                        the user managing this
                                                        account
                                                    </div>
                                                </div>
                                                <div
                                                    className="card-body p-3"
                                                    style={{
                                                        paddingTop: `${0} !important`
                                                    }}
                                                >
                                                    <div className="row">
                                                        <div className="col-lg-8">
                                                            <div className="row">
                                                                <div className="col-12">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField0"
                                                                                type="text"
                                                                                className={
                                                                                    validEmail ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validEmail ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                                placeholder="Enter Email"
                                                                                onChange={
                                                                                    this
                                                                                        .onEmail_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onEmail_Change
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField1"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="First Name"
                                                                                onChange={
                                                                                    this
                                                                                        .onFirstname_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onFirstname_Change
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField2"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Last Name"
                                                                                onChange={
                                                                                    this
                                                                                        .onLastname_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onLastname_Change
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="form-field">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField3"
                                                                                type="text"
                                                                                placeholder="Phone Number"
                                                                                onChange={
                                                                                    this
                                                                                        .onPhone1_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onPhone1_Change
                                                                                }
                                                                                value={
                                                                                    phone1
                                                                                }
                                                                                className={
                                                                                    validPhone1 ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validPhone1 ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-md">
                                                            Picture
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-md">
                                            <div className="d-block mb-2">
                                                <button
                                                    onClick={
                                                        this.submitBasicInfo
                                                    }
                                                    className="isw-btn isw-btn--raised bg-primary text-white w-100"
                                                >
                                                    <span>Save</span>
                                                </button>
                                            </div>

                                            <div className="d-block">
                                                <button className="isw-btn isw-btn--raised text-primary w-100">
                                                    <span>Cancel</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    {/* <div className="row mt-4">
                                        <div className="col-lg-9">
                                            <div className="isw-card">
                                                <div className="isw-card-header p-3 mb-4">
                                                    <h3 className="isw-card-h3">
                                                        Business Information
                                                    </h3>
                                                    <div className="isw-card-p">
                                                        Personal information of
                                                        the user managing this
                                                        account
                                                    </div>
                                                </div>
                                                <div
                                                    className="card-body p-3"
                                                    style={{
                                                        paddingTop: `${0} !important`
                                                    }}
                                                >
                                                    <div className="row">
                                                        <div className="col-lg-8">
                                                            <div className="row">
                                                                <div className="col-12">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField4"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Corporate Name"
                                                                                onChange={
                                                                                    this
                                                                                        .onCorpName_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onCorpName_Change
                                                                                }
                                                                                value={
                                                                                    corp_name
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField5"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="RC Number"
                                                                                onChange={
                                                                                    this
                                                                                        .onRC_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onRC_Change
                                                                                }
                                                                                value={
                                                                                    RC
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField6"
                                                                                type="text"
                                                                                value={
                                                                                    phone2
                                                                                }
                                                                                className={
                                                                                    validPhone2 ===
                                                                                    "true"
                                                                                        ? `form-field__input k_input green`
                                                                                        : validPhone2 ===
                                                                                          "false"
                                                                                        ? `form-field__input k_input red`
                                                                                        : `form-field__input k_input`
                                                                                }
                                                                                placeholder="Phone Number"
                                                                                onChange={
                                                                                    this
                                                                                        .onPhone2_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onPhone2_Change
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField7"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Street Address 1"
                                                                                onChange={
                                                                                    this
                                                                                        .onAddress1_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onAddress1_Change
                                                                                }
                                                                                calue={
                                                                                    address1
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-12">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField8"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="Street Address 2"
                                                                                onChange={
                                                                                    this
                                                                                        .onAddress2_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onAddress2_Change
                                                                                }
                                                                                value={
                                                                                    address2
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField9"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="State"
                                                                                onChange={
                                                                                    this
                                                                                        .onState_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onState_Change
                                                                                }
                                                                                value={
                                                                                    State
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-lg-6">
                                                                    <div className="form-field mb-5">
                                                                        <div className="form-field__control">
                                                                            <input
                                                                                id="exampleField10"
                                                                                type="text"
                                                                                className="form-field__input k_input"
                                                                                placeholder="LGA"
                                                                                onChange={
                                                                                    this
                                                                                        .onLGA_Change
                                                                                }
                                                                                onBlur={
                                                                                    this
                                                                                        .onLGA_Change
                                                                                }
                                                                                value={
                                                                                    LGA
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-md">
                                                            Logo
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                 */}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    console.log({ state });

    const {
        edit_basic_info,
        edit_basic_info_data
    } = state.cus_editBasicInfo_reducer;
    return {
        edit_basic_info,
        edit_basic_info_data
    };
};

export default connect(mapStateToProps)(ProfileInfo);

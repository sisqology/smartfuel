import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Actions } from "../../../redux-flow-all/actions/index";
import { ToastContainer, toast } from "react-toastify";

// styles
import "react-toastify/dist/ReactToastify.min.css";
import "../../../scss/pending.scss";
import { SUCCESS, FAILURE } from "../../../redux-flow-all/arsVariables";
import { history } from "../../../reuse/history";
import Static from "../../../reuse/Static";
import { validateEmail } from "../../../reuse/regexes";

// Assets

class ForgotPW extends Component {
    toastId = null;
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            pending: false,
            errorShown: false
        };
    }
    componentDidMount() {
        const { fakeAuth } = this.props;
        console.log({ fakeAuth, history });
        if (fakeAuth.isAuthenticated) {
            history.goBack("/customer/user-list");
        }
    }
    static showToast = (message, type) => {
        if (toast.isActive(ForgotPW.toastId)) {
            return toast.update(ForgotPW.toastId, {
                type: type,
                render: message
            });
        }
        return type === "error"
            ? (ForgotPW.toastId = toast.error(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }))
            : (ForgotPW.toastId = toast.success(message, {
                  position: toast.POSITION.TOP_RIGHT,
                  className: type
              }));
    };

    static redirectToLogin = props => {
        setTimeout(() => {
            props.history.push("/signin");
        }, 5000);
    };

    static getDerivedStateFromProps = (props, state) => {
        const { pending, errorShown } = state;
        const { forgot, forgot_data } = props;
        console.log({ forgot_data });

        // if reset is successful
        if (forgot === SUCCESS && errorShown === false && pending === true) {
            ForgotPW.showToast(forgot_data, "success");
            props.dispatch(Actions.reset());
            ForgotPW.redirectToLogin(props);
            return {
                pending: false,
                errorShown: true
            };
        }

        // if reset fails
        if (forgot === FAILURE && pending === true && errorShown === false) {
            ForgotPW.showToast(forgot_data, "error");
            props.dispatch(Actions.reset());
            return {
                pending: false,
                errorShown: true
            };
        }
        return state;
    };

    onEmailChange = e => {
        this.setState({ email: e.target.value });
    };

    onSubmit = e => {
        e.preventDefault();
        const { email } = this.state;
        this.setState({ pending: true, errorShown: false });
        const isValidEmail = validateEmail(email);
        if (isValidEmail) {
            return this.props.dispatch(
                Actions.cus_forgotPW({ emailAddress: email })
            );
        } else {
            this.setState({ pending: false, errorShown: true });
            return ForgotPW.showToast("Input valid Email", "error");
        }
    };

    render() {
        const { email } = this.state;

        return (
            <div className="isw-login">
                <div className="container-fluid p-0">
                    <div className="row no-gutters bg-primary">
                        <Static />

                        <div className="col-lg-6">
                            <ToastContainer autoClose={5000} />
                            <div
                                className="isw-login--middle"
                                style={{
                                    overflowY: "auto"
                                }}
                            >
                                <div className="isw-login--middle-form">
                                    <form
                                        className="row"
                                        style={{
                                            maxWidth: `${30}rem`,
                                            margin: `${0} auto`
                                        }}
                                    >
                                        <div className="col-12">
                                            <header>
                                                <h1 className="text-primary">
                                                    Password Reset
                                                </h1>
                                                <p>
                                                    Enter your email to reset
                                                    your password
                                                </p>
                                            </header>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-field mb-5">
                                                <div className="form-field__control">
                                                    <input
                                                        className="form-field__input k_input"
                                                        id="exampleField0"
                                                        type="email"
                                                        placeholder="Enter email"
                                                        onChange={
                                                            this.onEmailChange
                                                        }
                                                        onBlur={
                                                            this.onEmailChange
                                                        }
                                                        autoFocus={
                                                            email === ""
                                                                ? true
                                                                : false
                                                        }
                                                        value={email}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <button
                                                onClick={this.onSubmit}
                                                className="isw-btn isw-btn--raised bg-primary text-white w-100"
                                            >
                                                <span>Reset Password</span>
                                            </button>

                                            <div className="mt-4 text-center">
                                                <p
                                                    style={{
                                                        textAlign: "center"
                                                    }}
                                                >
                                                    Remember your password?{" "}
                                                    <Link
                                                        className="text-primary font-weight-bold "
                                                        to="/signin"
                                                    >
                                                        Sign in
                                                    </Link>{" "}
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-2">
                            <div className="isw-column--right" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { forgot, forgot_data, redirectTL } = state.cus_forgotPW_reducer;
    return {
        forgot,
        forgot_data,
        redirectTL
    };
};

export default connect(mapStateToProps)(ForgotPW);

// generic reducer for entire app
import loginUser from "./login_action";
import onInit from "./init_action";
import logoutUser from "./logout_action";
import reset from "./reset_action";

// handling CUSTOMER admin
import cus_resetPW from "../../customer-isw/Customer/redux-flow-customer/actions/cus_pwReset_action";
import cus_sendOTP from "../../customer-isw/Customer/redux-flow-customer/actions/cus_sendOTP_action";
import cus_resendOTP from "../../customer-isw/Customer/redux-flow-customer/actions/cus_resendOTP_action";
import cus_signupUser from "../../customer-isw/Customer/redux-flow-customer/actions/cus_signup_action";
import cus_verifyProfile from "../../customer-isw/Customer/redux-flow-customer/actions/cus_verifyProfile_action";
import cus_forgotPW from "../../customer-isw/Customer/redux-flow-customer/actions/cus_forgotPW_action";
import cus_changePW from "../../customer-isw/Customer/redux-flow-customer/actions/cus_changePW_action";

/**
 *
 * handling CUSTOMER USER BELOW
 *
 */
// handling user
import cus_createUser from "../../customer-isw/Customer/redux-flow-customer/actions/cus_createUser_action";
import cus_getLocation from "../../customer-isw/Customer/redux-flow-customer/actions/cus_getLocation_action";
import cus_getLocations from "../../customer-isw/Customer/redux-flow-customer/actions/cus_getLocations_action";
import cus_createLocation from "../../customer-isw/Customer/redux-flow-customer/actions/cus_createLocation_action";
import cus_editBasicInfo from "../../customer-isw/Customer/redux-flow-customer/actions/cus_editBasicInfo_action";
import cus_getUsers from "../../customer-isw/Customer/redux-flow-customer/actions/cus_getUsers_action";
import cus_getCustomers from "../../customer-isw/Customer/redux-flow-customer/actions/cus_getCustomers_action";

// handling permissions
import cus_permissions from "../../customer-isw/Customer/redux-flow-customer/actions/user_permissions/GET/cus_permissions_action";
import cus_userPermissions from "../../customer-isw/Customer/redux-flow-customer/actions/user_permissions/GET/cus_userPermissions_action";
import cus_roles from "../../customer-isw/Customer/redux-flow-customer/actions/user_permissions/GET/cus_roles_action";
import cus_getRemoveUserPermissions from "../../customer-isw/Customer/redux-flow-customer/actions/user_permissions/GET/cus_getRemoveUserPermissions_action";
import cus_batchUpdateUserPermissions from "../../customer-isw/Customer/redux-flow-customer/actions/user_permissions/POST/cus_batchUpdateUserPermissions_action";
import cus_changeUserRole from "../../customer-isw/Customer/redux-flow-customer/actions/user_permissions/POST/cus_changeUserRole_action";
import cus_removeUserPermission from "../../customer-isw/Customer/redux-flow-customer/actions/user_permissions/POST/cus_removeUserPermission_action";
import cus_removeUserPermissions from "../../customer-isw/Customer/redux-flow-customer/actions/user_permissions/POST/cus_removeUserPermissions_action";
import cus_updateUserPermissions from "../../customer-isw/Customer/redux-flow-customer/actions/user_permissions/POST/cus_updateUserPermissions_action";

/**
 *
 * handling ISW ADMIN
 *
 */
import isw_createVendor from "../../customer-isw/ISW/redux-flow-isw/actions/isw_createVendor_action";
export const Actions = {
    // action to signin user
    loginUser,
    // action on init of any route
    onInit,
    // action to logout user
    logoutUser,
    // action to signup user
    cus_signupUser,
    // action to reset password
    cus_resetPW,
    // action to send OTP
    cus_sendOTP,
    // action to resend OTP
    cus_resendOTP,
    // action to verifyprofile
    cus_verifyProfile,
    // action to forget password
    cus_forgotPW,
    // action to change password
    cus_changePW,
    // action to create CUSTOMER user
    cus_createUser,
    // action to create CUSTOMER location
    cus_createLocation,
    // action to edit basic profile info
    cus_editBasicInfo,
    // action to get location
    cus_getLocation,
    // action to get locations
    cus_getLocations,
    // action to get  users
    cus_getUsers,
    // action to get created customers
    cus_getCustomers,

    // GET
    // action to get permissions
    cus_permissions,
    // action to get user permissions
    cus_userPermissions,
    // action to get remove user permissions
    cus_getRemoveUserPermissions,
    // action to get roles
    cus_roles,

    // POST
    // action to remove user permissions
    cus_batchUpdateUserPermissions,
    // action to batch update user permissions
    cus_changeUserRole,
    // action to change user role permissions
    cus_removeUserPermission,
    // action to remove user permissions
    cus_removeUserPermissions,
    // action to remove users permissions
    cus_updateUserPermissions,
    // action to reset actions after every error response
    reset,

    // everything ISW below here
    isw_createVendor
};

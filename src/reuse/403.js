import React, { Fragment, Component } from "react";
import { Link } from "react-router-dom";
import { Icon403 } from "./svgs";
import { history } from "./history";
import { fakeAuth } from "../App";
class Page_403 extends Component {
    goHome = () => {
        history.goBack();
    };
    render() {
        console.log({ history, fakeAuth });
        return (
            <Fragment>
                <div className="isw-error">
                    <div className="isw-error-wrapper">
                        <h4>Error: 403 Unexpected Error</h4>
                        <div
                            className="isw-image"
                            style={{ margin: `${3}rem ${1}rem` }}
                        >
                            <Icon403 />
                        </div>
                        <div className="err-text-wrapper">
                            <p className="mb-2">
                                Sorry, access to this resource on the server is
                                denied.
                            </p>
                            <p>
                                Either check the URL,{" "}
                                <span
                                    onClick={this.goHome}
                                    className="text-danger csr"
                                >
                                    go home
                                </span>{" "}
                                , or feel free to{" "}
                                <Link to="" className="text-danger">
                                    report this issue
                                </Link>{" "}
                                .
                            </p>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
export default Page_403;

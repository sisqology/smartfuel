import {toast} from "react-toastify";

export function showToast(message, type){

    if(type === "success"){
        return (toast.success(message, {
            position: toast.POSITION.TOP_RIGHT
        }));
    }

    if(type === "error"){
        return (toast.error(message, {
            position: toast.POSITION.TOP_RIGHT
        }));
    }

    if(type === "warning"){
        return (toast.warn(message, {
            position: toast.POSITION.TOP_RIGHT
        }));
    }
}
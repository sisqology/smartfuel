import React, { Component, Fragment } from "react";

import Leye from "../assets/images/isw-imageBox.png";

class Pagination extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: null,
            usersPerPage: 2,
            users: [],
            total: 0
        };
    }

    componentDidMount() {
        console.log(this.props);
    }

    static getDerivedStateFromProps = (props, state) => {
        console.log({ props });
        const { data, total, currentPage } = props;
        return { users: data, total: total, currentPage: currentPage };
    };

    render() {
        const { currentPage, usersPerPage, users, total } = this.state;
        const {
            decrement,
            increment,
            handleClick,
            goToCustomerInfo
        } = this.props;

        // Logic for displaying users
        const indexOfLastuser = currentPage * usersPerPage;
        const indexOfFirstuser = indexOfLastuser - usersPerPage;
        const currentUsers = users.slice(indexOfFirstuser, indexOfLastuser);

        const renderusers = (
            <ul className="isw-table--body">
                {currentUsers.map(user => (
                    <li
                        onClick={() => goToCustomerInfo(user.id)}
                        key={user.id.toString()}
                        className="isw-table--bodyLi"
                        style={{
                            border: `${1}px solid #DBDBDB`,
                            background: "#fff",
                            height: `${81}px`,
                            cursor: "pointer"
                        }}
                    >
                        <span
                            className="isw-table--bodyA"
                            style={{
                                padding: `${0.7}rem ${1}rem`,
                                display: "flex",
                                width: `${100}%`,
                                height: `${81}px`
                            }}
                        >
                            <div className="col-lg-4 isw-valign--middle">
                                <div
                                    className="isw-table--bodyItem isw-table--bodyItem-img"
                                    style={{
                                        display: "flex"
                                    }}
                                >
                                    <img
                                        src={Leye}
                                        alt="leye"
                                        style={{
                                            borderRadius: `${50}%`,
                                            width: `${40}px`,
                                            height: `${40}px`,
                                            margin: `0 ${10}px 0 0`
                                        }}
                                    />
                                    <div className="isw-valign--middle">
                                        <h3 className="isw-subtitle">
                                            {user.firstName} {user.lastName}
                                        </h3>
                                        <p
                                            className="isw-p2 mb-0"
                                            style={{
                                                padding: 0,
                                                margin: `${8}px 0`
                                            }}
                                        >
                                            leye@smartware.com
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-2 isw-valign--middle">
                                <span className="isw-p2">
                                    {user.phoneNumber}
                                </span>
                            </div>
                            <div className="col-lg-3 isw-valign--middle">
                                <span className="isw-p2">Role</span>
                            </div>
                            <div className="col-lg-3 isw-valign--middle text-right">
                                <span className="isw-p2">Location</span>
                            </div>
                        </span>
                    </li>
                ))}
            </ul>
        );

        // Logic for displaying page numbers
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(users.length / usersPerPage); i++) {
            pageNumbers.push(i);
        }

        console.log({ pageNumbers, total, indexOfLastuser });

        return (
            <Fragment>
                <nav aria-label="pagination" style={{ width: `${100}%` }}>
                    {renderusers}
                </nav>
                <ul
                    className="pagination"
                    style={{
                        width: "fit-content",
                        alignSelf: "flex-end",
                        margin: `${10}px 0 0`
                    }}
                >
                    <li
                        className={
                            currentPage === 1
                                ? `page-item disabled`
                                : `page-item`
                        }
                    >
                        <span className="page-link" onClick={decrement}>
                            Previous
                        </span>
                    </li>
                    {pageNumbers.map(num => (
                        <li
                            key={num}
                            id={num}
                            className="page-link"
                            onClick={handleClick}
                        >
                            {num}
                        </li>
                    ))}
                    <li
                        className={
                            total === indexOfLastuser
                                ? `page-item disabled`
                                : `page-item`
                        }
                    >
                        <span className="page-link" onClick={increment}>
                            Next
                        </span>
                    </li>
                </ul>
            </Fragment>
        );
    }
}

export default Pagination;

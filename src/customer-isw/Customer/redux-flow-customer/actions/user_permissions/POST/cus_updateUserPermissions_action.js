import { UU_PERMISSIONS } from "../../../../../../redux-flow-all/arsVariables";

const cus_updateUserPermissions = payload => ({
    type: UU_PERMISSIONS,
    payload
});

export default cus_updateUserPermissions;

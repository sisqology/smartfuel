import React from "react";

// Styles
import "../scss/trumps.scss";
import "../scss/reset.scss";
import "../scss/loadingpage.scss";

const LoadingPage = ({ text }) => {
    return (
        <div className="loading-container">
            <div className="spinner">
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
                <div />
            </div>
            <p>{text}</p>
        </div>
    );
};

export default LoadingPage;

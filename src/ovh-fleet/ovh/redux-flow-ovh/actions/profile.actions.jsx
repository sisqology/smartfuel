import {ApiService} from "../../../api-service";
import {routeApis} from "../../../services-constants";
import * as OVHCONSTANTS from "../ovh-constants";
const axios = require('axios');


export function updateProfile(payload){
    return (dispatch) => {
        let consume = ApiService.request(routeApis.EDIT_PROFILE, "POST", payload);
        dispatch(request(consume));
        return consume
            .then(response => {
                if(response.data.code !== 1){
                    console.error(response);
                    dispatch(failure(response.data.description));
                }
                else{
                    dispatch(success(response));
                }
            })
            .catch(error => {
                dispatch(failure(error.description));
            });
    };

    function request(request) { return { type:OVHCONSTANTS.UPDATE_PROFILE_PENDING, request} }
    function success(response) { return {type:OVHCONSTANTS.UPDATE_PROFILE_SUCCESS, response} }
    function failure(error) { return {type:OVHCONSTANTS.UPDATE_PROFILE_FAILURE, error} }
}

import { call, put, takeLatest } from "redux-saga/effects";
import Services from "../../../../redux-flow-all/services/services";
import {
    GET_LOCATIONS_FAILURE,
    GET_LOCATIONS_SUCCESS,
    GET_LOCATIONS,
    ENCRYPT_USER
} from "../../../../redux-flow-all/arsVariables";
import { decryptAndRead } from "../../../../redux-flow-all/services/localStorageHelper";

function* getLocations(payload) {
    const decryptedToken = decryptAndRead(ENCRYPT_USER).access_token;
    console.log({ decryptedToken });
    try {
        const returnedData = yield call(
            Services.cus_getLocationsService,
            payload,
            decryptedToken
        );
        const { error, data, status } = returnedData;
        const { code, description, payload: payloadReturned } = data;
        console.log({ data, returnedData });
        if (error === false && status === 200 && code === 1) {
            return yield put({
                type: GET_LOCATIONS_SUCCESS,
                message: description,
                payloadReturned
            });
        } else if (error === false && status === 200 && code === -1) {
            return yield put({
                type: GET_LOCATIONS_FAILURE,
                message: description
            });
        } else if (error === false && status === 200 && code === -2) {
            return yield put({
                type: GET_LOCATIONS_FAILURE,
                message: description
            });
        }
    } catch (error) {
        console.log({ error });
        const { response } = error;
        if (response) {
            const { data, status } = response;
            if (status === 400) {
                return yield put({
                    type: GET_LOCATIONS_FAILURE,
                    message: data.error_description
                });
            }
        } else if (response === undefined) {
            return yield put({
                type: GET_LOCATIONS_FAILURE,
                message: "Failed to connect"
            });
        }
    }
}

export default function* cus_getLocationsSaga() {
    yield takeLatest(GET_LOCATIONS, getLocations);
}

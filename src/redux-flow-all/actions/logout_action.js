import { LOGOUT_USER } from "../arsVariables";

const logoutUser = () => ({
    type: LOGOUT_USER
});

export default logoutUser;

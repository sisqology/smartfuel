import {
    CUSTOMER_ADMIN,
    SUPER_ADMIN,
    VENDOR_ADMIN
} from "../redux-flow-all/arsVariables";

export const customerTester = props => {
    console.log({ props });
    if (CUSTOMER_ADMIN === props) {
        return true;
    } else {
        return false;
    }
};
export const superTester = props => {
    console.log({ props });
    if (SUPER_ADMIN === props) {
        return true;
    } else {
        return false;
    }
};
export const vendorTester = props => {
    console.log({ props });
    if (VENDOR_ADMIN === props) {
        return true;
    } else {
        return false;
    }
};

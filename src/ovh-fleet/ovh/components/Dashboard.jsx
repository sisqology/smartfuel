import * as React from "react";
import { connect } from "react-redux";
import {ToastContainer} from "react-toastify";
// import Sidebar from "../../../customer-isw/customer-reuse/sidebar";
import {Fragment} from "react";
import OvhMenu from "../templates/menu";
import OvhHeader from "../templates/header";


class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        const resp = this.props;

        return (
            <Fragment>
                <div id="">
                    <OvhHeader />
                </div>
                <main role="main" style={{ clear: "both", position: "relative"}}>
                    <div className="isw-mainLayout1">
                        <ToastContainer autoClose={5000} />
                        <OvhMenu />
                        <div className="isw-content--wrapper">
                            <div>
                                <form className="content" id="content-body">
                                    <div className="container-fluid container-limited">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="isw-card">
                                                    <div className="card-body p-3">
                                                        Dashboard
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
            </Fragment>
        );
    }
}

export default connect(null)(Dashboard);

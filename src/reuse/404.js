import React, { Fragment, Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { Icon404 } from "./svgs";
import { history } from "./history";
import { fakeAuth } from "../App";
class Page_404 extends Component {
    // componentDidMount() {}
    state = {};
    static getDerivedStateFromProps = (props, state) => {
        // const { history, fakeAuth } = props;
        const { state: STATE } = history.location;
        console.log({ history, fakeAuth });
        if (STATE) {
            if (!STATE.from.hash.includes("#") && STATE.from.pathname === "/") {
                history.push("/signin");
            }
        }
        // else if (history.location.pathname.includes("#")) {
        //     history.push("/#/404");
        // }

        return state;
    };

    goHome = () => {
        history.goBack();
    };

    componentWillUnmount() {
        console.log({ history, fakeAuth }, this.props);
    }
    render() {
        console.log({ history, fakeAuth }, this.props);

        return (
            <Fragment>
                <div className="isw-error">
                    <div className="isw-error-wrapper">
                        <h4>Error: 404 Unexpected Error</h4>
                        <div
                            className="isw-image"
                            style={{ margin: `${3}rem ${1}rem` }}
                        >
                            <Icon404 />
                        </div>
                        <div className="err-text-wrapper">
                            <p className="mb-2">
                                Sorry, the page you're looking for cannot be
                                accessed.
                            </p>
                            <p>
                                Either check the URL,{" "}
                                <span
                                    onClick={this.goHome}
                                    className="text-danger csr"
                                >
                                    go home
                                </span>{" "}
                                , or feel free to{" "}
                                <Link to="" className="text-danger">
                                    report this issue
                                </Link>{" "}
                                .
                            </p>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Page_404;

const BASEURL = 'https://feulapp.azurewebsites.net/';


export const routeApis = {
    BASEURL: BASEURL,
    VENDOR_USERS: BASEURL + 'api/Vendor/ListVendorUsersByVendor',
    CREATE_VENDOR_USER: BASEURL + 'api/UserProfiling/CreateVendorUser',
    CREATE_FLEET_OWNER: BASEURL + 'api/UserProfiling/CreateFleetOwner',
    LIST_FLEET_OWNERS: BASEURL + 'api/Vendor/ListFleetOwners',
    EDIT_PROFILE: BASEURL + '/api/UserProfiling/EditProfile',

};
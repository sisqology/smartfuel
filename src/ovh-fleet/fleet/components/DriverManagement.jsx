import * as React from "react";
import {Fragment} from "react";
import OvhHeader from "../../ovh/templates/header";
import {ToastContainer} from "react-toastify";
import OvhMenu from "../../ovh/templates/menu";
import {IconAddContact, IconFilter} from "../../../reuse/svgs";
import {NavLink} from "react-router-dom";
import Leye from "../../../assets/images/isw-imageBox.png";
import connect from "react-redux/es/connect/connect";

class DriverManagement extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
        this.goToAddFleetOwner = this.goToAddFleetOwner.bind(this);
    }

    goToAddFleetOwner(){
        this.props.history.push(`create-fleet-owner`);
    }


    render() {
        const resp = this.props;
        const url = "/fleet-owner/";

        return (
            <Fragment>
                <div id="">
                    <OvhHeader />
                </div>
                <div className="isw-mainLayout1">
                    <ToastContainer autoClose={5000}/>
                    <OvhMenu/>
                    <div></div>
                    <div className="isw-content--wrapper">
                        <div>
                            <div className="content" id="content-body">
                                <div className="container-fluid container-limited">

                                    <div className="row my-4">
                                        <div className="col-lg-6 isw-valign--middle mr-auto">
                                            <p className="isw-p mb-0">Users list (showing 1-8 of 26 records)</p>
                                        </div>

                                        <div className="col-lg-3 text-right">
                                            <button className="isw-btn border w-100" data-toggle="modal"
                                                    data-target="#exampleModalCenter">
                                                <IconFilter /><span>Filter Table</span>
                                            </button>
                                        </div>

                                        <div className="col-md text-right">
                                            <button className="isw-btn isw-btn--raised bg-primary text-white w-100" onClick={this.goToAddFleetOwner}>
                                                <IconAddContact /><span>Add Fleet Owner</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div className="row mb-4 mt-3">
                                        <div className="col-12">
                                            <div className="isw-table">
                                                <ul className="isw-table--head">
                                                    <li className="isw-table--headLi">
                                                        <div className="row">
                                                            <div className="col-lg-4">
                                                                <span className="isw-table--headItem">Name</span>
                                                            </div>
                                                            <div className="col-lg-2">
                                                                <span
                                                                    className="isw-table--headItem">Phone Number</span>
                                                            </div>
                                                            <div className="col-lg-2">
                                                                <span className="isw-table--headItem">Role</span>
                                                            </div>
                                                            <div className="col-lg-2">
                                                                <span className="isw-table--headItem">Processed</span>
                                                            </div>
                                                            <div className="col-lg-2 text-right">
                                                                <span className="isw-table--headItem">Status</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>


                                                <ul className="isw-table--body">
                                                    <li className="isw-table--bodyLi">
                                                        <NavLink className="row isw-table--bodyA"
                                                                 to={`${url}fleet-owner-detail`}>
                                                            <div className="col-lg-4 isw-valign--middle">
                                                                <div
                                                                    className="isw-table--bodyItem isw-table--bodyItem-img">
                                                                    <div className="isw-image isw-valign--middle mr-3"
                                                                         style={{
                                                                             height: `${2.5}rem`,
                                                                             width: `${2.5}rem`,
                                                                             borderRadius: `${100}%`,
                                                                             overflow:
                                                                                 "hidden"
                                                                         }}>
                                                                        <img
                                                                            src={
                                                                                Leye
                                                                            }
                                                                            alt="leye"
                                                                        />

                                                                    </div>
                                                                    <div className="isw-valign--middle">
                                                                        <h3 className="isw-subtitle">Adeleye Samuel</h3>
                                                                        <p className="isw-p2 mb-0">leye@smartware.com</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-2 isw-valign--middle">
                                                                <span className="isw-p2">08026372839</span>
                                                            </div>
                                                            <div className="col-lg-2 isw-valign--middle">
                                                                <span className="isw-p2">Logistics</span>
                                                            </div>
                                                            <div className="col-lg-2 isw-valign--middle">
                                                                <span className="isw-p2">23 orders</span>
                                                            </div>
                                                            <div className="col-lg-2 isw-valign--middle text-right">
                                                                <span className="isw-p2">Active</span>
                                                            </div>
                                                        </NavLink>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12">
                                            <nav aria-label="pagination" className="float-right">
                                                <ul className="pagination">
                                                    <li className="page-item disabled"><span
                                                        className="page-link">Previous</span></li>
                                                    <li className="page-item"><a className="page-link" href="#">1</a>
                                                    </li>
                                                    <li className="page-item active" aria-current="page">
                                                        <span className="page-link">2<span
                                                            className="sr-only">(current)</span></span>
                                                    </li>
                                                    <li className="page-item"><a className="page-link" href="#">3</a>
                                                    </li>
                                                    <li className="page-item"><a className="page-link" href="#">Next</a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default connect(null)(DriverManagement);
